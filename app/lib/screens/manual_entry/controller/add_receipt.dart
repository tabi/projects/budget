import '../../../core/data/database/tables/receipt.dart';
import '../../../core/data/server/sync.dart';
import '../../../core/data/server/sync_db.dart';
import '../../../core/model/receipt.dart';
import '../../receipt_list/state/receipt_list_state.dart';

//TODO: Move to broader scope, camera_entry_screen uses this as well
Future<void> addReceipt(ReceiptListState receiptListState, Receipt receipt) async {
  await _addReceiptToDatabase(receipt);
  await _addReceiptToServer(receipt);
  receiptListState.notify();
}

Future<void> _addReceiptToDatabase(Receipt receipt) async {
  await ReceiptTable().insert(receipt);
}

Future<void> _addReceiptToServer(Receipt receipt) async {
  final _syncDatabase = SyncDatabase();
  await _syncDatabase.createSyncSync(dataReceiptType, receipt.id, created);
  await Synchronise.synchronise();
}
