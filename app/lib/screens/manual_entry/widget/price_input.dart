import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../state/product_state.dart';

class PriceInput extends StatefulWidget {
  @override
  _PriceInputState createState() => _PriceInputState();
}

class _PriceInputState extends State<PriceInput> {
  FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  bool isValidInput(String newPrice) {
    try {
      final _price = double.parse(newPrice);
      if (_price > 0 && _price < 10000000) {
        return true;
      }
    } on Exception {
      print('Input error: $Exception');
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    final productState = ProductState.of(context);
    final product = productState.product;

    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 7 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12.0 * x)),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10.0 * x),
                Icon(Icons.local_offer, color: ColorPallet.darkTextColor, size: 27 * x),
                Container(
                  alignment: Alignment.centerLeft,
                  width: 284 * x,
                  height: 42 * y,
                  margin: EdgeInsets.only(top: 2 * y),
                  child: Stack(
                    children: [
                      TextField(
                        autofocus: true,
                        focusNode: myFocusNode,
                        style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500, fontSize: 18 * f),
                        autocorrect: false,
                        keyboardType: const TextInputType.numberWithOptions(decimal: true),
                        onChanged: (String newPrice) {
                          setState(
                            () {
                              newPrice = newPrice.replaceAll(',', '.');
                              if (isValidInput(newPrice)) {
                                product.price = double.parse(newPrice);
                                productState.notify();
                              } else {
                                product.price = null;
                                productState.notify();
                                if (newPrice.isNotEmpty) {
                                  if (newPrice != '0' && newPrice != '0.' && newPrice != '0,' && newPrice != ',' && newPrice != '.') {
                                    Toast.show(translations.text('invalidInput'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                                  }
                                }
                              }
                            },
                          );
                        },
                        decoration: InputDecoration(
                          hintText: product.price?.toString() ?? translations.text('enterPrice'),
                          hintStyle: TextStyle(color: ColorPallet.midGray, fontWeight: FontWeight.w500, fontSize: 18 * f),
                          filled: true,
                          fillColor: Colors.transparent,
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(horizontal: 10 * x),
                          prefix: product.price == null
                              ? const SizedBox()
                              : Padding(
                                  padding: EdgeInsets.only(right: 2.0 * x),
                                  child: Text(Translations.textStatic('currencySymbol', 'CurrencySetting')),
                                ),
                          prefixStyle: const TextStyle(color: ColorPallet.darkTextColor),
                        ),
                      ),
                      Positioned(
                        right: 10 * x,
                        child: Row(
                          children: [
                            Text(translations.text('enableReturn'),
                                style: TextStyle(
                                  fontSize: 12 * f,
                                  fontWeight: FontWeight.w500,
                                  color: ColorPallet.darkTextColor,
                                )),
                            Switch(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              onChanged: (value) {
                                product.isReturn = !product.isReturn;
                                productState.notify();
                              },
                              value: product.isReturn,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 48,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              '${translations.text('price')}*',
              style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}
