import 'package:flutter/material.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../../search/search.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton2 = GlobalKey();

class ShopInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    final receiptModel = ReceiptState.of(context).receipt;
    return Column(
      children: [
        SizedBox(height: 2 * y),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text('*${translations.text('required')}', style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 11 * f)),
            SizedBox(width: 27 * x),
          ],
        ),
        SizedBox(height: 2 * y),
        Stack(
          key: keyButton2,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8 * y),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12.0 * x)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
                  child: InkWell(
                    onTap: () async {
                      final result = await Navigator.push(
                        context,
                        MaterialPageRoute(
                          settings: const RouteSettings(name: 'SearchWidget'),
                          builder: (context) => const SearchWidget(false),
                        ),
                      );

                      if (result != null) {
                        receiptModel.store.name = result[0];
                        receiptModel.store.category = result[1];
                        ReceiptState.of(context).notify();
                      }
                    },
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(Icons.store, color: ColorPallet.darkTextColor, size: 27 * x),
                            SizedBox(width: 10.0 * x),
                            SizedBox(
                              width: 305 * x,
                              child: Text(receiptModel.store.name ?? translations.text('enterShop'),
                                  style: TextStyle(
                                      color: receiptModel.store.name != null ? ColorPallet.darkTextColor : ColorPallet.midGray,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18 * f),
                                  overflow: TextOverflow.ellipsis),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 48 * x,
              top: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
                color: Colors.white,
                child: Text(
                  '${translations.text('shop')}*',
                  style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 14 * f),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
