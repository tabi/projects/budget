import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton4 = GlobalKey();

class ShopAttributesInput extends StatefulWidget {
  @override
  _ShopAttributesInputState createState() => _ShopAttributesInputState();
}

class _ShopAttributesInputState extends State<ShopAttributesInput> {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');

    return Stack(
      key: keyButton4,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12)),
            child: ScopedModelDescendant<ReceiptState>(
              builder: (context, child, receiptState) => Row(
                children: <Widget>[
                  SizedBox(
                    height: 26 * y,
                  ),
                  SizedBox(
                    width: 364 * x,
                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 110 * x,
                            height: 27 * y,
                            child: Center(
                              child: AutoSizeText(
                                translations.text('abroad'),
                                style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 14 * f),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Switch(
                            activeColor: ColorPallet.pink,
                            value: receiptState.receipt.location.isAbroad,
                            onChanged: (val) {
                              setState(() {
                                receiptState.receipt.location.isAbroad = !receiptState.receipt.location.isAbroad;
                              });
                            },
                          )
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 125 * x,
                            height: 27 * y,
                            child: Center(
                              child: AutoSizeText(
                                translations.text('online'),
                                style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 14 * f),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Switch(
                            activeColor: ColorPallet.pink,
                            value: receiptState.receipt.location.isOnline,
                            onChanged: (val) {
                              setState(() {
                                receiptState.receipt.location.isOnline = !receiptState.receipt.location.isOnline;
                              });
                            },
                          ),
                        ],
                      )
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translations.text('others2'), style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}
