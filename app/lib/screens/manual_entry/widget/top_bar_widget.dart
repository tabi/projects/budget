import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../../../features/para_data/para_data_scoped_model.dart';
import '../../../features/spotlight_tutorial/controller/manual_entry_tutorial.dart';
import '../../receipt_list/state/receipt_list_state.dart';
import '../controller/add_receipt.dart';
import '../controller/update_receipt.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton8 = GlobalKey();

class TopBarWidget extends StatelessWidget {
  final ScrollController _scrollControllerPage;
  final bool isUpdate;
  const TopBarWidget(this._scrollControllerPage, this.isUpdate);

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    return Row(
      children: <Widget>[
        Text(
          translations.text('addReceipt'),
          style: TextStyle(color: Colors.white, fontSize: 22 * x),
        ),
        Expanded(
          child: Center(
            child: InkWell(
              key: keyButton1,
              onTap: () {
                ParaDataScopedModel.of(context).onTap('InkWell', 'showTutorialManualEntry');
                showTutorial(context);
              },
              child: Icon(Icons.info, color: Colors.white, size: 28.0 * x),
            ),
          ),
        ),
        ScopedModelDescendant<ReceiptState>(
          builder: (context, child, state) => SizedBox(
            height: 32 * y,
            child: ElevatedButton(
              key: keyButton8,
              style: ElevatedButton.styleFrom(
                primary: state.isReceiptComplete() ? ColorPallet.darkTextColor : ColorPallet.midGray,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0 * x),
                ),
              ),
              onPressed: () async {
                if (state.isReceiptComplete()) {
                  if (isUpdate) {
                    await updateReceipt(context, ReceiptState.of(context).receipt);
                  } else {
                    await addReceipt(ReceiptListState.of(context), ReceiptState.of(context).receipt);
                  }
                  SystemChrome.setSystemUIOverlayStyle(
                    const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
                  );
                  Navigator.of(context).pop();
                } else {
                  var warning = '';
                  if (!state.hasProducts() && !state.isstoreComplete()) {
                    warning = translations.text('warningIncomplete');
                  } else {
                    if (!state.hasProducts()) {
                      warning = translations.text('warningNoProduct');
                    }
                    if (!state.isstoreComplete()) {
                      warning = translations.text('warningNoShopInfo');
                      await _scrollControllerPage.animateTo(
                        0,
                        curve: Curves.easeOut,
                        duration: const Duration(milliseconds: 300),
                      );
                    }
                  }
                  await showDialog(
                    context: context,
                    routeSettings: const RouteSettings(name: 'ManualEntryInformationIncompleteDialog'),
                    builder: (BuildContext context) {
                      return AlertDialog(
                        titlePadding: const EdgeInsets.all(0),
                        title: Container(
                          color: ColorPallet.pink,
                          padding: const EdgeInsets.all(20),
                          child: Row(
                            children: <Widget>[
                              Text(
                                translations.text('informationIncomplete'),
                                style: TextStyle(fontSize: 20 * f, fontWeight: FontWeight.w500, color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                        content: Text(warning, style: TextStyle(fontSize: 17 * f, fontWeight: FontWeight.w500, color: ColorPallet.darkTextColor)),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(translations.text('ok'), style: const TextStyle(color: ColorPallet.pink)),
                          )
                        ],
                      );
                    },
                  );
                }
              },
              child: Text(
                translations.text('complete'),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16 * x,
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: 20 * x),
      ],
    );
  }
}
