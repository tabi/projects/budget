import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/model/receipt_product.dart';
import '../../../core/state/translations.dart';
import '../state/receipt_state.dart';
import 'add_product_dialog.dart';

class ReceiptProductsList extends StatelessWidget {
  const ReceiptProductsList(this.scrollController);

  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ReceiptState>(
      builder: (context, model, receiptState) {
        final products = receiptState.receipt.products.content;
        return ListView.builder(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          itemCount: products.length,
          itemBuilder: (BuildContext context, int index) {
            return _ProductTile(
              products[index],
              scrollController,
            );
          },
        );
      },
    );
  }
}

class _ProductTile extends StatefulWidget {
  final ReceiptProduct product;
  final ScrollController scrollController;

  const _ProductTile(this.product, this.scrollController);

  @override
  __ProductTileState createState() => __ProductTileState();
}

class __ProductTileState extends State<_ProductTile> {
  PointerDownEvent recognizeTap;
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    return ScopedModelDescendant<ReceiptState>(builder: (context, _, receiptState) {
      return Container(
        height: 57.5 * y,
        margin: EdgeInsets.only(bottom: 10 * y, left: 20 * x, right: 20 * x),
        width: 370 * x,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(12.0 * x),
          ),
          color: Colors.white,
          border: Border.all(
            width: 1.5,
            color: selected ? ColorPallet.darkTextColor : ColorPallet.lightGray,
          ),
        ),
        child: Listener(
          onPointerDown: (v) {
            recognizeTap = v;
          },
          onPointerUp: (v) {
            final _dx = (recognizeTap.localPosition.dx - v.localPosition.dx).abs() < 18;
            final _dy = (recognizeTap.localPosition.dy - v.localPosition.dy).abs() < 18;
            if (recognizeTap.pointer == v.pointer && _dx && _dy) {
              setState(() {
                selected = true;
              });
            }
          },
          child: PopupMenuButton<String>(
            onCanceled: () {
              setState(() {
                selected = false;
              });
            },
            onSelected: (v) {
              setState(() {
                selected = false;
              });
            },
            offset: Offset(20 * x, 35 * y),
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'duplicate',
                child: InkWell(
                  onTap: () {
                    receiptState.receipt.products.duplicateProduct(widget.product);
                    receiptState.notify();
                    widget.scrollController.animateTo(500 * y + receiptState.receipt.products.content.length * 70 * y,
                        curve: Curves.easeOut, duration: const Duration(milliseconds: 300));
                    Navigator.pop(context);
                  },
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 5 * x),
                      Icon(
                        Icons.content_copy,
                        color: ColorPallet.darkTextColor,
                        size: 20 * x,
                      ),
                      SizedBox(width: 17 * x),
                      Text(translations.text('duplicate'), style: TextStyle(fontSize: 16 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500)),
                    ],
                  ),
                ),
              ),
              PopupMenuItem<String>(
                value: 'modify',
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    showDialog(
                      context: context,
                      routeSettings: const RouteSettings(name: 'ProductDialog'),
                      builder: (BuildContext context) {
                        return ProductDialog(widget.product, receiptState);
                      },
                    ).then((isModified) {
                      if (isModified) {
                        ReceiptState.of(context).receipt.products.removeProduct(widget.product);
                      }
                    });
                  },
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 5 * x),
                      Icon(
                        Icons.edit,
                        color: ColorPallet.darkTextColor,
                        size: 20 * x,
                      ),
                      SizedBox(width: 17 * x),
                      Text(translations.text('modify'), style: TextStyle(fontSize: 16 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500)),
                    ],
                  ),
                ),
              ),
              PopupMenuItem<String>(
                value: 'delete',
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    ReceiptState.of(context).receipt.products.removeProduct(widget.product);
                    receiptState.notify();
                  },
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 5 * x),
                      Icon(
                        Icons.delete,
                        color: ColorPallet.darkTextColor,
                        size: 20 * x,
                      ),
                      SizedBox(width: 17 * x),
                      Text(translations.text('deleteItem'), style: TextStyle(fontSize: 16 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500)),
                    ],
                  ),
                ),
              ),
            ],
            child: Row(
              children: <Widget>[
                SizedBox(width: 5 * x),
                SizedBox(
                  width: 60 * x,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '${widget.product.count}x',
                        style: TextStyle(fontSize: 14 * f, fontWeight: FontWeight.w600, color: ColorPallet.darkTextColor),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: 3 * y),
                      Text(
                        widget.product.price.toString().replaceAll('.', ','),
                        style: TextStyle(fontSize: 14 * f, fontWeight: FontWeight.w800, color: ColorPallet.darkTextColor),
                        overflow: TextOverflow.visible,
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5 * x),
                SizedBox(
                  width: 200 * x,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.product.name,
                          style: TextStyle(fontSize: 16.5 * f, fontWeight: FontWeight.w600, color: ColorPallet.darkTextColor), overflow: TextOverflow.ellipsis),
                      Text(widget.product.category,
                          style: TextStyle(fontSize: 14 * f, fontWeight: FontWeight.w600, color: ColorPallet.midGray), overflow: TextOverflow.ellipsis),
                    ],
                  ),
                ),
                Expanded(child: Container()),
                SizedBox(
                  width: 70 * x,
                  child: Text(
                    widget.product.getTotalPrice().toStringAsFixed(2).replaceAll('.', ','),
                    style: TextStyle(fontSize: 18 * f, fontWeight: FontWeight.w600, color: ColorPallet.darkTextColor),
                    maxLines: 1,
                    overflow: TextOverflow.visible,
                    textAlign: TextAlign.end,
                  ),
                ),
                SizedBox(width: 15 * x),
              ],
            ),
          ),
        ),
      );
    });
  }
}
