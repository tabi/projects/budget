import 'package:flutter/material.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/data/database/tables/receipt_product.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../../search/search.dart';
import '../state/product_state.dart';

class ProductNameInput extends StatelessWidget {
  final ProductState productState;

  const ProductNameInput(this.productState);

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    final product = productState.product;
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  final result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      settings: const RouteSettings(name: 'SearchWidget'),
                      builder: (context) => const SearchWidget(true),
                    ),
                  );

                  if (result != null) {
                    product.name = result[0];
                    product.category = result[1];
                    product.coicop = await ReceiptProductTable().getCoicop(result[1]);
                    productState.notify();
                  }
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.label, color: ColorPallet.darkTextColor, size: 27 * x),
                        SizedBox(width: 10.0 * x),
                        SizedBox(
                          width: 266 * x,
                          child: Text(
                            product.name ?? translations.text('enterProductService'),
                            style: TextStyle(
                              color: ColorPallet.darkTextColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 18 * f,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                    if (product.category == null)
                      Container()
                    else
                      Column(
                        children: <Widget>[
                          SizedBox(height: 5 * y),
                          Row(
                            children: <Widget>[
                              SizedBox(width: 6.0 * x),
                              Icon(Icons.category, color: ColorPallet.pink, size: 16 * x),
                              SizedBox(width: 16.7 * x),
                              SizedBox(
                                width: 266 * x,
                                child: Text(product.category,
                                    style: TextStyle(
                                      color: ColorPallet.pink,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14 * f,
                                    ),
                                    overflow: TextOverflow.ellipsis),
                              )
                            ],
                          ),
                        ],
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              '${translations.text('productService')}*',
              style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 14.0 * f),
            ),
          ),
        ),
      ],
    );
  }
}
