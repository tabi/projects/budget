import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../core/controller/util/input.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';
import '../../features/menu/menu.dart';
import '../../features/para_data/para_data_name.dart';
import 'controller/util/login_setup.dart';

Translations translations;

class LoginScreen extends StatefulWidget with ParaDataName {
  @override
  String get name => 'LoginScreen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: ColorPallet.lightBlueWithOpacity),
    );
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Login');
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _QRLoginWidget(),
              _LoginManualWidget(),
            ],
          ),
        ),
      ),
    );
  }
}

class _QRLoginWidget extends StatefulWidget {
  @override
  __QRLoginWidgetState createState() => __QRLoginWidgetState();
}

class __QRLoginWidgetState extends State<_QRLoginWidget> {
  String barcode = '';

  Future<void> scan() async {
    // TODO: Implement QR code scanner here
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 5.5 / 10,
      color: ColorPallet.lightBlueWithOpacity,
      child: Padding(
        padding: EdgeInsets.only(left: 35.0 * x, right: 10 * x),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 68 * y,
            ),
            Text(
              '${translations.text('welcome')},',
              style: TextStyle(
                color: Colors.white,
                fontSize: 42 * f,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 5 * y),
            SizedBox(
              width: 360 * x,
              child: Text(
                translations.text('toLogin'),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22 * f,
                  fontWeight: FontWeight.w700,
                  height: 1.1,
                ),
              ),
            ),
            // Container(
            //   height: 150 * y,
            //   child: Center(
            //     child: Row(children: <Widget>[
            //       ButtonTheme(
            //         minWidth: 180.0 * x,
            //         height: 37.0 * y,
            //         child: RaisedButton(
            //           onPressed: () {
            //             scan();
            //           },
            //           color: ColorPallet.pink,
            //           child: Text(
            //             translations.text('scanQRcode'),
            //             style: TextStyle(
            //               color: Colors.white,
            //               fontSize: 17 * f,
            //               fontWeight: FontWeight.w700,
            //             ),
            //           ),
            //           shape: RoundedRectangleBorder(
            //             borderRadius: BorderRadius.circular(11.0),
            //           ),
            //         ),
            //       ),
            //       SizedBox(width: 60 * x),
            //       Image.asset(
            //         'assets/images/qr_code_scanning.png',
            //         height: 110 * y,
            //       )
            //     ]),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}

class _LoginManualWidget extends StatefulWidget {
  @override
  __LoginManualWidgetState createState() => __LoginManualWidgetState();
}

class __LoginManualWidgetState extends State<_LoginManualWidget> {
  String password = '';
  bool passwordInvisible = true;
  String username = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 4.5 / 10,

      /// 2,
      color: ColorPallet.primaryColor,
      child: Column(
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 40 * y,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40 * x),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorPallet.lightBlueWithOpacity,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0 * x, vertical: 2 * y),
              child: TextField(
                cursorColor: ColorPallet.darkTextColor,
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 19 * f, color: Colors.white),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(vertical: 7 * y),
                  border: InputBorder.none,
                  icon: Icon(Icons.person, color: Colors.white.withOpacity(0.7)),
                  hintText: translations.text('user'),
                  hintStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 17 * f, color: Colors.white.withOpacity(0.7)),
                ),
                onChanged: (value) {
                  value = InputUtil.sanitize(value);
                  setState(() {
                    username = value;
                  });
                },
              ),
            ),
          ),
          SizedBox(height: 18 * y),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40 * x),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorPallet.lightBlueWithOpacity,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0 * x, vertical: 2 * y),
              child: Stack(
                children: <Widget>[
                  TextField(
                    obscureText: passwordInvisible,
                    cursorColor: ColorPallet.darkTextColor,
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 19 * f, color: Colors.white),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 7 * y),
                      border: InputBorder.none,
                      icon: Icon(Icons.vpn_key, color: Colors.white.withOpacity(0.7)),
                      hintText: translations.text('password'),
                      hintStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 17 * f, color: Colors.white.withOpacity(0.7)),
                    ),
                    onChanged: (value) {
                      value = InputUtil.sanitize(value);
                      setState(() {
                        password = value;
                      });
                    },
                  ),
                  if (password == '')
                    Container()
                  else
                    Positioned(
                      top: 6 * y,
                      right: 10 * x,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            passwordInvisible = !passwordInvisible;
                          });
                        },
                        child: !passwordInvisible
                            ? Icon(Icons.visibility, color: Colors.white.withOpacity(0.7))
                            : Icon(
                                Icons.visibility_off,
                                color: Colors.white.withOpacity(0.7),
                              ),
                      ),
                    )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 120 * y,
            child: Center(
              child: ButtonTheme(
                minWidth: 330.0 * x,
                height: 37.0 * y,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 40 * x),
                    primary: ColorPallet.lightGreen,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(11),
                    ),
                  ),
                  onPressed: () async {
                    final isAuthenticated = await Login().authenticate(context, username, password);
                    if (isAuthenticated) {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          settings: const RouteSettings(name: 'Menu'),
                          builder: (context) => Menu(),
                        ),
                      );
                    }
                  },
                  child: Text(
                    translations.text('loginManually'),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17 * f,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
