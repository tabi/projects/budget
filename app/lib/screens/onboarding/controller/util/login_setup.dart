import 'dart:io' show Platform;

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../../../../core/controller/user_progress.dart';
import '../../../../core/data/server/builtvalues/sync_id.dart';
import '../../../../core/data/server/builtvalues/sync_phone_info.dart';
import '../../../../core/data/server/builtvalues/sync_register_data.dart';
import '../../../../core/data/server/sync.dart';
import '../../../../core/data/server/sync_db.dart';
import '../../../../core/state/configuration.dart';
import '../../../../core/state/translations.dart';

Translations translations;

class Login {
  Future<void> _initializeExperiment() async {
    var daysOfExperiment = 14;
    if (LanguageSetting.key == 'es') {
      daysOfExperiment = 6;
    }
    await UserProgressController().initDaysOfExperiment(daysOfExperiment);
    await saveLanguagePreference();
    await saveTablePreference();
  }

  Future<SyncRegisterData> _registerPhone(String username, String password) async {
    final phoneName = DateTime.now().millisecondsSinceEpoch.toString();
    final phoneInfos = <SyncPhoneInfo>[];
    final deviceInfo = DeviceInfoPlugin();

    phoneInfos.add(SyncPhoneInfo.newInstance('country', LanguageSetting.tablePreference.toString()));
    phoneInfos.add(SyncPhoneInfo.newInstance('language', LanguageSetting.key.toString()));

    if (Platform.isAndroid) {
      final androidInfo = await deviceInfo.androidInfo;
      phoneInfos.add(SyncPhoneInfo.newInstance('phoneType', 'android'));
      phoneInfos.add(SyncPhoneInfo.newInstance('model', androidInfo.model.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('manufacturer', androidInfo.manufacturer.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('brand', androidInfo.brand.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('product', androidInfo.product.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('type', androidInfo.type.toString()));
    } else if (Platform.isIOS) {
      final iosInfo = await deviceInfo.iosInfo;
      phoneInfos.add(SyncPhoneInfo.newInstance('phoneType', 'ios'));
      phoneInfos.add(SyncPhoneInfo.newInstance('model', iosInfo.model.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('name', iosInfo.name.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('systemName', iosInfo.systemName.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('systemVersion', iosInfo.systemVersion.toString()));
      phoneInfos.add(SyncPhoneInfo.newInstance('identifierForVendor', iosInfo.identifierForVendor.toString()));
    }
    final syncRegisterData = await Synchronise.registerNewPhone(username, password, phoneName, phoneInfos);
    return syncRegisterData;
  }

  Future<void> _configureApp(BuildContext context, SyncRegisterData syncRegisterData) async {
    String insights;
    String questionnaire;
    String paradata;
    String ocr;

    for (final gi in syncRegisterData.groupInfos) {
      switch (gi.key) {
        case 'InsightsConfiguration':
          insights = gi.value;
          break;
        case 'QuestionnaireConfiguration':
          questionnaire = gi.value;
          break;
        case 'ParadataConfiguration':
          paradata = gi.value;
          break;
        case 'OCRConfiguration':
          ocr = gi.value;
          break;
      }
    }
    await Configuration.of(context).save(
      insightsString: insights,
      questionnaireString: questionnaire,
      paradataString: paradata,
      ocrString: ocr,
    );
  }

  Future<void> _updateSyncId(SyncRegisterData syncRegisterData, String password) async {
    final _syncDatabase = SyncDatabase();
    var syncId = await _syncDatabase.getSyncId();

    syncId = SyncId((b) => b
      ..userName = syncRegisterData.user.name
      ..userPassword = password
      ..phoneName = syncRegisterData.phone.name);
    await _syncDatabase.updateSyncId(syncId);
  }

  static Future<void> saveLanguagePreference() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('languagePreference', LanguageSetting.key);
  }

  static Future<void> saveTablePreference() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('tablePreference', LanguageSetting.tablePreference);
  }

  Future<String> getUsername() async {
    final prefs = await SharedPreferences.getInstance();
    final username = prefs.getString('login_username') ?? '';
    return username;
  }

  Future<void> _saveUsername(String username) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('login_username', username);
  }

  Future<bool> authenticate(BuildContext context, String username, String password) async {
    await _saveUsername(username);
    if (username.toLowerCase() == 'hbs' && password.toLowerCase() == '2020') {
      await _initializeExperiment();

      return true;
    }

    final syncRegisterData = await _registerPhone(username, password);

    if (syncRegisterData == null) {
      Toast.show(Translations.textStatic('cantConnectToServer', 'Login'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return false;
    }

    if (syncRegisterData.user.id == -1) {
      Toast.show(Translations.textStatic('incorrectUsernameOrPassword', 'Login'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return false;
    }

    await _configureApp(context, syncRegisterData);
    await _updateSyncId(syncRegisterData, password);
    await _initializeExperiment();

    return true;
  }
}
