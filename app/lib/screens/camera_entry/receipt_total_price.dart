import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';

double _price;

class ReceiptTotalPriceScreen extends StatefulWidget {
  @override
  _ReceiptTotalPriceScreenState createState() => _ReceiptTotalPriceScreenState();
}

class _ReceiptTotalPriceScreenState extends State<ReceiptTotalPriceScreen> {
  String getDescription() {
    switch (LanguageSetting.key) {
      case 'nl':
        return 'Voer het totaal bedrag in:';
        break;
      case 'be_nl':
        return 'Voer het totaal bedrag in:';
        break;
      case 'be_fr':
        return 'Saisissez le montant total:';
        break;
      case 'de':
        return 'Geben Sie den Gesamtbetrag ein:';
        break;
      case 'es':
        return 'Introduzca el importe total:';
        break;
      case 'lu_fr':
        return 'Saisissez le montant total:';
        break;
      case 'no':
        return 'Skriv inn totalbeløpet :';
        break;
      case 'pl':
        return 'Wprowadź kwotę całkowitą:';
        break;
      case 'sl':
        return 'Vnesite skupni znesek:';
        break;
      case 'fi':
        return 'Kirjoita kokonaissumma:';
        break;
      case 'en':
        return 'Enter the total amount:';
        break;
      case 'hu':
        return 'Adja meg a teljes összeget :';
        break;
      default:
        return 'Enter the total amount:';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorPallet.pink,
        title: Row(
          children: <Widget>[
            Text(
              Translations(context, 'Manual_Entry').text('totalAmount'),
              style: TextStyle(fontSize: 24 * f),
            ),
          ],
        ),
      ),
      body: Center(
        child: SizedBox(
          height: 300 * y,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(getDescription(),
                  style: TextStyle(
                    fontSize: 16 * f,
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w500,
                  )),
              SizedBox(height: 0 * y),
              PriceInput(),
              SizedBox(height: 0 * y),
            ],
          ),
        ),
      ),
    );
  }
}

class PriceInput extends StatefulWidget {
  @override
  _PriceInputState createState() => _PriceInputState();
}

class _PriceInputState extends State<PriceInput> {
  FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  bool isValidInput(String newPrice) {
    try {
      final _price = double.parse(newPrice);
      if (_price > 0 && _price < 10000000) {
        return true;
      }
    } on Exception {
      print('Input error: $Exception');
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 7 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12.0 * x)),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10.0 * x),
                Icon(Icons.local_offer, color: ColorPallet.darkTextColor, size: 27 * x),
                Container(
                  alignment: Alignment.centerLeft,
                  width: 284 * x,
                  height: 42 * y,
                  margin: EdgeInsets.only(top: 2 * y),
                  child: Stack(
                    children: [
                      TextField(
                        autofocus: true,
                        focusNode: myFocusNode,
                        style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500, fontSize: 18 * f),
                        autocorrect: false,
                        keyboardType: const TextInputType.numberWithOptions(decimal: true),
                        onChanged: (String newPrice) {
                          setState(
                            () {
                              newPrice = newPrice.replaceAll(',', '.');
                              if (isValidInput(newPrice)) {
                                _price = double.parse(newPrice);
                              } else {
                                _price = null;
                                if (newPrice.isNotEmpty) {
                                  if (newPrice != '0' && newPrice != '0.' && newPrice != '0,' && newPrice != ',' && newPrice != '.') {
                                    Toast.show(translations.text('invalidInput'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                                  }
                                }
                              }
                            },
                          );
                        },
                        decoration: InputDecoration(
                          hintStyle: TextStyle(color: ColorPallet.midGray, fontWeight: FontWeight.w500, fontSize: 18 * f),
                          filled: true,
                          fillColor: Colors.transparent,
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(horizontal: 10 * x),
                          prefix: _price == null
                              ? const SizedBox()
                              : Padding(
                                  padding: EdgeInsets.only(right: 2.0 * x),
                                  child: Text(Translations.textStatic('currencySymbol', 'CurrencySetting')),
                                ),
                          prefixStyle: const TextStyle(color: ColorPallet.darkTextColor),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 50 * y),
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context, _price);
          },
          style: ElevatedButton.styleFrom(
            primary: _price != null ? ColorPallet.darkTextColor : ColorPallet.midGray,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0 * x),
            ),
          ),
          child: Text(
            Translations(context, 'Questionnaire').text('confirmButtonText'),
            style: TextStyle(
              color: Colors.white,
              fontSize: 16 * x,
            ),
          ),
        )
      ],
    );
  }
}
