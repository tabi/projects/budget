import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:receipt_scanner/models/receipt_data.dart';
import 'package:receipt_scanner/pages/receipt_processing_page.dart';
import 'package:receipt_scanner/util/algorithms/receipt_data_parser.dart';
import 'package:receipt_scanner/util/language_codes.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/model/receipt.dart';
import '../../../core/state/configuration.dart';
import '../../../core/state/translations.dart';
import '../../../screens/camera_entry/camera_screen.dart';
import '../../../screens/camera_entry/controller/setup_receipt.dart';
import '../../../screens/camera_entry/controller/util/save_image_bytes.dart';
import '../../../screens/camera_entry/receipt_total_price.dart';
import '../../../screens/manual_entry/manual_entry_screen.dart';
import '../../../screens/manual_entry/widget/receipt_type_screen.dart';
import '../../../screens/receipt_list/state/receipt_list_state.dart';
import 'check_receipt_quality.dart';
import 'info_video.dart';

MethodChannel channel = const MethodChannel('receipt_scanner');

Future<void> openReceiptScanner(BuildContext context, {double qualityTreshold = 0.2}) async {
  final translations = Translations(context, 'ReceiptOCR');
  final ocrConfiguration = Configuration.of(context).ocr;

  final hasPermission = await _hasCameraPermission();
  if (!hasPermission) {
    _showToast(translations.text('noPermission'));
    return;
  }

  switch (ocrConfiguration) {
    case OCRConfiguration.disabled:
      disabledOcr(context, translations);
      break;
    case OCRConfiguration.only_crop:
      only_crop(translations, context);
      break;
    case OCRConfiguration.local_ocr:
      local_ocr(translations, context, qualityTreshold: qualityTreshold);
      break;
  }
}

void disabledOcr(BuildContext context, Translations translations) async {
  final imagePath = await getBasicReceiptPhoto(context);
  if (imagePath == null) {
    _showToast(translations.text('canceledOcr'));
    return null;
  }

  final receiptCategory = await _pushReceiptCategoryScreen(context);
  if (receiptCategory == null) {
    _showToast(translations.text('noReceiptCategorySelected'));
    return null;
  }

  final totalReceiptPrice = await _getReceiptTotalPrice(context);
  if (totalReceiptPrice == null) {
    _showToast(translations.text('canceledOcr'));
    return null;
  }

  final receipt = await getBasicReceipt(totalReceiptPrice, imagePath, receiptCategory);

  _pushModificationScreen(context, receipt);

  ReceiptListState.of(context).notifyListeners();
}

void only_crop(Translations translations, BuildContext context) async {
  final rawReceiptData = await _getReceiptData(context);
  if (rawReceiptData == null) {
    _showToast(translations.text('canceledOcr'));
    return null;
  }

  final selectedReceiptData = await _pushReceiptProcessingScreen(context, rawReceiptData);
  if (selectedReceiptData == null) {
    _showToast(translations.text('failedToProcess'));
    return null;
  }

  final receiptCategory = await _pushReceiptCategoryScreen(context);
  if (receiptCategory == null) {
    _showToast(translations.text('noReceiptCategorySelected'));
    return null;
  }

  final totalReceiptPrice = await _getReceiptTotalPrice(context);
  if (totalReceiptPrice == null) {
    _showToast(translations.text('canceledOcr'));
    return null;
  }

  final croppedImageLocation = await saveImageBytes(selectedReceiptData.croppedImageData, 0);

  final receipt = await getBasicReceipt(totalReceiptPrice, croppedImageLocation, receiptCategory);

  _pushModificationScreen(context, receipt);

  ReceiptListState.of(context).notifyListeners();
}

void local_ocr(Translations translations, BuildContext context, {double qualityTreshold = 0.2}) async {
  await showVideoTutorial(context);

  final rawReceiptData = await _getReceiptData(context);
  if (rawReceiptData == null) {
    _showToast(translations.text('canceledOcr'));
    return null;
  }

  final selectedReceiptData = await _pushReceiptProcessingScreen(context, rawReceiptData);
  if (selectedReceiptData == null) {
    _showToast(translations.text('failedToProcess'));
    return null;
  }

  final receiptQualityScore = getReceiptQualityScore(selectedReceiptData);
  if (receiptQualityScore < qualityTreshold) {
    _showToast(translations.text('qualityErrorOcr'));
    return null;
  }

  if (selectedReceiptData.products.isEmpty) {
    _showToast(translations.text('noProductsFound'));
    return null;
  }

  final receiptCategory = await _pushReceiptCategoryScreen(context);
  if (receiptCategory == null) {
    _showToast(translations.text('noReceiptCategorySelected'));
    return null;
  }

  final receipt = await _buildReceipt(selectedReceiptData, receiptCategory, receiptQualityScore);
  if (receipt == null) {
    _showToast(translations.text('failedToProcess'));
    return null;
  }

  _pushModificationScreen(context, receipt);
}

void _showToast(String msg) {
  Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.CENTER, fontSize: 16.0 * f);
}

Future<bool> _hasCameraPermission() async {
  if (await Permission.camera.request().isGranted) {
    return true;
  } else {
    return false;
  }
}

Future<String> _getReceiptData(context) async {
  final translations = Translations(context, 'Receipt_Scanner');

  var tipsString = '[]';

  if (Platform.isAndroid) {
    tipsString =
        '[${translations.text('tipWhiteBackground')}, ${translations.text('tipCreases')}, ${translations.text('tipShadow')}, ${translations.text('tipDaylight')}, ${translations.text('tipSquaredCorners')}, ${translations.text('tipFlatness')}]';
  }

  try {
    final jsonDataString = await channel.invokeMethod('openReceiptScanner', {'tips': tipsString});
    if (jsonDataString == 'scanFailed' || jsonDataString == 'scanCancelled') {
      return null;
    }
    return jsonDataString;
  } catch (e) {
    print(e);
    return null;
  }
}

Future<ReceiptData> _pushReceiptProcessingScreen(BuildContext context, String rawReceiptData) async {
  _setLanguageCode();
  final selectedReceiptData = await Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => ReceiptProcessingPage(
        rawReceiptData,
        ColorPallet.pink,
      ),
    ),
  );
  return selectedReceiptData;
}

Future<ProductCategory> _pushReceiptCategoryScreen(BuildContext context) async {
  final receiptCategory = await Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'ReceiptTypeScreen'),
      builder: (context) => ReceiptTypeScreen(),
    ),
  );
  return receiptCategory;
}

Future<Receipt> _buildReceipt(ReceiptData receiptData, ProductCategory receiptCategory, double receiptQualityScore) async {
  final receipt = await initialize_receipt(receiptData, receiptCategory, receiptQualityScore);
  return receipt;
}

void _pushModificationScreen(BuildContext context, Receipt receipt) {
  Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'ManualEntryScreen'),
      builder: (context) => ManualEntryScreen(receipt, false),
    ),
  );
}

void _setLanguageCode() {
  switch (LanguageSetting.key) {
    case 'nl':
      ReceiptDataParser.setCountryCode(ImplementedLanguageCode.NL_nl);
      break;
    case 'en':
      ReceiptDataParser.setCountryCode(ImplementedLanguageCode.UK_en);
      break;
    case 'hu':
      ReceiptDataParser.setCountryCode(ImplementedLanguageCode.HU_hu);
      break;
    case 'sl':
      ReceiptDataParser.setCountryCode(ImplementedLanguageCode.SK_sk);
      break;
    case 'fi':
      ReceiptDataParser.setCountryCode(ImplementedLanguageCode.FI_fi);
      break;
    case 'es':
      ReceiptDataParser.setCountryCode(ImplementedLanguageCode.ES_es);
      break;
    default:
      ReceiptDataParser.setCountryCode(ImplementedLanguageCode.NL_nl);
  }
}

class ProductCategory {
  String category;
  String coicop;
  ProductCategory(this.category, this.coicop);
}

Future<String> getBasicReceiptPhoto(BuildContext context) async {
  final imagePath = await Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'RegularCameraScreen'),
      builder: (context) => const CameraPage(false),
    ),
  );
  return imagePath;
}

Future<double> _getReceiptTotalPrice(BuildContext context) async {
  final receiptTotalPrice = await Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'TotalReceiptPriceScreen'),
      builder: (context) => ReceiptTotalPriceScreen(),
    ),
  );
  return receiptTotalPrice;
}

Future<void> showVideoTutorial(context) async {
  final prefs = await SharedPreferences.getInstance();
  final hasSeenVideo = prefs.getBool('hasSeenOCRVideo') ?? false;
  if (hasSeenVideo == false) {
    await prefs.setBool('hasSeenOCRVideo', true);
    await Navigator.push(
      context,
      MaterialPageRoute(
        settings: const RouteSettings(name: 'TotalReceiptPriceScreen'),
        builder: (context) => ChewieDemo(),
      ),
    );
  }
}
