import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/state/translations.dart';

class ChewieDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ChewieDemoState();
  }
}

class _ChewieDemoState extends State<ChewieDemo> {
  VideoPlayerController _videoPlayerController1;
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    initializePlayer();
  }

  @override
  void dispose() {
    _videoPlayerController1.dispose();
    _chewieController?.dispose();
    super.dispose();
  }

  Future<void> initializePlayer() async {
    if (LanguageSetting.key == 'es') {
      _videoPlayerController1 = VideoPlayerController.asset('assets/videos/receipt_scan_demo_es.mp4');
    } else if (LanguageSetting.key == 'lu_fr') {
      _videoPlayerController1 = VideoPlayerController.asset('assets/videos/receipt_scan_demo_lu_fr.mp4');
    } else {
      _videoPlayerController1 = VideoPlayerController.asset('assets/videos/receipt_scan_demo.mp4');
    }

    await Future.wait([_videoPlayerController1.initialize()]);

    _createChewieController();
    setState(() {});
  }

  void _createChewieController() {
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController1,
      autoPlay: true,
      looping: true,
      deviceOrientationsOnEnterFullScreen: [DeviceOrientation.portraitUp],
      // aspectRatio: 9 / 16,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Text(getTitle()),
            automaticallyImplyLeading: false,
            titleSpacing: 25 * x,
            elevation: 0,
            actions: [
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0 * x),
                  child: const Icon(Icons.close),
                ),
              ),
            ],
          ),
          body: Container(
            child: _chewieController != null && _chewieController.videoPlayerController.value.isInitialized
                ? Chewie(
                    controller: _chewieController,
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const CircularProgressIndicator(),
                      SizedBox(height: 20 * y),
                      const Text('Loading'),
                    ],
                  ),
          )),
    );
  }
}

String getTitle() {
  switch (LanguageSetting.key) {
    case 'nl':
      return 'Scan voorbeeld';
      break;
    case 'be_nl':
      return 'Scan voorbeeld';
      break;
    case 'be_fr':
      return 'Exemple de balayage';
      break;
    case 'de':
      return 'Beispiel scannen';
      break;
    case 'es':
      return 'Ejemplo de escaneo';
      break;
    case 'lu_fr':
      return 'Exemple de balayage';
      break;
    case 'no':
      return 'Skanneksempel ';
      break;
    case 'pl':
      return 'Przykład skanowania';
      break;
    case 'sl':
      return 'Primer skeniranja';
      break;
    case 'fi':
      return 'Skannausesimerkki';
      break;
    case 'en':
      return 'Scan example';
      break;
    case 'hu':
      return 'Szkennelési példa';
      break;
    default:
      return 'Scan example';
  }
}
