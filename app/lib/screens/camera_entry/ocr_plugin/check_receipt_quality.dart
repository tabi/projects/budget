import 'package:flutter/material.dart';
import 'package:receipt_scanner/models/quality.dart';
import 'package:receipt_scanner/models/receipt_data.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';

/// Returns if the quality of the receipt is sufficient based an the ratio of products with a quality issue
double getReceiptQualityScore(ReceiptData data) {
  final numberOfProducts = data.products.length;
  final numberOfProductsWithQualityIssues = data.products.where((element) => element.qualityAssessments.first != Quality.good).length;
  return numberOfProductsWithQualityIssues / numberOfProducts;
}

/// Returns an Altert Dialog containing text suggesting fixes to the user
AlertDialog createInsufficientQualityDialog(BuildContext context, ReceiptData data, Function() onYes, Function() onNo) {
  final translations = Translations(context, 'Receipt_Scanner_Quality');

  // Collect all quality issues
  final issues =
      data.products.where((element) => element.qualityAssessments.first != Quality.good).map((e) => e.qualityAssessments).expand((element) => element).toSet();

  // TODO: - Localise
  final suggestions = issues
      .map((qualityIssue) {
        switch (qualityIssue) {
          case Quality.missingTitle:
            return translations.text('receiptNotReadable');
          case Quality.oneSided:
          case Quality.missingPrice:
          case Quality.poorAnalysis:
            return translations.text('receiptProductsNotAligned');
          default:
            {
              return '';
            }
        }
      })
      .toSet()
      .toList();

  return AlertDialog(
    content: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(translations.text('receiptNotGoodEnough'), style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w900, fontSize: 20 * f)),
          IconButton(icon: const Icon(Icons.close), onPressed: onNo),
        ]),
        const SizedBox(height: 10),
        Text(translations.text('receiptImageQualityTooLow')),
        const SizedBox(height: 20),
        Text('${translations.text('tips')}:', style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w900, fontSize: 18 * f)),
        Flexible(
          child: SingleChildScrollView(
            child: UnorderedList(suggestions),
          ),
        ),
        const SizedBox(height: 40),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: ColorPallet.midblue,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              ),
              onPressed: () {
                onYes();
              },
              child: Text(
                translations.text('redoScan'),
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16 * f),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
                onPressed: () {
                  onNo();
                },
                child: Text(
                  translations.text('cancel'),
                  style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16 * f, color: ColorPallet.pink),
                )),
          ],
        )
      ],
    ),
    elevation: 24,
  );
}

/// Returns an Altert Dialog containing text suggesting fixes to the user
AlertDialog createScanFailedDialog(BuildContext context, Function() onYes, Function() onNo) {
  final translations = Translations(context, 'Receipt_Scanner_Quality');

  return AlertDialog(
    content: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(translations.text('receiptNotGoodEnough'), style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w900, fontSize: 20 * f)),
          IconButton(icon: const Icon(Icons.close), onPressed: onNo),
        ]),
        const SizedBox(height: 10),
        Text(translations.text('receiptImageQualityTooLow')),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: ColorPallet.midblue,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              ),
              onPressed: () {
                onYes();
              },
              child: Text(
                translations.text('redoScan'),
                style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16 * f),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
                onPressed: () {
                  onNo();
                },
                child: Text(
                  translations.text('cancel'),
                  style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16 * f, color: ColorPallet.pink),
                )),
          ],
        )
      ],
    ),
    elevation: 24,
  );
}

class UnorderedList extends StatelessWidget {
  const UnorderedList(this.texts);
  final List<String> texts;

  @override
  Widget build(BuildContext context) {
    final widgetList = <Widget>[];
    for (final text in texts) {
      // Add list item
      widgetList.add(UnorderedListItem(text));
      // Add space between items
      widgetList.add(const SizedBox(height: 5));
    }

    return Column(children: widgetList);
  }
}

class UnorderedListItem extends StatelessWidget {
  const UnorderedListItem(this.text);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text('• '),
        Expanded(
          child: Text(text),
        ),
      ],
    );
  }
}
