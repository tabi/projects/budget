import 'package:edit_distance/edit_distance.dart';

import '../model/match.dart';
import 'util/edit_distance.dart';
import 'util/load_match_list.dart';
import 'util/sort_results.dart';

Future<List<SearchMatch>> getMatches(
  String userInput,
  bool isProductSearch,
  NormalizedStringDistance stringDistanceMetric,
  SortOrder sortOrder,
  int examplesPerCategory,
  int numberOfResults,
) async {
  final _matchs = isProductSearch ? await getProductMatchList() : await getShopMatchList();

  final results = <SearchMatch>[];

  //Compute edit distance between user input and all product names
  final matchs = <SearchMatch>[];
  for (final match in _matchs) {
    final distance = getEditDistance(match.name, userInput, stringDistanceMetric);
    if (distance < 1) {
      match.editDistance = distance;
      matchs.add(match);
    }
  }

  //Sort matches
  sortMatchs(matchs, sortOrder);

  //Select [examplesPerCategory] examples per category, and [numberOfResults] results in total
  final _categories = <String>[];
  for (final match in matchs) {
    if (results.length < numberOfResults) {
      if (countOccurences(_categories, match.category) < examplesPerCategory) {
        _categories.add(match.category);
        results.add(match);
      }
    }
  }
  return results;
}

int countOccurences(List<String> categories, String category) {
  final count = categories.where((String c) => c == category).toList().length;
  return count;
}
