import '../../model/match.dart';

enum SortOrder { wordLengthThenFrequency, frequencyThenWordLength }

void sortMatchs(List<SearchMatch> products, SortOrder sortOrder) {
  if (sortOrder == SortOrder.wordLengthThenFrequency) {
    products.sort((a, b) {
      final editDistanceRank = a.editDistance.compareTo(b.editDistance);
      if (editDistanceRank != 0) {
        return editDistanceRank;
      } else {
        final wordLengthRank = a.name.length.compareTo(b.name.length);
        if (wordLengthRank != 0) {
          return wordLengthRank;
        } else {
          final frequencyRank = b.frequency.compareTo(a.frequency);
          return frequencyRank;
        }
      }
    });
  } else {
    products.sort((a, b) {
      final editDistanceRank = a.editDistance.compareTo(b.editDistance);
      if (editDistanceRank != 0) {
        return editDistanceRank;
      } else {
        final frequencyRank = b.frequency.compareTo(a.frequency);
        if (frequencyRank != 0) {
          return frequencyRank;
        } else {
          final wordLengthRank = a.name.length.compareTo(b.name.length);
          return wordLengthRank;
        }
      }
    });
  }
}
