import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../../../core/state/translations.dart';

class SettingState extends Model {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String hour = '';
  String minute = '';
  bool mostRecentSearchSuggestionProducts = true;
  bool mostRecentSearchSuggestionStore = true;
  bool status = false;

  Future<void> changeNotificationTime(BuildContext context, TimeOfDay newTime) async {
    hour = newTime.hour.toString();
    minute = newTime.minute.toString().length == 1 ? '0${newTime.minute}' : newTime.minute.toString();
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('notificationHour', hour);
    await prefs.setString('notificationMinute', minute);
    notifyListeners();

    if (status) {
      await turnOffNotifications(context, false);
      await turnOnNotification(context);
    }
  }

  Future<void> changeNotificationSetting(BuildContext context) async {
    status = !status;
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('notificationStatus', status);
    if (status) {
      await turnOnNotification(context);
    } else {
      await turnOffNotifications(context, true);
    }
    notifyListeners();
  }

  Future<void> turnOnNotification(BuildContext context) async {
    final time = Time(
      int.parse(hour),
      int.parse(minute),
    );
    const androidPlatformChannelSpecifics =
        AndroidNotificationDetails('repeatDailyAtTime channel id', 'repeatDailyAtTime channel name', channelDescription: 'repeatDailyAtTime description');
    const iOSPlatformChannelSpecifics = IOSNotificationDetails();
    const platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics, iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.showDailyAtTime(0, Translations.textStatic('notificationTitle', 'Settings'),
        Translations.textStatic('notificationMessage', 'Settings'), time, platformChannelSpecifics);

    if (Platform.isAndroid) {
      Toast.show('${Translations.textStatic('notificationsTurnedOnAt', 'Settings')} ${'$hour:$minute'}', context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  Future<void> turnOffNotifications(BuildContext context, bool showToast) async {
    await flutterLocalNotificationsPlugin.cancelAll();

    if (showToast) {
      Toast.show(Translations.textStatic('notificationsOff', 'Settings'), context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  Future<void> loadSettings() async {
    final prefs = await SharedPreferences.getInstance();
    hour = prefs.getString('notificationHour') ?? '20';
    minute = prefs.getString('notificationMinute') ?? '00';
    status = prefs.getBool('notificationStatus') ?? false;

    notifyListeners();
  }

  void initNotificationPlugin() {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    const initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
    const initializationSettingsIOS = IOSInitializationSettings();
    const initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
    );
  }

  void changeSearchSuggestionTypeProducts() {
    mostRecentSearchSuggestionProducts = !mostRecentSearchSuggestionProducts;
    notifyListeners();
  }

  void changeSearchSuggestionTypeStore() {
    mostRecentSearchSuggestionStore = !mostRecentSearchSuggestionStore;
    notifyListeners();
  }

  static SettingState of(BuildContext context) => ScopedModel.of<SettingState>(context);
}
