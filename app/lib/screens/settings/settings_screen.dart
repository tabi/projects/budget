import 'dart:io' show Platform;

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/international.dart';
import '../../core/state/translations.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/para_data/para_data_scoped_model.dart';
import '../../features/spotlight_tutorial/controller/settings.dart';
import '../onboarding/controller/util/login_setup.dart';
import 'state/settings_state.dart';

Translations translations;

TextStyle headerText;
TextStyle settingText;
TextStyle valueText;
TextStyle valueTextBlue;

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();

class SettingsScreen extends StatefulWidget with ParaDataName {
  @override
  String get name => 'SettingsScreen';

  @override
  State<StatefulWidget> createState() {
    return _SettingsScreenState();
  }
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  void initState() {
    initTargets(keyButton1, keyButton2, keyButton3);
    super.initState();

    // Future.delayed(
    //   const Duration(milliseconds: 200),
    //   () {
    //     showInitialTutorial(context);
    //   },
    // );
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getString('mainSettingsTutorial') ?? '';
    if (status == '') {
      await prefs.setString('mainSettingsTutorial', 'shown');
      showTutorial(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Settings');
    headerText = TextStyle(color: ColorPallet.darkTextColor, fontSize: 18.0 * f, fontWeight: FontWeight.w700);
    settingText = TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f, fontWeight: FontWeight.w600);
    valueText = TextStyle(color: ColorPallet.midGray, fontSize: 16.0 * f, fontWeight: FontWeight.w600);
    valueTextBlue = TextStyle(color: ColorPallet.primaryColor, fontSize: 16.0 * f, fontWeight: FontWeight.w600);

    return Column(
      children: <Widget>[
        _TopBarWidget(),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(bottom: 8.0 * y),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 8.0 * y),
                _NotificationWidget(),
                SizedBox(height: 8.0 * y),
                // _LanguageWidget(),
                // SizedBox(height: 8.0 * y),
                _ContactWidget(),
                SizedBox(height: 8.0 * y),
                _VersionWidget(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _TopBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPallet.primaryColor,
      height: 50 * y,
      child: Row(
        children: <Widget>[
          SizedBox(width: 22.0 * x),
          ScopedModelDescendant<SettingState>(builder: (context, child, model) {
            return Text(
              translations.text('settings'),
              style: TextStyle(color: Colors.white, fontSize: 22.0 * f, fontWeight: FontWeight.w600),
            );
          }),
          SizedBox(width: 10 * x),
          InkWell(
              onTap: () {
                ParaDataScopedModel.of(context).onTap('InkWell', 'showTutorialSettings');
                showTutorial(context);
              },
              child: Icon(Icons.info, color: Colors.white, size: 28.0 * x)),
        ],
      ),
    );
  }
}

class _NotificationWidget extends StatefulWidget {
  @override
  __NotificationWidgetState createState() => __NotificationWidgetState();
}

class __NotificationWidgetState extends State<_NotificationWidget> {
  @override
  Widget build(BuildContext context) {
    SettingState.of(context).loadSettings();
    return _SettingsBoxWidget(
        settingsWidget: Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
      child: ScopedModelDescendant<SettingState>(
        builder: (context, child, model) => Column(
          key: keyButton1,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.notifications, color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translations.text('notifications'), style: headerText),
              ],
            ),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translations.text('dailyReminders'), style: settingText),
                Switch(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  onChanged: (v) {
                    ParaDataScopedModel.of(context).onTap('Switch', 'notifications:$v');
                    if (SettingState.of(context).flutterLocalNotificationsPlugin == null) {
                      SettingState.of(context).initNotificationPlugin();
                    }
                    model.changeNotificationSetting(context);
                  },
                  value: model.status,
                  activeColor: ColorPallet.pink,
                ),
              ],
            ),
            SizedBox(height: 8.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translations.text('time'), style: settingText),
                InkWell(
                  onTap: () async {
                    await ParaDataScopedModel.of(context).onTap('InkWell', 'mailLauncher');
                    if (Platform.isIOS) {
                      await showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return CupertinoDatePicker(
                            onDateTimeChanged: (DateTime value) {
                              setState(
                                () {
                                  final notificationTime = TimeOfDay.fromDateTime(value);
                                  SettingState.of(context).changeNotificationTime(context, notificationTime);
                                },
                              );
                            },
                            initialDateTime: DateTime.parse('2019-01-01 ${int.parse(model.hour) < 10 ? '0${model.hour}' : model.hour}:${model.minute}'),
                            mode: CupertinoDatePickerMode.time,
                          );
                        },
                      );
                    } else {
                      final notificationTime = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay(hour: int.parse(model.hour), minute: int.parse(model.minute)),
                        builder: (BuildContext context, Widget child) {
                          return MediaQuery(
                            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
                            child: child,
                          );
                        },
                      );
                      await SettingState.of(context).changeNotificationTime(context, notificationTime);
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.only(right: 9.5 * x),
                    child: Text('${model.hour}:${model.minute}',
                        style: model.status ? TextStyle(color: ColorPallet.pink, fontSize: 16.0 * f, fontWeight: FontWeight.w600) : valueText),
                  ),
                ),
              ],
            ),
            SizedBox(height: 8.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    ));
  }
}

class _ContactWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          key: keyButton2,
          margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.forum, color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('contact'), style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('phoneNumber'), style: settingText),
                  InkWell(
                      onTap: () async {
                        await ParaDataScopedModel.of(context).onTap('InkWell', 'phoneCallLauncher');
                        final url = 'tel:${translations.text('phoneNumberDigits')}';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(translations.text('phoneNumberDigits'), style: valueTextBlue),
                      ))
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              SizedBox(height: 24.0 * y),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('email'), style: settingText),
                  InkWell(
                      onTap: () async {
                        await ParaDataScopedModel.of(context).onTap('InkWell', 'mailLauncher');
                        final deviceInfo = DeviceInfoPlugin();
                        final username = await Login().getUsername();

                        String deviceInfoString;
                        if (Platform.isAndroid) {
                          final androidInfo = await deviceInfo.androidInfo;
                          deviceInfoString = androidInfo.model;
                        } else if (Platform.isIOS) {
                          final iosInfo = await deviceInfo.iosInfo;
                          deviceInfoString = iosInfo.model;
                        }
                        final url = 'mailto:${translations.text('emailAdress')}?subject=HBS App&body=Send from $deviceInfoString, $username';

                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(translations.text('emailAdress'), style: valueTextBlue),
                      ))
                ],
              ),
              SizedBox(height: 8.0 * y),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}

class _SettingsBoxWidget extends StatelessWidget {
  final Widget settingsWidget;

  const _SettingsBoxWidget({@required this.settingsWidget});

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(builder: (context, child, model) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0 * x),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(5.0 * x, 1.0 * y), blurRadius: 2.0 * x, spreadRadius: 3.0 * x)],
        ),
        child: settingsWidget,
      );
    });
  }
}

class _LanguageWidget extends StatefulWidget {
  @override
  __LanguageWidgetState createState() => __LanguageWidgetState();
}

class __LanguageWidgetState extends State<_LanguageWidget> {
  @override
  Widget build(BuildContext context) {
    SettingState.of(context).loadSettings();
    return _SettingsBoxWidget(
        settingsWidget: Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
      child: ScopedModelDescendant<SettingState>(
        builder: (context, child, model) => Column(
          key: keyButton3,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.language, color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translations.text('language'), style: headerText),
              ],
            ),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translations.text('language'), style: settingText),
                DropdownButton<String>(
                  value: International.languageFromId(LanguageSetting.key).name,
                  items: International.languages().map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(value, style: TextStyle(color: ColorPallet.pink, fontSize: 16.0 * f, fontWeight: FontWeight.w600)),
                      ),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      LanguageSetting.of(context).language = International.languageFromName(newValue).id;
                      Login.saveLanguagePreference();
                    });
                  },
                ),
              ],
            ),
            SizedBox(height: 0.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    ));
  }
}

class _VersionWidget extends StatelessWidget {
  Future<String> getVersionNumber() async {
    final packageInfo = await PackageInfo.fromPlatform();
    final versionName = packageInfo.version;
    final versionCode = packageInfo.buildNumber;
    return '$versionName+$versionCode';
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingState>(builder: (context, child, model) {
      return _SettingsBoxWidget(
        settingsWidget: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.info_outline, color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translations.text('information'), style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translations.text('versionNumber'), style: settingText),
                  Padding(
                    padding: EdgeInsets.only(right: 9.5 * x),
                    child: FutureBuilder(
                      future: getVersionNumber(),
                      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                        if (!snapshot.hasData) {
                          return const Text('');
                        } else {
                          return Text(snapshot.data, style: settingText);
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}
