import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../core/controller/user_progress.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/data/server/sync.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/international.dart';
import '../../core/model/progress_lists.dart';
import '../../core/state/translations.dart';
import '../../core/widget/gauge_chart.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/para_data/para_data_scoped_model.dart';
import '../../features/spotlight_tutorial/controller/overview_tutorial.dart';
import '../receipt_list/state/receipt_list_state.dart';
import 'widget/calendar.dart';

Translations translations;

int maxDisplayMonth;
int minDisplayMonth;
int monthIndex;

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();
GlobalKey keyButton4 = GlobalKey();

class OverviewScreen extends StatefulWidget with ParaDataName {
  final GlobalKey addKeyButton;

  const OverviewScreen(this.addKeyButton);

  @override
  String get name => 'OverviewScreen';

  @override
  State<StatefulWidget> createState() {
    return _OverviewScreenState();
  }
}

class _OverviewScreenState extends State<OverviewScreen> {
  double distance;
  double initial;

  int _calendarMonth;
  int _calendarYear;
  String _displayUser = 'Tom';
  final _informationPopup = false;

  @override
  void initState() {
    keyButton4 = widget.addKeyButton;
    super.initState();

    Synchronise.synchronise();
    _calendarYear = DateTime.now().year;
    _calendarMonth = DateTime.now().month;
    minDisplayMonth = getPreviousMonthInt(_calendarMonth);
    maxDisplayMonth = getNextMonthInt(_calendarMonth);

    // Future.delayed(Duration(milliseconds: 1000), () {
    //   showInitialTutorial(context);
    // });
  }

  int getPreviousMonthInt(int currentMonth) {
    return currentMonth == 1 ? 12 : currentMonth - 1;
  }

  int getNextMonthInt(int currentMonth) {
    return currentMonth == 12 ? 1 : currentMonth + 1;
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getString('mainOverviewTutorial') ?? '';
    if (status == '') {
      await prefs.setString('mainOverviewTutorial', 'shown');
      showTutorial(context);
    }
  }

  void setUser(String user) {
    setState(() {
      _displayUser = user;
    });
  }

  void currentCalendarMonth() {
    if (!_informationPopup) {
      setState(() {
        _calendarYear = DateTime.now().year;
        _calendarMonth = DateTime.now().month;
      });
    }
  }

  void _nextCalendarMonth() {
    if (!_informationPopup) {
      setState(() {
        _calendarYear = _calendarMonth == 12 ? _calendarYear + 1 : _calendarYear;
        _calendarMonth = _calendarMonth == 12 ? 1 : _calendarMonth + 1;
      });
    }
  }

  void _previousCalendarMonth() {
    if (!_informationPopup) {
      setState(() {
        _calendarYear = _calendarMonth == 1 ? _calendarYear - 1 : _calendarYear;
        _calendarMonth = _calendarMonth == 1 ? 12 : _calendarMonth - 1;
      });
    }
  }

  String getMonthString(int monthInt) {
    String monthString;
    if (LanguageSetting.key == 'itemKey' || LanguageSetting.key == 'pageKey') {
      monthString = DateFormat.MMMM(International.languageFromId('en').localeKey).format(DateTime(_calendarYear, _calendarMonth));
    } else {
      monthString = DateFormat.MMMM(International.languageFromId(LanguageSetting.key).localeKey).format(DateTime(_calendarYear, _calendarMonth));
    }
    return monthString[0].toUpperCase() + monthString.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Calendar');
    initTargets(keyButton1, keyButton2, keyButton3, keyButton4, context);
    return Center(
      child: Column(
        children: <Widget>[
          _TopBarWidget(_nextCalendarMonth, _previousCalendarMonth, getMonthString(_calendarMonth), _informationPopup, setUser, _displayUser,
              currentCalendarMonth, _calendarMonth, _calendarYear),
          _MidSectionWidget(_nextCalendarMonth, _previousCalendarMonth, _calendarMonth, _calendarYear),
        ],
      ),
    );
  }
}

class _TopBarWidget extends StatefulWidget {
  final String displayMonth;
  final String displayUser;
  final bool informationPopup;
  final int _calendarMonth;
  final int _calendarYear;

  const _TopBarWidget(this.nextCalendarMonth, this.previousCalendarMonth, this.displayMonth, this.informationPopup, this.setUser, this.displayUser,
      this.currentCalendarMonth, this._calendarMonth, this._calendarYear);

  @override
  __TopBarWidgetState createState() => __TopBarWidgetState();

  final void Function() nextCalendarMonth;

  final void Function() previousCalendarMonth;

  final void Function(String user) setUser;

  final void Function() currentCalendarMonth;
}

class __TopBarWidgetState extends State<_TopBarWidget> {
  @override
  void initState() {
    super.initState();
    monthIndex = 0;
  }

  bool isCurrentMonth() {
    if (widget._calendarYear == DateTime.now().year && widget._calendarMonth == DateTime.now().month) {
      return true;
    }
    return false;
  }

  bool canGoBackAMonth() {
    if (monthIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  bool canGoForwardAMonth() {
    if (monthIndex < 1) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final image = International.countryFromId(LanguageSetting.tablePreference).topbarImage;
    return Stack(
      children: <Widget>[
        InkWell(
          child: Image(image: AssetImage('assets/images/$image'), fit: BoxFit.fitWidth),
        ),
        Positioned(
          right: 16.0 * x,
          top: 14.0 * y,
          child: InkWell(
            onTap: () {
              ParaDataScopedModel.of(context).onTap('InkWell', 'showTutorialOverview');
              showTutorial(context);
            },
            key: keyButton1,
            child: Icon(Icons.info, color: Colors.white, size: 28.0 * x),
          ),
        ),
        Positioned(
          bottom: 17 * y,
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        ParaDataScopedModel.of(context).onTap('InkWell', 'previousCalendarMonth');
                        if (canGoBackAMonth()) {
                          monthIndex--;
                          widget.previousCalendarMonth();
                        }
                      },
                      child: Icon(Icons.keyboard_arrow_left, color: canGoBackAMonth() ? Colors.white : Colors.white.withOpacity(0.39), size: 29.0 * x),
                    ),
                    SizedBox(width: 10.0 * x),
                    Container(
                      width: 120.0 * x,
                      padding: EdgeInsets.only(bottom: 1 * y),
                      child: Center(
                        child: Text(
                          widget.displayMonth,
                          style: TextStyle(color: Colors.white, fontSize: 20.0 * f, fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                    SizedBox(width: 10.0 * x),
                    InkWell(
                      onTap: () {
                        ParaDataScopedModel.of(context).onTap('InkWell', 'nextCalendarMonth');
                        if (canGoForwardAMonth()) {
                          monthIndex++;
                          widget.nextCalendarMonth();
                        }
                      },
                      child: Icon(Icons.keyboard_arrow_right, color: canGoForwardAMonth() ? Colors.white : Colors.white.withOpacity(0.39), size: 29.0 * x),
                    ),
                  ],
                ),
                SizedBox(width: 62.0 * x),
                Container(width: 26.5 * x),
                SizedBox(width: 20.0 * x),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class _MidSectionWidget extends StatefulWidget {
  final int _calendarMonth;
  final int _calendarYear;

  const _MidSectionWidget(this._nextCalendarMonth, this._previousCalendarMonth, this._calendarMonth, this._calendarYear);

  @override
  State<StatefulWidget> createState() {
    return __MidSectionWidgetState();
  }

  final Function() _nextCalendarMonth;

  final Function() _previousCalendarMonth;
}

class __MidSectionWidgetState extends State<_MidSectionWidget> {
  bool _informationPopup = false;

  void showInformationPopup() {
    setState(() {
      _informationPopup = true;
    });
  }

  void hideInformationPopup() {
    setState(() {
      _informationPopup = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(bottom: 6.0 * y),
        child: _informationPopup
            ? _InformationPopup(hideInformationPopup)
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(height: 4 * y),
                  Expanded(
                    flex: 21,
                    key: keyButton2,
                    child: _CalendarWidget(widget._nextCalendarMonth, widget._previousCalendarMonth, widget._calendarYear, widget._calendarMonth),
                  ),
                  SizedBox(height: 8 * y),
                  Expanded(flex: 9, child: _ProgressWidget(showInformationPopup)),
                  SizedBox(height: 4 * y),
                ],
              ),
      ),
    );
  }
}

class _CalendarWidget extends StatelessWidget {
  final int _calendarMonth;
  final int _calendarYear;
  final Function() _nextCalendarMonth;
  final Function() _previousCalendarMonth;

  const _CalendarWidget(this._nextCalendarMonth, this._previousCalendarMonth, this._calendarYear, this._calendarMonth);

  bool canGoBackAMonth() {
    if (monthIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  bool canGoForwardAMonth() {
    if (monthIndex < 1) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    double initialSwipe;
    double distanceSwiped;
    return ScopedModelDescendant<ReceiptListState>(
      builder: (_, __, ___) => FutureBuilder<ProgressLists>(
        future: UserProgressController().getProgressLists(),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return const SizedBox();
          } else {
            return GestureDetector(
              onPanStart: (DragStartDetails details) {
                initialSwipe = details.globalPosition.dx;
              },
              onPanUpdate: (DragUpdateDetails details) {
                distanceSwiped = details.globalPosition.dx - initialSwipe;
              },
              onPanEnd: (DragEndDetails details) {
                initialSwipe = 0.0;
                if (distanceSwiped < 50) {
                  if (canGoForwardAMonth()) {
                    _nextCalendarMonth();
                    monthIndex++;
                  }
                }
                if (distanceSwiped > 50) {
                  if (canGoBackAMonth()) {
                    _previousCalendarMonth();
                    monthIndex--;
                  }
                }
              },
              child: CalanderWidget(
                _calendarYear,
                _calendarMonth,
                snapshot.data.daysInExperiment,
                snapshot.data.daysCompleted,
                snapshot.data.daysMissing,
              ),
            );
          }
        },
      ),
    );
  }
}

class _ProgressWidget extends StatefulWidget {
  final void Function() showInformationPopup;

  const _ProgressWidget(this.showInformationPopup);

  @override
  __ProgressWidgetState createState() => __ProgressWidgetState();
}

class __ProgressWidgetState extends State<_ProgressWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 9.0 * x),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(1.0 * x, 1.0 * x), blurRadius: 2.0 * x, spreadRadius: 3.0 * x)],
          ),
          child: SizedBox(
            width: (MediaQuery.of(context).size.width - (27 * x)) / 2,
            height: 132.0 * y,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ScopedModelDescendant<ReceiptListState>(
                  builder: (_, __, ___) => FutureBuilder<ProgressLists>(
                    future: UserProgressController().getProgressLists(),
                    builder: (context, progressLists) {
                      if (progressLists.data == null) {
                        return Container();
                      }
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: 150 * x,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  translations.text('progress'),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 14.0 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 12.0 * y),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                height: 15.0 * y,
                                width: 15.0 * x,
                                decoration: BoxDecoration(
                                  color: ColorPallet.lightGreen,
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(1.0 * x, 1.0 * y), blurRadius: 1.0 * x, spreadRadius: 1.0 * x)
                                  ],
                                ),
                              ),
                              SizedBox(width: 10.0 * x),
                              Text(
                                '${progressLists.data.daysCompleted.length} ${translations.text('daysCompleted')}',
                                style: TextStyle(fontSize: 14.0 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700),
                              )
                            ],
                          ),
                          SizedBox(height: 8.0 * y),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                height: 15.0 * y,
                                width: 15.0 * x,
                                decoration: BoxDecoration(
                                  color: ColorPallet.orange,
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(1.0 * x, 1.0 * y), blurRadius: 1.0 * x, spreadRadius: 1.0 * x)
                                  ],
                                ),
                              ),
                              SizedBox(width: 10.0 * x),
                              Text(
                                '${progressLists.data.daysMissing.length - 1 >= 0 ? progressLists.data.daysMissing.length - 1 : 0} ${translations.text('daysMissing')}',
                                style: TextStyle(fontSize: 14.0 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700),
                              )
                            ],
                          ),
                          SizedBox(height: 8.0 * y),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                height: 15.0 * y,
                                width: 15.0 * x,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: ColorPallet.veryLightBlue,
                                  boxShadow: [
                                    BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(1.0 * x, 1.0 * y), blurRadius: 1.0 * x, spreadRadius: 1.0 * x)
                                  ],
                                ),
                              ),
                              SizedBox(width: 10.0 * x),
                              Text(
                                '${progressLists.data.daysRemaining.length} ${translations.text('daysRemaining')}',
                                style: TextStyle(fontSize: 14.0 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700),
                              )
                            ],
                          )
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        InkWell(
          // onTap: widget.showInformationPopup,
          child: Container(
            margin: EdgeInsets.only(left: 9.0 * x),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: ColorPallet.veryLightGray,
                  offset: Offset(1.0 * x, 1.0 * x),
                  blurRadius: 2.0 * x,
                  spreadRadius: 3.0 * x,
                ),
              ],
            ),
            child: SizedBox(
              key: keyButton3,
              width: (MediaQuery.of(context).size.width - (27 * x)) / 2,
              height: 132.0 * y,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      ScopedModelDescendant<ReceiptListState>(
                        builder: (_, __, ___) => FutureBuilder<ProgressLists>(
                          future: UserProgressController().getProgressLists(),
                          builder: (context, progressLists) {
                            if (progressLists.data == null) {
                              return Container();
                            } else {
                              final daysCompleted = progressLists.data.daysCompleted.length;
                              final daysRemaining = progressLists.data.daysRemaining.length;
                              final daysMissing = progressLists.data.daysMissing.length;
                              final daysInExperiment = progressLists.data.daysInExperiment.length;
                              final percentageComplete = (daysCompleted / daysInExperiment * 100).toStringAsFixed(0);

                              final data = [
                                GaugeSegment('daysCompleted', daysCompleted, charts.ColorUtil.fromDartColor(ColorPallet.lightGreen)),
                                GaugeSegment('daysMissing', daysMissing, charts.ColorUtil.fromDartColor(ColorPallet.orange)),
                                GaugeSegment('daysRemaining', daysRemaining, charts.ColorUtil.fromDartColor(ColorPallet.veryLightBlue)),
                              ];

                              final chartData = [
                                charts.Series<GaugeSegment, String>(
                                  id: 'Segments',
                                  domainFn: (GaugeSegment segment, _) => segment.segment,
                                  measureFn: (GaugeSegment segment, _) => segment.size,
                                  colorFn: (GaugeSegment segment, _) => segment.color,
                                  data: data,
                                )
                              ];

                              return SizedBox(
                                width: 190 * x,
                                height: 124 * y,
                                child: Stack(
                                  children: [
                                    GaugeChart(chartData),
                                    Center(
                                      child: Text('$percentageComplete%',
                                          style: TextStyle(
                                            color: ColorPallet.darkTextColor,
                                            fontSize: 18.0 * f,
                                            fontWeight: FontWeight.w700,
                                          )),
                                    )
                                  ],
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _InformationPopup extends StatelessWidget {
  final void Function() hideInformationPopup;

  const _InformationPopup(this.hideInformationPopup);

  Future<void> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 400.0 * y,
        margin: EdgeInsets.symmetric(horizontal: 7.0 * x),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(5.0 * x, 1.0 * y), blurRadius: 2.0 * x, spreadRadius: 3.0 * x)],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0 * y),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 8,
                      child: Center(
                        child: Text(translations.text('infoTitle'),
                            style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 18.0 * f, fontWeight: FontWeight.w700)),
                      )),
                  Expanded(
                    child: Center(
                      child: InkWell(
                        onTap: hideInformationPopup,
                        child: Icon(
                          Icons.close,
                          color: ColorPallet.darkTextColor,
                          size: 27.0 * x,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 10,
              child: Scrollbar(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 5.0 * y),
                  child: SingleChildScrollView(
                    child: FutureBuilder<ProgressLists>(
                        future: UserProgressController().getProgressLists(),
                        builder: (context, progressLists) {
                          if (progressLists == null) {
                            return Container();
                          }
                          final startDate = DateFormat('d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
                              .format(progressLists.data.daysInExperiment.first);
                          final endDate = DateFormat('d MMMM', International.languageFromId(LanguageSetting.key).localeKey)
                              .format(progressLists.data.daysInExperiment.last);

                          return LanguageSetting.key == 'nl'
                              ? RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[
                                    TextSpan(
                                      text: 'Heeft u vragen?',
                                      style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 16.0 * f),
                                    ),
                                    const TextSpan(text: '\n'),
                                    const TextSpan(text: '\n'),
                                    TextSpan(
                                      text:
                                          '''Zie de schriftelijke handleiding voor meer informatie over de app en het onderzoek. Kijk of u een antwoord kunt vinden in de rubriek ‘veel gestelde vragen’ (FAQ).

Ook zijn op youtube filmpjes te vinden over het invullen van de app, te vinden via ''',
                                      style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f),
                                    ),
                                    TextSpan(
                                      text: 'deze link.',
                                      style: TextStyle(color: ColorPallet.primaryColor, fontSize: 16.0 * f),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          _launchURL('https://www.youtube.com/channel/UCHReugknWrrno18qj8YQqfg');
                                        },
                                    ),
                                    const TextSpan(text: '\n'),
                                    const TextSpan(text: '\n'),
                                    TextSpan(
                                      text:
                                          '''Staat uw antwoord er niet bij? Bel ons gerust op (045) 570 7388. U kunt ook mailen naar WINhelpdesk@cbs.nl. Wij zijn bereikbaar van maandag tot en met vrijdag tussen 9.00 en 17.00 uur.''',
                                      style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f),
                                    ),
                                  ]),
                                )
                              : LanguageSetting.key == 'en'
                                  ? RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                          text: translations.text('infoBody'),
                                          style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f),
                                        ),
                                        TextSpan(
                                          text: 'youtube.',
                                          style: TextStyle(color: ColorPallet.primaryColor, fontSize: 16.0 * f),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              _launchURL('https://www.youtube.com/playlist?list=PL3c7jGlkxpEo65jnJ_8F3-lKMjDrR6wg_');
                                            },
                                        ),
                                        TextSpan(
                                            text: '\n\n${translations.text('infoForMoreInfo')}',
                                            style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f)),
                                        const TextSpan(text: '\n'),
                                      ]),
                                    )
                                  : RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                          text: translations.text('infoBody'),
                                          style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f),
                                        ),
                                        TextSpan(
                                          text: LanguageSetting.key == 'fi' || LanguageSetting.key == 'sl'
                                              ? ''
                                              : '${' ${translations.text('between')} '}$startDate${' ${translations.text('between')} '}$endDate. ',
                                          style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f),
                                        ),
                                        TextSpan(
                                            text: '\n\n${translations.text('infoForMoreInfo')}',
                                            style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 16.0 * f)),
                                        TextSpan(
                                          text: translations.text('infoWebsiteLink'),
                                          style: TextStyle(color: ColorPallet.primaryColor, fontSize: 16.0 * f),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              _launchURL(translations.text('url'));
                                            },
                                        ),
                                        const TextSpan(text: '\n'),
                                      ]),
                                    );
                        }),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
