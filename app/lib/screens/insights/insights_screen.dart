import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../../core/controller/util/currency_formatter.dart';
import '../../core/controller/util/date.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/state/configuration.dart';
import '../../core/state/translations.dart';
import '../../core/widget/draggable_scrollbar.dart';
import '../../features/filter_drawer/filter_drawer.dart';
import '../../features/filter_drawer/state/filter.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/para_data/para_data_scoped_model.dart';
import '../../features/spotlight_tutorial/controller/inisights_tutorial.dart';
import '../overview/widget/bar_chart.dart';
import '../receipt_list/state/receipt_list_state.dart';
import 'controller/charts_table.dart';
import 'controller/util/type_converter.dart';
import 'widget/donut_chart.dart';

Translations translations;

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();
GlobalKey keyButton4 = GlobalKey();
GlobalKey keyButton5 = GlobalKey();

Map<String, dynamic> dropdownValue;

bool timeChartPage;
String coicopId = '.';
DateTime dayInPeriod = DateTime.now();
String period = 'month';

class InsightsScreen extends StatefulWidget with ParaDataName {
  @override
  String get name => 'InsightsScreen';

  @override
  State<StatefulWidget> createState() {
    timeChartPage = false;
    coicopId = '.';
    dayInPeriod = DateTime.now();
    period = 'month';
    return __InsightsPageState();
  }
}

class __InsightsPageState extends State<InsightsScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    initTargets(keyButton1, keyButton2, keyButton3, keyButton4, keyButton5);
    super.initState();

    // Future.delayed(Duration(milliseconds: 400), () {
    //   showInitialTutorial(context);
    // });
  }

  void showFilterDrawer() {
    _scaffoldKey.currentState.openEndDrawer();
  }

  void showTimeChart() {
    setState(() {
      timeChartPage = true;
    });
  }

  void showCategoryChart() {
    setState(() {
      timeChartPage = false;
    });
  }

  void showNewCoicop(String coicopCode) {
    setState(() {
      coicopId = coicopCode;
    });
  }

  bool nextPeriodExists(int absMinDate, int absMaxDate, int swipe) {
    final newDate = nextDateInPeriod(swipe);
    return DateUtil.nextPeriodExists(newDate, absMinDate, absMaxDate, period, swipe);
  }

  DateTime nextDateInPeriod(int swipe) {
    DateTime nextDate;
    if (period == 'week') {
      nextDate = dayInPeriod.add(Duration(days: 7 * swipe));
    } else {
      nextDate = dayInPeriod.add(Duration(days: 30 * swipe));
    }
    return nextDate;
  }

  void showNewPeriod(int swipe, int absMinDate, int absMaxDate) {
    setState(() {
      if (nextPeriodExists(absMinDate, absMaxDate, swipe)) {
        dayInPeriod = nextDateInPeriod(swipe);
      }
    });
  }

  void changePeriod(DateTime _dayInPeriod, bool toWeek) {
    setState(() {
      if (period == 'month') {
        dayInPeriod = _dayInPeriod;
        period = 'week';
      } else {
        period = 'month';
      }
    });
  }

  Map<String, dynamic> lastElement(List<Map<String, dynamic>> snapshot) {
    for (final x in snapshot) {
      if (x['code'] == coicopId) {
        return x;
      }
    }
    return snapshot[snapshot.length - 1];
  }

  Widget getFilterIcon(FilterState model) {
    var filterCounter = 0;
    if (model.endDate != null || model.startDate != null) {
      filterCounter = filterCounter + 1;
    }
    if (model.minValue != null || model.maxValue != null) {
      filterCounter = filterCounter + 1;
    }

    if (filterCounter == 0) {
      return Icon(Icons.filter_list, size: 37.0 * f, color: Colors.white);
    } else {
      return Badge(
        position: BadgePosition(top: 2 * y),
        badgeColor: ColorPallet.pink,
        badgeContent: Text(
          filterCounter.toString(),
          style: const TextStyle(color: Colors.white),
        ),
        child: Icon(Icons.filter_list, size: 37.0 * f, color: Colors.white),
      );
    }
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getString('mainInsightsTutorial') ?? '';
    if (status == '') {
      await prefs.setString('mainInsightsTutorial', 'shown');
      showTutorial(context);
    }
  }

  String _cleanName(String name) {
    final nameClean = name.indexOf(' #_#') > 0 ? name.substring(0, name.indexOf(' #_#')) : name;
    final result = nameClean.toString()[0].toUpperCase() + nameClean.toString().substring(1).toLowerCase();
    return result;
  }

  void _onHorizontalDrag(DragEndDetails details, int absMinDate, int absMaxDate) {
    if (details.primaryVelocity == 0) return; // user have just tapped on screen (no dragging)

    if (details.primaryVelocity.compareTo(0) == -1) {
      showNewPeriod(1, absMinDate, absMaxDate);
    } else {
      showNewPeriod(-1, absMinDate, absMaxDate);
    }
  }

  Widget _buildBarChartWidget(BuildContext context, dynamic snapshot, int absMinDate, int absMaxDate) {
    translations = Translations(context, 'Overview');

    return Column(
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.9,
          height: 25 * y,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 35.0 * x,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: 60.0 * x,
                    child: TextButton(
                      style: TextButton.styleFrom(primary: Colors.transparent),
                      onPressed: () {
                        if (nextPeriodExists(absMinDate, absMaxDate, -1)) {
                          showNewPeriod(-1, absMinDate, absMaxDate);
                        } else {
                          Toast.show(translations.text('noEarlierExpenses'), context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                        }
                      },
                      child: Icon(
                        Icons.keyboard_arrow_left,
                        size: 29.0 * x,
                        color: nextPeriodExists(absMinDate, absMaxDate, -1) ? ColorPallet.darkTextColor : ColorPallet.lightGray,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.33,
                    child: Center(
                      child: Text(DateUtil.barChartTitle(dayInPeriod, period),
                          style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 16.0 * f)),
                    ),
                  ),
                  SizedBox(
                    width: 60.0 * x,
                    child: TextButton(
                      style: TextButton.styleFrom(primary: Colors.transparent),
                      onPressed: () {
                        if (nextPeriodExists(absMinDate, absMaxDate, 1)) {
                          showNewPeriod(1, absMinDate, absMaxDate);
                        } else {
                          Toast.show(translations.text('noLaterExpenses'), context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                        }
                      },
                      child: Icon(
                        Icons.keyboard_arrow_right,
                        size: 29.0 * x,
                        color: nextPeriodExists(absMinDate, absMaxDate, 1) ? ColorPallet.darkTextColor : ColorPallet.lightGray,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 50.0 * x,
                child: TextButton(
                  style: TextButton.styleFrom(primary: Colors.transparent),
                  onPressed: () {
                    changePeriod(dayInPeriod, true);
                  },
                  child: Icon(
                    period == 'week' ? Icons.zoom_out : Icons.zoom_in,
                    size: 35.0 * x,
                    color: ColorPallet.darkTextColor,
                  ),
                ),
              )
            ],
          ),
        ),
        GestureDetector(
          onDoubleTap: () {
            //changePeriod();
          },
          onHorizontalDragEnd: (DragEndDetails details) => _onHorizontalDrag(details, absMinDate, absMaxDate),
          child: Container(
            height: 223.0 * y,
            //height: 278.0 * y,
            width: MediaQuery.of(context).size.width * 0.9,
            margin: EdgeInsets.symmetric(horizontal: 7.0 * x),
            child: BarChart(snapshot, DateUtil.barChartTitle(dayInPeriod, period), DateUtil.barChartTickSpec(dayInPeriod, period), changePeriod,
                period == 'week', absMinDate, absMaxDate),
          ),
        ),
        SizedBox(height: 5 * y),
      ],
    );
  }

  Widget _buildCategoryListHorizontal(BuildContext context, List<Map<String, dynamic>> snapshot) {
    return SizedBox(
      height: 43 * y,
      child: Center(
        child: ListView.builder(
          itemCount: snapshot.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: <Widget>[
                _buildCategoryWidgetHorizontal(context, snapshot[index]),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildCategoryListVertical(BuildContext context, List<Map<String, dynamic>> snapshot) {
    final _arrowsController = ScrollController();
    return SizedBox(
      height: 172 * y,
      child: DraggableScrollbar.rrect(
        alwaysVisibleScrollThumb: snapshot.length > 4,
        heightScrollThumb: snapshot.length > 10 ? 50.0 : 50.0 + (10 - snapshot.length) * 10.0,
        backgroundColor: Colors.grey, // ColorPallet.primaryColor,
        padding: EdgeInsets.only(right: 4.0 * x),
        controller: _arrowsController,
        child: ListView.builder(
          controller: _arrowsController,
          itemCount: snapshot.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: <Widget>[
                _buildCategoryWidgetVertical(context, snapshot[index]),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildCategoryWidgetHorizontal(BuildContext context, final Map<String, dynamic> snapshot) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10 * y, vertical: 4 * x),
      width: MediaQuery.of(context).size.width * 0.9,
      height: 35 * y,
      child: InkWell(
        onTap: () {
          // print('parentCode = ' + snapshot['parent'].toString());
          ParaDataScopedModel.of(context).onTap('InkWell', 'showParentCoicop');
          showNewCoicop(snapshot['parent'].toString());
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10 * x),
          child: Row(
            children: <Widget>[
              Container(
                child: snapshot['code'].toString() != snapshot['parent'].toString()
                    ? Icon(Icons.arrow_back, size: 22 * x, color: ColorPallet.darkTextColor)
                    : SizedBox(width: 103 * x),
              ),
              SizedBox(width: 15 * x),
              SizedBox(
                width: snapshot['code'].toString() != snapshot['parent'].toString() ? 310 * x : 230 * x,
                child: Text(_cleanName(snapshot['name']),
                    overflow: TextOverflow.ellipsis, style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 17.0 * f)),
              ),

              // Text(snapshot['percentage'] + '%',
              //     style: TextStyle(
              //         color: ColorPallet.midGray,
              //         fontWeight: FontWeight.w700,
              //         fontSize: 17.0 * f)),
              // SizedBox(width: 2),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCategoryWidgetVertical(BuildContext context, final Map<String, dynamic> snapshot) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10 * x, vertical: 4 * y),
      width: MediaQuery.of(context).size.width * 0.85,
      height: 35 * y,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        onPressed: () {
          showNewCoicop(snapshot['code'].toString());
        },
        child: Row(
          children: <Widget>[
            Container(
                width: 15 * x,
                height: 15 * y,
                decoration: BoxDecoration(
                  color: snapshot['color'],
                  borderRadius: BorderRadius.circular(3.0 * x),
                )),
            SizedBox(width: 15 * x),
            SizedBox(
              width: 240 * x,
              child: Text(_cleanName(snapshot['name']),
                  overflow: TextOverflow.ellipsis, style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 16.0 * f)),
            ),
            Expanded(
              child: Container(),
            ),
            Text(snapshot['percentage'] != 'NaN' ? '${snapshot['percentage']}%' : '0%',
                style: TextStyle(color: ColorPallet.midGray, fontWeight: FontWeight.w700, fontSize: 16.0 * f))
          ],
        ),
      ),
    );
  }

  Widget _buildDonutChart(BuildContext context, dynamic snapshot) {
    if (snapshot == null) {
      return Center(
          child: Container(
        height: 240 * x,
        width: 240 * x,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(240 * x), border: Border.all(color: ColorPallet.midGray.withOpacity(0.2), width: 30 * x)),
      ));
    }
    if (snapshot == 1) {
      return Center(
        child: Container(
          height: 255 * x,
          width: 255 * x,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(255 * x), border: Border.all(color: ColorPallet.lightGreen, width: 28 * x)),
        ),
      );
    }
    return DonutChart(snapshot, showNewCoicop, animate: true);
  }

  Widget _buildDonutCenter(BuildContext context, dynamic snapshot) {
    if (snapshot == null) {
      return Text('0'.addCurrencyFormat(), style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w800, fontSize: 19.0 * f));
    } else {
      return Text(double.parse(snapshot.toString()).toStringAsFixed(2).addCurrencyFormat(),
          style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w800, fontSize: 19.0 * f));
    }
  }

  Widget _buildDonutChartWidget(BuildContext context, dynamic snapshot1, dynamic snapshot2) {
    return Stack(
      children: <Widget>[
        SizedBox(width: MediaQuery.of(context).size.width, height: 274 * f, child: Container(child: _buildDonutChart(context, snapshot1))),
        Center(
          child: SizedBox(
            height: 274 * f,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('${translations.text('total')}:', style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w800, fontSize: 17.0 * f)),
                SizedBox(height: 10.0 * y),
                Container(child: _buildDonutCenter(context, snapshot2)),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildTopBar(
      BuildContext context, final void Function() showFilterDrawer, final void Function() showTimeChart, final void Function() showCategoryChart) {
    return Container(
      decoration: BoxDecoration(
        color: ColorPallet.primaryColor,
        boxShadow: [BoxShadow(color: ColorPallet.veryLightGray, offset: Offset(0.0 * x, 4.0 * y), blurRadius: 2.0 * x, spreadRadius: 2.0 * x)],
      ),
      height: 80 * y, //78 * y,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(width: 22.0 * x),
              Text(
                translations.text('insightsPage'),
                style: TextStyle(color: Colors.white, fontSize: 22.0 * f, fontWeight: FontWeight.w600),
              ),
              SizedBox(width: 10 * x),
              InkWell(
                  onTap: () {
                    ParaDataScopedModel.of(context).onTap('InkWell', 'showTutorialInsights');
                    showTutorial(context);
                  },
                  child: Icon(Icons.info, key: keyButton1, color: Colors.white, size: 28.0 * x)),
              Expanded(child: Container()),
              ScopedModelDescendant<FilterState>(
                builder: (context, child, model) => InkWell(
                  key: keyButton2,
                  onTap: () {
                    ParaDataScopedModel.of(context).onTap('InkWell', 'openFilterDrawer');
                    showFilterDrawer();
                  },
                  child: getFilterIcon(model),
                ),
              ),
              SizedBox(width: 22.0 * x),
            ],
          ),
          Expanded(child: Container()),
          Row(
            key: keyButton3,
            children: <Widget>[
              InkWell(
                onTap: () {
                  showCategoryChart();
                  ParaDataScopedModel.of(context).onTap('PieChart', 'openTab');
                },
                child: SizedBox(
                  height: 41 * y,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 6.3 * y),
                      Row(
                        children: <Widget>[
                          Icon(Icons.pie_chart, color: timeChartPage ? Colors.white.withOpacity(0.6) : Colors.white, size: 20 * x),
                          SizedBox(width: 10 * x),
                          Text(translations.text('byCategory'),
                              style:
                                  TextStyle(color: timeChartPage ? Colors.white.withOpacity(0.6) : Colors.white, fontWeight: FontWeight.w600, fontSize: 14 * f))
                        ],
                      ),
                      Expanded(child: Container()),
                      Container(
                        height: 6 * y,
                        width: MediaQuery.of(context).size.width / 2,
                        color: timeChartPage ? ColorPallet.primaryColor : Colors.white,
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  showTimeChart();
                  ParaDataScopedModel.of(context).onTap('BarChart', 'openTab');
                },
                child: SizedBox(
                  height: 41 * y,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 6.3 * y),
                      Row(
                        children: <Widget>[
                          Icon(Icons.equalizer, color: timeChartPage ? Colors.white : Colors.white.withOpacity(0.6), size: 20 * x),
                          SizedBox(width: 10 * x),
                          Text(translations.text('overTime'),
                              style:
                                  TextStyle(fontSize: 14 * f, color: timeChartPage ? Colors.white : Colors.white.withOpacity(0.6), fontWeight: FontWeight.w600))
                        ],
                      ),
                      Expanded(child: Container()),
                      Container(
                        height: 6 * y,
                        width: MediaQuery.of(context).size.width / 2,
                        color: timeChartPage ? Colors.white : ColorPallet.primaryColor,
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Overview');
    return ScopedModelDescendant<Configuration>(
      builder: (_, __, configuration) {
        if (configuration.insights == InsightsConfiguration.delayed) {
          return Container(
            color: Colors.white,
            child: Center(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.75,
                child: Text(
                  translations.text('insightsDelayed'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ColorPallet.darkTextColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 20.0 * f,
                  ),
                ),
              ),
            ),
          );
        } else {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            key: _scaffoldKey,
            endDrawer: SizedBox(
              width: 290.0 * x,
              child: FliterDrawer(),
            ),
            body: ScopedModelDescendant<ReceiptListState>(builder: (_, __, ___) {
              return ScopedModelDescendant<FilterState>(
                builder: (context, child, model) => FutureBuilder(
                  future: ChartData.getData(
                    coicopId,
                    dayInPeriod,
                    period,
                    doubleToString(model.minValue),
                    doubleToString(model.maxValue),
                    dateTimeToString(model.startDate),
                    dateTimeToString(model.endDate),
                    timeChartPage,
                  ),
                  builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
                    if (!snapshot.hasData || snapshot.data.isEmpty) {
                      return Container();
                    }
                    dropdownValue = lastElement(snapshot.data[0]);
                    return Column(
                      children: <Widget>[
                        _buildTopBar(context, showFilterDrawer, showTimeChart, showCategoryChart),
                        SizedBox(height: 4 * y),
                        Column(
                          key: keyButton4,
                          children: <Widget>[
                            _buildCategoryListHorizontal(context, snapshot.data[0]),
                            _buildCategoryListVertical(context, timeChartPage ? snapshot.data[4] : snapshot.data[1]),
                          ],
                        ),
                        SizedBox(height: 15 * y),
                        Container(
                          key: keyButton5,
                          child: timeChartPage
                              ? _buildBarChartWidget(context, snapshot.data[5], snapshot.data[6], snapshot.data[7])
                              : _buildDonutChartWidget(context, snapshot.data[2], snapshot.data[3]),
                        ),
                      ],
                    );
                  },
                ),
              );
            }),
          );
        }
      },
    );
  }
}

class _CategoryListWidget extends StatefulWidget {
  final snapshot;

  const _CategoryListWidget(this.snapshot);

  @override
  State<StatefulWidget> createState() {
    return _CategoryListWidgetState();
  }
}

class _CategoryListWidgetState extends State<_CategoryListWidget> {
  ScrollController arrowsController = ScrollController();

  ScrollController _controller = ScrollController();
  bool _isScrolledToEnd = false;
  bool _isScrolledToStart;

  @override
  void initState() {
    _controller = arrowsController;
    _isScrolledToStart = _controller.initialScrollOffset == 0;
    _controller.addListener(_onScroll);
    super.initState();
  }

  void _onScroll() {
    final offset = _controller.offset;
    final minOffset = _controller.position.minScrollExtent;
    final maxOffset = _controller.position.maxScrollExtent;
    final isScrolledToEnd = offset >= maxOffset;
    final isScrolledToStart = offset <= minOffset;
    setState(
      () {
        _isScrolledToStart = true;
        _isScrolledToEnd = true;

        if (isScrolledToEnd) {
          _isScrolledToStart = false;
        }

        if (isScrolledToStart) {
          _isScrolledToEnd = false;
        }
      },
    );
  }

  List<Color> _getColors(bool isStartEnabled, bool isEndEnabled) =>
      [(isStartEnabled ? Colors.transparent : Colors.white), Colors.white, Colors.white, (isEndEnabled ? Colors.transparent : Colors.white)];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 172 * y,
      child: ShaderMask(
        shaderCallback: (bounds) => LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: const [
            0,
            0.35 * 0.5,
            1 - 0.35 * 0.5,
            1,
          ],
          colors: _getColors(0.35 > 0 && !(_isScrolledToStart ?? true), 0.35 > 0 && !(_isScrolledToEnd ?? false)),
        ).createShader(bounds.shift(Offset(-bounds.left, -bounds.top))),
        blendMode: BlendMode.dstIn,
        child: DraggableScrollbar.rrect(
          alwaysVisibleScrollThumb: widget.snapshot.length > 4,
          heightScrollThumb: widget.snapshot.length > 10 ? 50.0 : 50.0 + (10 - widget.snapshot.length) * 10.0,
          backgroundColor: ColorPallet.midGray.withOpacity(0.6),
          padding: EdgeInsets.only(right: 10.0 * x),
          controller: arrowsController,
          child: ListView.builder(
            controller: arrowsController,
            itemCount: widget.snapshot.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: <Widget>[
                  _CategoricalChartWidget(widget.snapshot[index]),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

class _CategoricalChartWidget extends StatefulWidget {
  final snapshot;

  const _CategoricalChartWidget(this.snapshot);

  @override
  __CategoricalChartWidgetState createState() => __CategoricalChartWidgetState();
}

class __CategoricalChartWidgetState extends State<_CategoricalChartWidget> {
  String _cleanName(String name) {
    final nameClean = name.indexOf(' #_#') > 0 ? name.substring(0, name.indexOf(' #_#')) : name;
    final result = nameClean.toString()[0].toUpperCase() + nameClean.toString().substring(1).toLowerCase();
    return result;
  }

  void showNewCoicop(String coicopCode) {
    setState(() {
      coicopId = coicopCode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4 * y),
      width: MediaQuery.of(context).size.width * 0.87,
      height: 35 * y,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        onPressed: () {
          showNewCoicop(widget.snapshot['code'].toString());
        },
        child: Row(
          children: <Widget>[
            Container(
                width: 15 * x,
                height: 15 * y,
                decoration: BoxDecoration(
                  color: widget.snapshot['color'],
                  borderRadius: BorderRadius.circular(3.0 * x),
                )),
            SizedBox(width: 15 * x),
            SizedBox(
              width: 240 * x,
              child: Text(_cleanName(widget.snapshot['name']),
                  overflow: TextOverflow.ellipsis, style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 16.0 * f)),
            ),
            Expanded(
              child: Container(),
            ),
            // ignore: prefer_interpolation_to_compose_strings
            Text(widget.snapshot['percentage'] != 'NaN' ? widget.snapshot['percentage'] + '%' : '0%',
                style: TextStyle(color: ColorPallet.midGray, fontWeight: FontWeight.w700, fontSize: 16.0 * f))
          ],
        ),
      ),
    );
  }
}
