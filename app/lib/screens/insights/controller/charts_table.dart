import 'dart:async';
import 'dart:math';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import '../../../core/controller/util/currency_formatter.dart';
import '../../../core/controller/util/date.dart';
import '../../../core/model/color_pallet.dart';
import '../model/tree.dart';

class ChartData {
  static Tree coicopTree;
  static bool refreshdata = true;
  static double minFilterPrice;
  static double maxFilterPrice;
  static int minFilterDate;
  static int maxFilterDate;
  static int minFoundDate;
  static int maxFoundDate;

  static void reset() {
    refreshdata = true;
  }

  static Future<void> loadChartTables() async {
    refreshdata = false;
    coicopTree = Tree.instance;
    await coicopTree.initCoicop();
    refreshdata = true;
    await coicopTree.initProducts(minFilterPrice, maxFilterPrice, minFilterDate, maxFilterDate);
  }

  static Map<int, Color> categoryColors = {
    0: ColorPallet.lightGreen,
    1: ColorPallet.orange,
    2: ColorPallet.primaryColor,
    3: ColorPallet.pink,
    4: ColorPallet.darkGreen,
    5: ColorPallet.midblue,
    6: ColorPallet.lightGreen.withOpacity(0.5),
    7: ColorPallet.orange.withOpacity(0.5),
    8: ColorPallet.primaryColor.withOpacity(0.5),
    9: ColorPallet.pink.withOpacity(0.5),
    10: ColorPallet.darkGreen.withOpacity(0.5),
    11: ColorPallet.midblue.withOpacity(0.5),
  };

  static void deterimineFilterValues(
      bool timeChartPage, DateTime dayInPeriod, String period, String minimumPrice, String maximumPrice, String startDate, String endDate) {
    if (minimumPrice == '') {
      minFilterPrice = 0.0;
    } else {
      minFilterPrice = double.parse(minimumPrice);
    }

    if (maximumPrice == '') {
      maxFilterPrice = 1000000000.0;
    } else {
      maxFilterPrice = double.parse(maximumPrice);
    }

    if (startDate == '') {
      minFilterDate = 19000101;
    } else {
      minFilterDate = DateUtil.dateStringToDateInt(startDate);
    }

    if (endDate == '') {
      maxFilterDate = 22220101;
    } else {
      maxFilterDate = DateUtil.dateStringToDateInt(endDate);
    }

    if (timeChartPage) {
      final day = DateUtil.dateTimeToDateString(dayInPeriod);
      String firstDay;
      String lastDay;
      if (period == 'week') {
        firstDay = DateUtil.firstDateOfWeek(day);
        lastDay = DateUtil.lastDateOfWeek(day);
      } else {
        firstDay = DateUtil.firstDateOfMonth(day);
        lastDay = DateUtil.lastDateOfMonth(day);
      }
      minFilterDate = max(minFilterDate, DateUtil.dateStringToDateInt(firstDay));
      maxFilterDate = min(maxFilterDate, DateUtil.dateStringToDateInt(lastDay));
    }
  }

  static dynamic determineFoundDateBoundaries(String coicopId) async {
    minFoundDate = 22220101;
    maxFoundDate = 19000101;
    for (final i in coicopTree.allItemsDated(coicopId)) {
      final dt = DateUtil.dateStringToDateInt(i.date);
      minFoundDate = min(minFoundDate, dt);
      maxFoundDate = max(maxFoundDate, dt);
    }
  }

  static Future<void> getFilteredData(
      bool timeChartPage, DateTime dayInPeriod, String period, String minimumPrice, String maximumPrice, String startDate, String endDate) async {
    deterimineFilterValues(timeChartPage, dayInPeriod, period, minimumPrice, maximumPrice, startDate, endDate);

    await loadChartTables();
  }

  static Future<List<dynamic>> getData(String coicopId, DateTime dayInPeriod, String period, String minimumPrice, String maximumPrice, String startDate,
      String endDate, bool timeChartPage) async {
    final data = <dynamic>[];

    await getFilteredData(false, dayInPeriod, period, minimumPrice, maximumPrice, startDate, endDate);
    {
      final data0 = await getAncestors(coicopId);
      while (data0.length > 1) {
        data0.removeAt(0);
      }
      data.add(data0);
      data.add(await getChildren(coicopId));
      data.add(await getDonutChartData(coicopId));
      data.add(coicopTree.nodes[coicopId].value.toString());
    }

    determineFoundDateBoundaries(coicopId);

    await getFilteredData(true, dayInPeriod, period, minimumPrice, maximumPrice, startDate, endDate);
    {
      data.add(await getChildren(coicopId));
      data.add(await getBarChartData(coicopId, dayInPeriod, period));
      data.add(minFoundDate);
      data.add(maxFoundDate);
    }

    return data;
  }

  static Map<String, dynamic> insight(Item item, String type, double totalSpend, Color color) {
    final allowed = item.value.abs();
    final percentage = totalSpend == 0.0 ? 0.0 : (allowed / totalSpend) * 100.0;
    return {
      'code': item.id,
      'name': item.description,
      'parent': item.parentId,
      'spendTotal': double.parse(item.value.toString()),
      'percentage': percentage.toStringAsFixed(0),
      'type': type,
      'color': color
    };
  }

  static Future<List<Map<String, dynamic>>> getAncestors(String coicopId) async {
    final totalSpend = coicopTree.nodes[coicopId].value;

    final category = <Map<String, dynamic>>[];

    for (final item in coicopTree.ancestors(coicopId)) {
      category.add(insight(item, 'ancestor', totalSpend, const Color(0xFFEFEFEF)));
    }

    final item = coicopTree.item(coicopId);
    category.add(insight(item, 'ancestor', totalSpend, const Color(0xFF000000)));

    category.sort((a, b) {
      return b['spendTotal'].compareTo(a['spendTotal']);
    });

    return category;
  }

  static Future<List<Map<String, dynamic>>> getChildren(String coicopId) async {
    var totalSpend = coicopTree.nodes[coicopId].value;

    final category = <Map<String, dynamic>>[];

    totalSpend = 0.0;
    for (final item in coicopTree.children(coicopId)) {
      if (item.value > 0.0) {
        totalSpend = totalSpend + item.value;
      }
    }
    for (final item in coicopTree.items(coicopId)) {
      if (item.value > 0.0) {
        totalSpend = totalSpend + item.value;
      }
    }

    for (final item in coicopTree.children(coicopId)) {
      category.add(insight(item, 'child', totalSpend, const Color(0xFFEFEFEF)));
    }

    for (final item in coicopTree.items(coicopId)) {
      category.add(insight(item, 'item', totalSpend, const Color(0xFFEFEFEF)));
    }

    category.sort((a, b) {
      return b['spendTotal'].compareTo(a['spendTotal']);
    });

    var colorCounter = 0;
    for (final category in category) {
      if (category['type'] == 'child' || category['type'] == 'item') {
        category['color'] = categoryColors[colorCounter];
        colorCounter++;
        if (colorCounter == 12) colorCounter = 0;
      }
    }

    return category;
  }

  static dynamic getDonutChartData(String coicopId) async {
    final category1Donut = await getChildren(coicopId);

    category1Donut.removeWhere((category) {
      return category['spendTotal'] == 0;
    });

    if (category1Donut.isEmpty) {
      return null;
    }

    if (category1Donut.length == 1) {
      return 1;
    }

    final data = <ExpenseCategory>[];
    for (final category in category1Donut) {
      final value = int.parse(double.parse(category['spendTotal'].toString()).toStringAsFixed(0));
      data.add(ExpenseCategory(category['name'].toString(), value.abs(), getChartColor(category['color']), category['code'],
          double.parse(category['spendTotal'].toString()).toStringAsFixed(2)));
    }

    return [
      charts.Series<ExpenseCategory, String>(
        id: 'Category',
        domainFn: (ExpenseCategory category, _) => category.categoryName,
        measureFn: (ExpenseCategory category, _) => category.moneySpent,
        data: data,
        colorFn: (ExpenseCategory category, _) => category.color,
        labelAccessorFn: (ExpenseCategory category, _) => category.label.addCurrencyFormat(),
        outsideLabelStyleAccessorFn: (ExpenseCategory category, _) => charts.TextStyleSpec(
            fontFamily: 'Source Sans Pro Bold',
            fontSize: 16.floor(), // size in Pts.
            color: getChartColor(ColorPallet.darkTextColor)),
      )
    ];
  }

  static dynamic getBarChartData(String coicopId, DateTime dayInPeriod, String period) async {
    final childColors = await getChildren(coicopId);

    final day = DateUtil.dateTimeToDateString(dayInPeriod);
    String firstDay;
    String lastDay;
    if (period == 'week') {
      firstDay = DateUtil.firstDateOfWeek(day);
      lastDay = DateUtil.lastDateOfWeek(day);
    } else {
      firstDay = DateUtil.firstDateOfMonth(day);
      lastDay = DateUtil.lastDateOfMonth(day);
    }
    final minDate = DateTime.parse(firstDay);
    final maxDate = DateTime.parse(lastDay);

    var allItemsDated = <String, Map<String, double>>{};

    var itemsFound = false;
    allItemsDated = {};
    for (final i in coicopTree.periodItemsDated(coicopId, firstDay, lastDay)) {
      itemsFound = true;
      if (!allItemsDated.containsKey(i.description)) {
        allItemsDated[i.description] = {i.date: i.value};
      } else {
        if (!allItemsDated[i.description].containsKey(i.date)) {
          allItemsDated[i.description][i.date] = i.value;
        } else {
          allItemsDated[i.description][i.date] += i.value;
        }
      }
    }

    if (!itemsFound) {
      allItemsDated[''] = {};
    }

    final allDates = <String, Map<String, double>>{};
    for (final id in allItemsDated.keys) {
      allDates[id] = {};
      var dt = minDate;
      while (dt.compareTo(maxDate) <= 0) {
        final dd = DateUtil.dateTimeToDateString(dt);
        allDates[id][dd] = 0.0;
        dt = dt.add(const Duration(days: 1));
      }
      for (final dd in allItemsDated[id].keys) {
        allDates[id][dd] = allItemsDated[id][dd];
      }
    }

    final barData = <String, List<ExpenseDate>>{};
    for (final id in allDates.keys) {
      barData[id] = [];
      for (final dd in allDates[id].keys) {
        barData[id].add(ExpenseDate(dd, allDates[id][dd]));
      }
    }

    final barChartData = <charts.Series<ExpenseDate, String>>[];
    for (final id in barData.keys) {
      Color barColor = Colors.blue;
      for (final col in childColors) {
        if (col['name'].toString() == id) {
          barColor = col['color'];
        }
      }
      barChartData.add(charts.Series<ExpenseDate, String>(
        id: id,
        domainFn: (ExpenseDate sales, _) => period == 'week' ? DateUtil.barChartWeekday(sales.date) : DateUtil.barChartMonthday(sales.date),
        measureFn: (ExpenseDate sales, _) => sales.expense,
        data: barData[id],
        colorFn: (ExpenseDate sales, _) => getChartColor(barColor),
      ));
    }

    return barChartData;
  }

  static charts.Color getChartColor(Color color) {
    return charts.Color(r: color.red, g: color.green, b: color.blue, a: color.alpha);
  }
}

class ExpenseCategory {
  ExpenseCategory(this.categoryName, this.moneySpent, this.color, this.snapshotCode, this.label);

  final String categoryName;
  final charts.Color color;
  final String label;
  final int moneySpent;
  final snapshotCode;
}

class ExpenseDate {
  ExpenseDate(this.date, this.expense);

  final String date;
  final double expense;
}
