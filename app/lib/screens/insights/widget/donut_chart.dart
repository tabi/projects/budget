import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import '../../../core/controller/util/responsive_ui.dart';

class DonutChart extends StatelessWidget {
  final bool animate;
  final List<charts.Series> seriesList;
  final Function(String code) changeCategory;

  const DonutChart(this.seriesList, this.changeCategory, {this.animate});

  @override
  Widget build(BuildContext context) {
    void _onSelectionChanged(charts.SelectionModel model) {
      if (model.selectedDatum.isNotEmpty) {
        changeCategory(model.selectedDatum[0].datum.snapshotCode);
      }
    }

    return charts.PieChart(
      seriesList,
      animate: animate,
      defaultRenderer: charts.ArcRendererConfig(
        arcWidth: (30 * x).floor(),
        arcRendererDecorators: [
          charts.ArcLabelDecorator(labelPosition: charts.ArcLabelPosition.outside),
        ],
      ),
      selectionModels: [charts.SelectionModelConfig(type: charts.SelectionModelType.info, changedListener: _onSelectionChanged)],
    );
  }
}
