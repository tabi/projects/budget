import 'package:flutter/material.dart';

import '../../../core/data/database/tables/receipt.dart';
import '../../../core/model/receipt.dart';
import '../../../core/widget/receipt_tile_widget.dart';
import '../../../features/filter_drawer/state/filter.dart';
import '../widget/date_header_widget.dart';

class ReceiptController {
  Future<List<Widget>> getAnnotatedReceipts(FilterState filterState) async {
    final receipts = await ReceiptTable().query(
      startDate: filterState.startDate?.millisecondsSinceEpoch,
      endDate: filterState.endDate?.millisecondsSinceEpoch,
    );

    final annotadedReceipts = <Widget>[];
    int _lastDay;
    final minValue = filterState.minValue ?? double.infinity * -1;
    final maxValue = filterState.maxValue ?? double.infinity;

    for (final receipt in receipts) {
      final currentDay = receipt.date.day;
      final _totalReceiptValue = receipt.products.getTotalPrice();
      if (isNotFiltered(filterState.searchInput, receipt)) {
        if ((_totalReceiptValue <= maxValue) && (_totalReceiptValue >= minValue)) {
          if (_lastDay != currentDay) {
            _lastDay = currentDay;
            annotadedReceipts.add(DateHeaderWidget(receipt.date));
          }
          annotadedReceipts.add(ReceiptTileWidget(receipt));
        }
      }
    }
    return annotadedReceipts;
  }
}

bool isNotFiltered(String searchInput, Receipt receipt) {
  if (searchInput == null || searchInput == '') {
    return true;
  }

  searchInput = searchInput.toLowerCase();

  if (receipt.store.name.toLowerCase().contains(searchInput)) {
    return true;
  }

  for (final product in receipt.products.content) {
    if (product.name.toLowerCase().contains(searchInput)) {
      return true;
    }
  }
  return false;
}
