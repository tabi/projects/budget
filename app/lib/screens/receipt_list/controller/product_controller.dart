import '../../../core/data/database/tables/receipt_product.dart';
import '../../../core/model/receipt_product.dart';

class ProductController {
  Future<List<ReceiptProduct>> getProducts(String receiptId) async {
    return ReceiptProductTable().query(receiptId: receiptId);
  }
}
