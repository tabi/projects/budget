import 'package:flutter/material.dart';

import '../../../../core/controller/util/responsive_ui.dart';
import '../../../../core/model/category_icon_map.dart';
import '../../../../core/model/color_pallet.dart';

class ReceiptUtil {
  Icon getStoreIcon(String storeType) {
    if (IconMap.icon.containsKey(storeType)) {
      return Icon(
        IconMap.icon[storeType][0],
        color: ColorPallet.darkTextColor,
        size: IconMap.icon[storeType][2] ? 19 * x : 25 * x,
      );
    }
    return Icon(Icons.store, size: 25 * x, color: ColorPallet.darkTextColor);
  }
}
