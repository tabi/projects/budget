import 'package:flutter/material.dart';

import '../../features/filter_drawer/filter_drawer.dart';
import '../../features/filter_drawer/state/filter.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/spotlight_tutorial/controller/receipt_list_tutorial.dart';
import 'widget/receipt_list_widget.dart';
import 'widget/top_bar_widget.dart';

class ReceiptListScreen extends StatelessWidget with ParaDataName {
  @override
  String get name => 'ReceiptListScreen';

  @override
  Widget build(BuildContext context) {
    initTargets(keyButton1, keyButton2, keyButton3);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: FilterState.of(context).scaffoldKey,
      endDrawer: FliterDrawer(),
      body: Column(
        children: <Widget>[
          TopBarWidget(),
          ReceiptListWidget(),
        ],
      ),
    );
  }
}
