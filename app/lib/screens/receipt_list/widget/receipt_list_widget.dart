import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/widget/no_receipt_image.dart';
import '../../../features/filter_drawer/state/filter.dart';
import '../controller/receipt_controller.dart';
import '../state/receipt_list_state.dart';

class ReceiptListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<FilterState>(builder: (_, __, filterState) {
      return ScopedModelDescendant<ReceiptListState>(builder: (_, __, receiptListState) {
        return FutureBuilder<List<Widget>>(
          future: ReceiptController().getAnnotatedReceipts(filterState),
          builder: (_, snapshot) {
            if (snapshot?.data == null) {
              return Container();
            }
            if (snapshot.data.isEmpty) {
              return NoReceiptImage();
            }
            return Expanded(
              child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 6.0 * x, vertical: 6 * y),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return snapshot.data[index];
                },
              ),
            );
          },
        );
      });
    });
  }
}
