import '../../../core/controller/util/date.dart';
import '../../../core/data/database/tables/receipt.dart';
import '../../../core/model/receipt.dart';

class ReceiptController {
  Future<List<Receipt>> getReceipts(DateTime date) async {
    final startOfDay = DateUtil.getNormalizedDayStart(date).subtract(const Duration(seconds: 1));
    final endOfDay = startOfDay.add(const Duration(days: 1));
    final receipts = await ReceiptTable().query(startDate: startOfDay.millisecondsSinceEpoch, endDate: endOfDay.millisecondsSinceEpoch);
    return receipts;
  }
}
