import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/database/database_helper.dart';
import '../model/international.dart';

/*
Scoped model used to carry the current language setting across the app
*/

class LanguageSetting extends Model {
  LanguageSetting() {
    intializeLanguage();
  }

  Future<void> intializeLanguage() async {
    final country = International.countryFromCode(ui.window.locale.languageCode);
    final prefs = await SharedPreferences.getInstance();
    //_language =  prefs.getString('languagePreference') ?? 'en';
    _language = prefs.getString('languagePreference') ?? country.languageId;
  }

  String _language = 'nl';
  static String key = 'nl';
  static String _tablePreference = 'nl';

  set language(String language) {
    _language = language;
    key = language;
    notifyListeners();
  }

  String get language => _language;

  static set tablePreference(tablePreference) {
    _tablePreference = tablePreference;
  }

  static String get tablePreference {
    if (_tablePreference == 'itemKey' || _tablePreference == 'pageKey') {
      return 'en';
    }
    return _tablePreference;
  }

  static LanguageSetting of(BuildContext context) => ScopedModel.of<LanguageSetting>(context);
}

/*
Class from which translations can be requested
*/

class Translations {
  Translations(this.context, this.pageKey);

  BuildContext context;
  String pageKey;

  String text(String itemKey, {bool ignoreWarning = false, String language}) {
    if (LanguageSetting.key == 'pageKey') {
      return '$pageKey*';
    }
    if (LanguageSetting.key == 'itemKey') {
      return '$itemKey*';
    }
    language = language ?? LanguageSetting.of(context).language;
    if (language == null) {
      return ignoreWarning ? itemKey : '4*$itemKey';
    }
    language = language.toUpperCase();

    //Language is not yet defined
    if (!_languages.contains(language)) {
      return ignoreWarning ? itemKey : '4*$itemKey';
    }
    //PageKey is not yet defined
    if (!_pageKeys.contains('$language:$pageKey')) {
      return ignoreWarning ? itemKey : '3*$itemKey';
    }
    //ItemKey is nog yet definited
    if (!_words.containsKey('$language:$pageKey:$itemKey')) {
      return ignoreWarning ? itemKey : '2*$itemKey';
    }

    final translation = _words['$language:$pageKey:$itemKey'];

    //Key is not yet translations.text
    if (translation == 'null' || translation == '') {
      return ignoreWarning ? itemKey : '1*$itemKey';
    }
    //Language, page, key and translation are defined
    if (translation.contains('{{')) {
      return _interpolation(language, translation);
    } else {
      return translation.replaceAll('\\n', '\n');
    }
  }

  static String textStatic(String itemKey, String pageKey) {
    if (LanguageSetting.key == 'pageKey') {
      return '$pageKey*';
    }
    if (LanguageSetting.key == 'itemKey') {
      return '$itemKey*';
    }
    final translations = Translations(null, pageKey);
    return translations.text(itemKey, language: LanguageSetting.key);
  }

  String _interpolation(String language, String str) {
    return str.splitMapJoin(RegExp('{{([^{}]+)}}'), onMatch: (m) => _interpolationWords['$language:${m.group(0)}']);
  }
}

/*
Initialize word map from database. 
*/

List<String> _languages = [];
List<String> _pageKeys = [];
Map<String, String> _words = {};
Map<String, String> _interpolationWords = {};

Future<void> initializeTranslations() async {
  final db = await DatabaseHelper.instance.database;
  final List<Map<String, dynamic>> translationTbl = await db.rawQuery('SELECT * FROM tblTranslate');
  for (final row in translationTbl) {
    final language = row['language'].toString();
    final pageKey = row['pageKey'].toString();
    final itemKey = row['itemKey'].toString();
    final translation = row['translation'].toString();

    if (itemKey.contains('{{')) {
      _interpolationWords['$language:$itemKey'] = translation;
    } else {
      if (!_languages.contains(language)) {
        _languages.add(language);
      }
      if (!_pageKeys.contains(pageKey)) {
        _pageKeys.add('$language:$pageKey');
      }
      _words['$language:$pageKey:$itemKey'] = translation;
    }
  }
}
