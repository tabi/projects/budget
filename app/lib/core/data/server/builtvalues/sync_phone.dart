import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'sync_phone.g.dart';

abstract class SyncPhone implements Built<SyncPhone, SyncPhoneBuilder> {
  static Serializer<SyncPhone> get serializer => _$syncPhoneSerializer;

  int get id;
  int get userId;
  String get name;
  int get syncOrder;

  factory SyncPhone([Function(SyncPhoneBuilder b) updates]) = _$SyncPhone;

  SyncPhone._();

  factory SyncPhone.newInstance(String name) {
    return SyncPhone((b) => b
      ..id = -1
      ..userId = -1
      ..name = name
      ..syncOrder = 0);
  }

  factory SyncPhone.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
