// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_register_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncRegisterBody> _$syncRegisterBodySerializer =
    new _$SyncRegisterBodySerializer();

class _$SyncRegisterBodySerializer
    implements StructuredSerializer<SyncRegisterBody> {
  @override
  final Iterable<Type> types = const [SyncRegisterBody, _$SyncRegisterBody];
  @override
  final String wireName = 'SyncRegisterBody';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncRegisterBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'phoneInfos',
      serializers.serialize(object.phoneInfos,
          specifiedType:
              const FullType(BuiltList, const [const FullType(SyncPhoneInfo)])),
    ];

    return result;
  }

  @override
  SyncRegisterBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncRegisterBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'phoneInfos':
          result.phoneInfos.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SyncPhoneInfo)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncRegisterBody extends SyncRegisterBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final BuiltList<SyncPhoneInfo> phoneInfos;

  factory _$SyncRegisterBody(
          [void Function(SyncRegisterBodyBuilder) updates]) =>
      (new SyncRegisterBodyBuilder()..update(updates)).build();

  _$SyncRegisterBody._({this.user, this.phone, this.phoneInfos}) : super._() {
    BuiltValueNullFieldError.checkNotNull(user, 'SyncRegisterBody', 'user');
    BuiltValueNullFieldError.checkNotNull(phone, 'SyncRegisterBody', 'phone');
    BuiltValueNullFieldError.checkNotNull(
        phoneInfos, 'SyncRegisterBody', 'phoneInfos');
  }

  @override
  SyncRegisterBody rebuild(void Function(SyncRegisterBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncRegisterBodyBuilder toBuilder() =>
      new SyncRegisterBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncRegisterBody &&
        user == other.user &&
        phone == other.phone &&
        phoneInfos == other.phoneInfos;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, user.hashCode), phone.hashCode), phoneInfos.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncRegisterBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('phoneInfos', phoneInfos))
        .toString();
  }
}

class SyncRegisterBodyBuilder
    implements Builder<SyncRegisterBody, SyncRegisterBodyBuilder> {
  _$SyncRegisterBody _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  ListBuilder<SyncPhoneInfo> _phoneInfos;
  ListBuilder<SyncPhoneInfo> get phoneInfos =>
      _$this._phoneInfos ??= new ListBuilder<SyncPhoneInfo>();
  set phoneInfos(ListBuilder<SyncPhoneInfo> phoneInfos) =>
      _$this._phoneInfos = phoneInfos;

  SyncRegisterBodyBuilder();

  SyncRegisterBodyBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _user = $v.user.toBuilder();
      _phone = $v.phone.toBuilder();
      _phoneInfos = $v.phoneInfos.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncRegisterBody other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncRegisterBody;
  }

  @override
  void update(void Function(SyncRegisterBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncRegisterBody build() {
    _$SyncRegisterBody _$result;
    try {
      _$result = _$v ??
          new _$SyncRegisterBody._(
              user: user.build(),
              phone: phone.build(),
              phoneInfos: phoneInfos.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();
        _$failedField = 'phoneInfos';
        phoneInfos.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncRegisterBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
