import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_phone.dart';
import 'sync_start_questionnaire.dart';
import 'sync_sync.dart';
import 'sync_user.dart';

part 'sync_start_questionnaire_body.g.dart';

abstract class SyncStartQuestionnaireBody implements Built<SyncStartQuestionnaireBody, SyncStartQuestionnaireBodyBuilder> {
  static Serializer<SyncStartQuestionnaireBody> get serializer => _$syncStartQuestionnaireBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  SyncStartQuestionnaire get startQuestionnaire;

  factory SyncStartQuestionnaireBody([Function(SyncStartQuestionnaireBodyBuilder b) updates]) = _$SyncStartQuestionnaireBody;

  SyncStartQuestionnaireBody._();

  factory SyncStartQuestionnaireBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
