import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_phone.dart';
import 'sync_search_product.dart';
import 'sync_sync.dart';
import 'sync_user.dart';

part 'sync_push_search_product_body.g.dart';

abstract class SyncPushSearchProductBody implements Built<SyncPushSearchProductBody, SyncPushSearchProductBodyBuilder> {
  static Serializer<SyncPushSearchProductBody> get serializer => _$syncPushSearchProductBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  SyncSearchProduct get searchProduct;

  factory SyncPushSearchProductBody([Function(SyncPushSearchProductBodyBuilder b) updates]) = _$SyncPushSearchProductBody;

  SyncPushSearchProductBody._();

  factory SyncPushSearchProductBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
