// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_paradata_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncParadataBody> _$syncParadataBodySerializer =
    new _$SyncParadataBodySerializer();

class _$SyncParadataBodySerializer
    implements StructuredSerializer<SyncParadataBody> {
  @override
  final Iterable<Type> types = const [SyncParadataBody, _$SyncParadataBody];
  @override
  final String wireName = 'SyncParadataBody';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncParadataBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'paradatas',
      serializers.serialize(object.paradatas,
          specifiedType:
              const FullType(BuiltList, const [const FullType(ParaData)])),
    ];

    return result;
  }

  @override
  SyncParadataBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncParadataBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'paradatas':
          result.paradatas.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ParaData)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncParadataBody extends SyncParadataBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final int syncOrder;
  @override
  final SyncSync synchronisation;
  @override
  final BuiltList<ParaData> paradatas;

  factory _$SyncParadataBody(
          [void Function(SyncParadataBodyBuilder) updates]) =>
      (new SyncParadataBodyBuilder()..update(updates)).build();

  _$SyncParadataBody._(
      {this.user,
      this.phone,
      this.syncOrder,
      this.synchronisation,
      this.paradatas})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(user, 'SyncParadataBody', 'user');
    BuiltValueNullFieldError.checkNotNull(phone, 'SyncParadataBody', 'phone');
    BuiltValueNullFieldError.checkNotNull(
        syncOrder, 'SyncParadataBody', 'syncOrder');
    BuiltValueNullFieldError.checkNotNull(
        synchronisation, 'SyncParadataBody', 'synchronisation');
    BuiltValueNullFieldError.checkNotNull(
        paradatas, 'SyncParadataBody', 'paradatas');
  }

  @override
  SyncParadataBody rebuild(void Function(SyncParadataBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncParadataBodyBuilder toBuilder() =>
      new SyncParadataBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncParadataBody &&
        user == other.user &&
        phone == other.phone &&
        syncOrder == other.syncOrder &&
        synchronisation == other.synchronisation &&
        paradatas == other.paradatas;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, user.hashCode), phone.hashCode), syncOrder.hashCode),
            synchronisation.hashCode),
        paradatas.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncParadataBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('syncOrder', syncOrder)
          ..add('synchronisation', synchronisation)
          ..add('paradatas', paradatas))
        .toString();
  }
}

class SyncParadataBodyBuilder
    implements Builder<SyncParadataBody, SyncParadataBodyBuilder> {
  _$SyncParadataBody _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  ListBuilder<ParaData> _paradatas;
  ListBuilder<ParaData> get paradatas =>
      _$this._paradatas ??= new ListBuilder<ParaData>();
  set paradatas(ListBuilder<ParaData> paradatas) =>
      _$this._paradatas = paradatas;

  SyncParadataBodyBuilder();

  SyncParadataBodyBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _user = $v.user.toBuilder();
      _phone = $v.phone.toBuilder();
      _syncOrder = $v.syncOrder;
      _synchronisation = $v.synchronisation.toBuilder();
      _paradatas = $v.paradatas.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncParadataBody other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncParadataBody;
  }

  @override
  void update(void Function(SyncParadataBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncParadataBody build() {
    _$SyncParadataBody _$result;
    try {
      _$result = _$v ??
          new _$SyncParadataBody._(
              user: user.build(),
              phone: phone.build(),
              syncOrder: BuiltValueNullFieldError.checkNotNull(
                  syncOrder, 'SyncParadataBody', 'syncOrder'),
              synchronisation: synchronisation.build(),
              paradatas: paradatas.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();

        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'paradatas';
        paradatas.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncParadataBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
