import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'sync_start_questionnaire.g.dart';

abstract class SyncStartQuestionnaire implements Built<SyncStartQuestionnaire, SyncStartQuestionnaireBuilder> {
  static Serializer<SyncStartQuestionnaire> get serializer => _$syncStartQuestionnaireSerializer;

  String get results;

  factory SyncStartQuestionnaire([Function(SyncStartQuestionnaireBuilder b) updates]) = _$SyncStartQuestionnaire;

  SyncStartQuestionnaire._();

  factory SyncStartQuestionnaire.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
