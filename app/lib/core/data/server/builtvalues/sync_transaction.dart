import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'sync_transaction.g.dart';

abstract class SyncTransaction implements Built<SyncTransaction, SyncTransactionBuilder> {
  static Serializer<SyncTransaction> get serializer => _$syncTransactionSerializer;

  int get date;
  String get discountAmount;
  String get discountPercentage;
  String get discountText;
  String get expenseAbroad;
  String get expenseOnline;
  String get receiptLocation;
  String get receiptProductType;
  String get store;
  String get storeType;
  double get totalPrice;
  String get transactionID;

  factory SyncTransaction([Function(SyncTransactionBuilder b) updates]) = _$SyncTransaction;

  SyncTransaction._();

  factory SyncTransaction.newInstance(String transactionId) {
    return SyncTransaction((b) => b
      ..date = 0
      ..discountAmount = ''
      ..discountPercentage = ''
      ..discountText = ''
      ..expenseAbroad = ''
      ..expenseOnline = ''
      ..receiptLocation = ''
      ..receiptProductType = ''
      ..store = ''
      ..storeType = ''
      ..totalPrice = 0
      ..transactionID = transactionId);
  }

  factory SyncTransaction.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
