// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_product.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncProduct> _$syncProductSerializer = new _$SyncProductSerializer();

class _$SyncProductSerializer implements StructuredSerializer<SyncProduct> {
  @override
  final Iterable<Type> types = const [SyncProduct, _$SyncProduct];
  @override
  final String wireName = 'SyncProduct';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncProduct object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'price',
      serializers.serialize(object.price,
          specifiedType: const FullType(double)),
      'product',
      serializers.serialize(object.product,
          specifiedType: const FullType(String)),
      'productCategory',
      serializers.serialize(object.productCategory,
          specifiedType: const FullType(String)),
      'productCode',
      serializers.serialize(object.productCode,
          specifiedType: const FullType(String)),
      'productDate',
      serializers.serialize(object.productDate,
          specifiedType: const FullType(String)),
      'productGroupID',
      serializers.serialize(object.productGroupID,
          specifiedType: const FullType(String)),
      'transactionID',
      serializers.serialize(object.transactionID,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncProduct deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncProductBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'price':
          result.price = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'product':
          result.product = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'productCategory':
          result.productCategory = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'productCode':
          result.productCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'productDate':
          result.productDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'productGroupID':
          result.productGroupID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'transactionID':
          result.transactionID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncProduct extends SyncProduct {
  @override
  final double price;
  @override
  final String product;
  @override
  final String productCategory;
  @override
  final String productCode;
  @override
  final String productDate;
  @override
  final String productGroupID;
  @override
  final String transactionID;

  factory _$SyncProduct([void Function(SyncProductBuilder) updates]) =>
      (new SyncProductBuilder()..update(updates)).build();

  _$SyncProduct._(
      {this.price,
      this.product,
      this.productCategory,
      this.productCode,
      this.productDate,
      this.productGroupID,
      this.transactionID})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(price, 'SyncProduct', 'price');
    BuiltValueNullFieldError.checkNotNull(product, 'SyncProduct', 'product');
    BuiltValueNullFieldError.checkNotNull(
        productCategory, 'SyncProduct', 'productCategory');
    BuiltValueNullFieldError.checkNotNull(
        productCode, 'SyncProduct', 'productCode');
    BuiltValueNullFieldError.checkNotNull(
        productDate, 'SyncProduct', 'productDate');
    BuiltValueNullFieldError.checkNotNull(
        productGroupID, 'SyncProduct', 'productGroupID');
    BuiltValueNullFieldError.checkNotNull(
        transactionID, 'SyncProduct', 'transactionID');
  }

  @override
  SyncProduct rebuild(void Function(SyncProductBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncProductBuilder toBuilder() => new SyncProductBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncProduct &&
        price == other.price &&
        product == other.product &&
        productCategory == other.productCategory &&
        productCode == other.productCode &&
        productDate == other.productDate &&
        productGroupID == other.productGroupID &&
        transactionID == other.transactionID;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, price.hashCode), product.hashCode),
                        productCategory.hashCode),
                    productCode.hashCode),
                productDate.hashCode),
            productGroupID.hashCode),
        transactionID.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncProduct')
          ..add('price', price)
          ..add('product', product)
          ..add('productCategory', productCategory)
          ..add('productCode', productCode)
          ..add('productDate', productDate)
          ..add('productGroupID', productGroupID)
          ..add('transactionID', transactionID))
        .toString();
  }
}

class SyncProductBuilder implements Builder<SyncProduct, SyncProductBuilder> {
  _$SyncProduct _$v;

  double _price;
  double get price => _$this._price;
  set price(double price) => _$this._price = price;

  String _product;
  String get product => _$this._product;
  set product(String product) => _$this._product = product;

  String _productCategory;
  String get productCategory => _$this._productCategory;
  set productCategory(String productCategory) =>
      _$this._productCategory = productCategory;

  String _productCode;
  String get productCode => _$this._productCode;
  set productCode(String productCode) => _$this._productCode = productCode;

  String _productDate;
  String get productDate => _$this._productDate;
  set productDate(String productDate) => _$this._productDate = productDate;

  String _productGroupID;
  String get productGroupID => _$this._productGroupID;
  set productGroupID(String productGroupID) =>
      _$this._productGroupID = productGroupID;

  String _transactionID;
  String get transactionID => _$this._transactionID;
  set transactionID(String transactionID) =>
      _$this._transactionID = transactionID;

  SyncProductBuilder();

  SyncProductBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _price = $v.price;
      _product = $v.product;
      _productCategory = $v.productCategory;
      _productCode = $v.productCode;
      _productDate = $v.productDate;
      _productGroupID = $v.productGroupID;
      _transactionID = $v.transactionID;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncProduct other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncProduct;
  }

  @override
  void update(void Function(SyncProductBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncProduct build() {
    final _$result = _$v ??
        new _$SyncProduct._(
            price: BuiltValueNullFieldError.checkNotNull(
                price, 'SyncProduct', 'price'),
            product: BuiltValueNullFieldError.checkNotNull(
                product, 'SyncProduct', 'product'),
            productCategory: BuiltValueNullFieldError.checkNotNull(
                productCategory, 'SyncProduct', 'productCategory'),
            productCode: BuiltValueNullFieldError.checkNotNull(
                productCode, 'SyncProduct', 'productCode'),
            productDate: BuiltValueNullFieldError.checkNotNull(
                productDate, 'SyncProduct', 'productDate'),
            productGroupID: BuiltValueNullFieldError.checkNotNull(
                productGroupID, 'SyncProduct', 'productGroupID'),
            transactionID: BuiltValueNullFieldError.checkNotNull(
                transactionID, 'SyncProduct', 'transactionID'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
