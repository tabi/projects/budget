// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_transaction.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncTransaction> _$syncTransactionSerializer =
    new _$SyncTransactionSerializer();

class _$SyncTransactionSerializer
    implements StructuredSerializer<SyncTransaction> {
  @override
  final Iterable<Type> types = const [SyncTransaction, _$SyncTransaction];
  @override
  final String wireName = 'SyncTransaction';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncTransaction object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'date',
      serializers.serialize(object.date, specifiedType: const FullType(int)),
      'discountAmount',
      serializers.serialize(object.discountAmount,
          specifiedType: const FullType(String)),
      'discountPercentage',
      serializers.serialize(object.discountPercentage,
          specifiedType: const FullType(String)),
      'discountText',
      serializers.serialize(object.discountText,
          specifiedType: const FullType(String)),
      'expenseAbroad',
      serializers.serialize(object.expenseAbroad,
          specifiedType: const FullType(String)),
      'expenseOnline',
      serializers.serialize(object.expenseOnline,
          specifiedType: const FullType(String)),
      'receiptLocation',
      serializers.serialize(object.receiptLocation,
          specifiedType: const FullType(String)),
      'receiptProductType',
      serializers.serialize(object.receiptProductType,
          specifiedType: const FullType(String)),
      'store',
      serializers.serialize(object.store,
          specifiedType: const FullType(String)),
      'storeType',
      serializers.serialize(object.storeType,
          specifiedType: const FullType(String)),
      'totalPrice',
      serializers.serialize(object.totalPrice,
          specifiedType: const FullType(double)),
      'transactionID',
      serializers.serialize(object.transactionID,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncTransaction deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncTransactionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'discountAmount':
          result.discountAmount = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'discountPercentage':
          result.discountPercentage = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'discountText':
          result.discountText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'expenseAbroad':
          result.expenseAbroad = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'expenseOnline':
          result.expenseOnline = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'receiptLocation':
          result.receiptLocation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'receiptProductType':
          result.receiptProductType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'store':
          result.store = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'storeType':
          result.storeType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'totalPrice':
          result.totalPrice = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'transactionID':
          result.transactionID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncTransaction extends SyncTransaction {
  @override
  final int date;
  @override
  final String discountAmount;
  @override
  final String discountPercentage;
  @override
  final String discountText;
  @override
  final String expenseAbroad;
  @override
  final String expenseOnline;
  @override
  final String receiptLocation;
  @override
  final String receiptProductType;
  @override
  final String store;
  @override
  final String storeType;
  @override
  final double totalPrice;
  @override
  final String transactionID;

  factory _$SyncTransaction([void Function(SyncTransactionBuilder) updates]) =>
      (new SyncTransactionBuilder()..update(updates)).build();

  _$SyncTransaction._(
      {this.date,
      this.discountAmount,
      this.discountPercentage,
      this.discountText,
      this.expenseAbroad,
      this.expenseOnline,
      this.receiptLocation,
      this.receiptProductType,
      this.store,
      this.storeType,
      this.totalPrice,
      this.transactionID})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(date, 'SyncTransaction', 'date');
    BuiltValueNullFieldError.checkNotNull(
        discountAmount, 'SyncTransaction', 'discountAmount');
    BuiltValueNullFieldError.checkNotNull(
        discountPercentage, 'SyncTransaction', 'discountPercentage');
    BuiltValueNullFieldError.checkNotNull(
        discountText, 'SyncTransaction', 'discountText');
    BuiltValueNullFieldError.checkNotNull(
        expenseAbroad, 'SyncTransaction', 'expenseAbroad');
    BuiltValueNullFieldError.checkNotNull(
        expenseOnline, 'SyncTransaction', 'expenseOnline');
    BuiltValueNullFieldError.checkNotNull(
        receiptLocation, 'SyncTransaction', 'receiptLocation');
    BuiltValueNullFieldError.checkNotNull(
        receiptProductType, 'SyncTransaction', 'receiptProductType');
    BuiltValueNullFieldError.checkNotNull(store, 'SyncTransaction', 'store');
    BuiltValueNullFieldError.checkNotNull(
        storeType, 'SyncTransaction', 'storeType');
    BuiltValueNullFieldError.checkNotNull(
        totalPrice, 'SyncTransaction', 'totalPrice');
    BuiltValueNullFieldError.checkNotNull(
        transactionID, 'SyncTransaction', 'transactionID');
  }

  @override
  SyncTransaction rebuild(void Function(SyncTransactionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncTransactionBuilder toBuilder() =>
      new SyncTransactionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncTransaction &&
        date == other.date &&
        discountAmount == other.discountAmount &&
        discountPercentage == other.discountPercentage &&
        discountText == other.discountText &&
        expenseAbroad == other.expenseAbroad &&
        expenseOnline == other.expenseOnline &&
        receiptLocation == other.receiptLocation &&
        receiptProductType == other.receiptProductType &&
        store == other.store &&
        storeType == other.storeType &&
        totalPrice == other.totalPrice &&
        transactionID == other.transactionID;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, date.hashCode),
                                                discountAmount.hashCode),
                                            discountPercentage.hashCode),
                                        discountText.hashCode),
                                    expenseAbroad.hashCode),
                                expenseOnline.hashCode),
                            receiptLocation.hashCode),
                        receiptProductType.hashCode),
                    store.hashCode),
                storeType.hashCode),
            totalPrice.hashCode),
        transactionID.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncTransaction')
          ..add('date', date)
          ..add('discountAmount', discountAmount)
          ..add('discountPercentage', discountPercentage)
          ..add('discountText', discountText)
          ..add('expenseAbroad', expenseAbroad)
          ..add('expenseOnline', expenseOnline)
          ..add('receiptLocation', receiptLocation)
          ..add('receiptProductType', receiptProductType)
          ..add('store', store)
          ..add('storeType', storeType)
          ..add('totalPrice', totalPrice)
          ..add('transactionID', transactionID))
        .toString();
  }
}

class SyncTransactionBuilder
    implements Builder<SyncTransaction, SyncTransactionBuilder> {
  _$SyncTransaction _$v;

  int _date;
  int get date => _$this._date;
  set date(int date) => _$this._date = date;

  String _discountAmount;
  String get discountAmount => _$this._discountAmount;
  set discountAmount(String discountAmount) =>
      _$this._discountAmount = discountAmount;

  String _discountPercentage;
  String get discountPercentage => _$this._discountPercentage;
  set discountPercentage(String discountPercentage) =>
      _$this._discountPercentage = discountPercentage;

  String _discountText;
  String get discountText => _$this._discountText;
  set discountText(String discountText) => _$this._discountText = discountText;

  String _expenseAbroad;
  String get expenseAbroad => _$this._expenseAbroad;
  set expenseAbroad(String expenseAbroad) =>
      _$this._expenseAbroad = expenseAbroad;

  String _expenseOnline;
  String get expenseOnline => _$this._expenseOnline;
  set expenseOnline(String expenseOnline) =>
      _$this._expenseOnline = expenseOnline;

  String _receiptLocation;
  String get receiptLocation => _$this._receiptLocation;
  set receiptLocation(String receiptLocation) =>
      _$this._receiptLocation = receiptLocation;

  String _receiptProductType;
  String get receiptProductType => _$this._receiptProductType;
  set receiptProductType(String receiptProductType) =>
      _$this._receiptProductType = receiptProductType;

  String _store;
  String get store => _$this._store;
  set store(String store) => _$this._store = store;

  String _storeType;
  String get storeType => _$this._storeType;
  set storeType(String storeType) => _$this._storeType = storeType;

  double _totalPrice;
  double get totalPrice => _$this._totalPrice;
  set totalPrice(double totalPrice) => _$this._totalPrice = totalPrice;

  String _transactionID;
  String get transactionID => _$this._transactionID;
  set transactionID(String transactionID) =>
      _$this._transactionID = transactionID;

  SyncTransactionBuilder();

  SyncTransactionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _date = $v.date;
      _discountAmount = $v.discountAmount;
      _discountPercentage = $v.discountPercentage;
      _discountText = $v.discountText;
      _expenseAbroad = $v.expenseAbroad;
      _expenseOnline = $v.expenseOnline;
      _receiptLocation = $v.receiptLocation;
      _receiptProductType = $v.receiptProductType;
      _store = $v.store;
      _storeType = $v.storeType;
      _totalPrice = $v.totalPrice;
      _transactionID = $v.transactionID;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncTransaction other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncTransaction;
  }

  @override
  void update(void Function(SyncTransactionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncTransaction build() {
    final _$result = _$v ??
        new _$SyncTransaction._(
            date: BuiltValueNullFieldError.checkNotNull(
                date, 'SyncTransaction', 'date'),
            discountAmount: BuiltValueNullFieldError.checkNotNull(
                discountAmount, 'SyncTransaction', 'discountAmount'),
            discountPercentage: BuiltValueNullFieldError.checkNotNull(
                discountPercentage, 'SyncTransaction', 'discountPercentage'),
            discountText: BuiltValueNullFieldError.checkNotNull(
                discountText, 'SyncTransaction', 'discountText'),
            expenseAbroad: BuiltValueNullFieldError.checkNotNull(
                expenseAbroad, 'SyncTransaction', 'expenseAbroad'),
            expenseOnline: BuiltValueNullFieldError.checkNotNull(
                expenseOnline, 'SyncTransaction', 'expenseOnline'),
            receiptLocation: BuiltValueNullFieldError.checkNotNull(
                receiptLocation, 'SyncTransaction', 'receiptLocation'),
            receiptProductType: BuiltValueNullFieldError.checkNotNull(
                receiptProductType, 'SyncTransaction', 'receiptProductType'),
            store: BuiltValueNullFieldError.checkNotNull(store, 'SyncTransaction', 'store'),
            storeType: BuiltValueNullFieldError.checkNotNull(storeType, 'SyncTransaction', 'storeType'),
            totalPrice: BuiltValueNullFieldError.checkNotNull(totalPrice, 'SyncTransaction', 'totalPrice'),
            transactionID: BuiltValueNullFieldError.checkNotNull(transactionID, 'SyncTransaction', 'transactionID'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
