import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_phone.dart';
import 'sync_phone_info.dart';
import 'sync_user.dart';

part 'sync_register_body.g.dart';

abstract class SyncRegisterBody implements Built<SyncRegisterBody, SyncRegisterBodyBuilder> {
  static Serializer<SyncRegisterBody> get serializer => _$syncRegisterBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  BuiltList<SyncPhoneInfo> get phoneInfos;

  factory SyncRegisterBody([Function(SyncRegisterBodyBuilder b) updates]) = _$SyncRegisterBody;

  SyncRegisterBody._();

  factory SyncRegisterBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
