// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_group_info.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncGroupInfo> _$syncGroupInfoSerializer =
    new _$SyncGroupInfoSerializer();

class _$SyncGroupInfoSerializer implements StructuredSerializer<SyncGroupInfo> {
  @override
  final Iterable<Type> types = const [SyncGroupInfo, _$SyncGroupInfo];
  @override
  final String wireName = 'SyncGroupInfo';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncGroupInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'groupId',
      serializers.serialize(object.groupId, specifiedType: const FullType(int)),
      'key',
      serializers.serialize(object.key, specifiedType: const FullType(String)),
      'value',
      serializers.serialize(object.value,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncGroupInfo deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncGroupInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'groupId':
          result.groupId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'key':
          result.key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncGroupInfo extends SyncGroupInfo {
  @override
  final int id;
  @override
  final int groupId;
  @override
  final String key;
  @override
  final String value;

  factory _$SyncGroupInfo([void Function(SyncGroupInfoBuilder) updates]) =>
      (new SyncGroupInfoBuilder()..update(updates)).build();

  _$SyncGroupInfo._({this.id, this.groupId, this.key, this.value}) : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'SyncGroupInfo', 'id');
    BuiltValueNullFieldError.checkNotNull(groupId, 'SyncGroupInfo', 'groupId');
    BuiltValueNullFieldError.checkNotNull(key, 'SyncGroupInfo', 'key');
    BuiltValueNullFieldError.checkNotNull(value, 'SyncGroupInfo', 'value');
  }

  @override
  SyncGroupInfo rebuild(void Function(SyncGroupInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncGroupInfoBuilder toBuilder() => new SyncGroupInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncGroupInfo &&
        id == other.id &&
        groupId == other.groupId &&
        key == other.key &&
        value == other.value;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), groupId.hashCode), key.hashCode),
        value.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncGroupInfo')
          ..add('id', id)
          ..add('groupId', groupId)
          ..add('key', key)
          ..add('value', value))
        .toString();
  }
}

class SyncGroupInfoBuilder
    implements Builder<SyncGroupInfo, SyncGroupInfoBuilder> {
  _$SyncGroupInfo _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  int _groupId;
  int get groupId => _$this._groupId;
  set groupId(int groupId) => _$this._groupId = groupId;

  String _key;
  String get key => _$this._key;
  set key(String key) => _$this._key = key;

  String _value;
  String get value => _$this._value;
  set value(String value) => _$this._value = value;

  SyncGroupInfoBuilder();

  SyncGroupInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _groupId = $v.groupId;
      _key = $v.key;
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncGroupInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncGroupInfo;
  }

  @override
  void update(void Function(SyncGroupInfoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncGroupInfo build() {
    final _$result = _$v ??
        new _$SyncGroupInfo._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'SyncGroupInfo', 'id'),
            groupId: BuiltValueNullFieldError.checkNotNull(
                groupId, 'SyncGroupInfo', 'groupId'),
            key: BuiltValueNullFieldError.checkNotNull(
                key, 'SyncGroupInfo', 'key'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'SyncGroupInfo', 'value'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
