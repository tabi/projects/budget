import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../../../features/para_data/para_data_table.dart';
import '../../../screens/search/controller/add_search_suggestion.dart';
import '../server/sync_db.dart';
import 'tables/questionnaire_multi_table.dart';
import 'tables/questionnaire_normal_table.dart';
import 'tables/questionnaire_person_info_table.dart';
import 'tables/questionnaire_single_table.dart';
import 'tables/receipt.dart';
import 'tables/receipt_product.dart';
import 'tables/user_progress.dart';

class DatabaseHelper {
  DatabaseHelper._();

  static final DatabaseHelper instance = DatabaseHelper._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  Future<Database> _initDatabase() async {
    final path = await _getDatabasePath();
    await _importDatabase();
    return openDatabase(path);
  }

  Future<String> _getDatabasePath() async {
    final databasesDir = await getApplicationDocumentsDirectory();
    final path = join(databasesDir.path, 'database.db');
    return path;
  }

  Future<void> _importDatabase() async {
    final path = await _getDatabasePath();
    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      final data = await rootBundle.load(join('assets', 'database.db'));
      final List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes);
      _database = await openDatabase(path);
      await ReceiptProductTable().createTable();
      await ReceiptTable().createTable();
      await UserProgressTable().createTable();
      await QuestionnaireNormalTable.createTable();
      await QuestionnaireSingleTable.createTable();
      await QuestionnaireMultiTable.createTable();
      await QuestionnairePersonInfoTable.createTable();
      await ParaDataTable(_database).initialize();
      //await SyncDatabase.createTables(_database);
      final _syncDatabase = SyncDatabase();
      await _syncDatabase.createTables(_database);

      await SearchSuggestions.createTables(_database);
    }
  }
}
