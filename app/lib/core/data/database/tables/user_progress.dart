import '../../../model/user_progress.dart';
import '../database_helper.dart';

class UserProgressTable {
  static String tableName = 'tbl_user_progress';

  Future<void> createTable() async {
    final db = await DatabaseHelper.instance.database;
    await db.execute('''
          CREATE TABLE $tableName (
            date INT,
            complete INT)
          ''');
  }

  Future<void> insert(UserProgress userProgress) async {
    final db = await DatabaseHelper.instance.database;
    await db.insert(tableName, userProgress.toMap());
  }

  Future<List<UserProgress>> query() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> maps = await db.query(tableName);
    final userProgressList = <UserProgress>[];
    for (final map in maps) {
      userProgressList.add(UserProgress.fromMap(map));
    }
    return userProgressList;
  }

  Future<UserProgress> queryDate(DateTime date) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> maps = await db.query(
      tableName,
      where: 'date = ?',
      whereArgs: [date.millisecondsSinceEpoch],
    );
    if (maps.isNotEmpty) {
      return UserProgress.fromMap(maps[0]);
    } else {
      return null;
    }
  }

  Future<void> update(DateTime date, UserProgress userProgress) async {
    final db = await DatabaseHelper.instance.database;
    await db.update(tableName, userProgress.toMap(), where: 'date = ?', whereArgs: [userProgress.toMap()['date']]);
  }
}
