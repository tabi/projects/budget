import 'dart:convert';

import 'package:budget_onderzoek/core/data/database/tables/questionnaire_single_table.dart';

import 'questionnaire_multi_table.dart';
import 'questionnaire_normal_table.dart';
import 'questionnaire_person_info_table.dart';

Future<String> getQuestionnaireExport() async {
  final export = <String, dynamic>{};
  export['mutliTable'] = await QuestionnaireMultiTable().queryForExport();
  export['normalTable'] = await QuestionnaireNormalTable().queryForExport();
  export['personInfoTable'] = await QuestionnairePersonInfoTable().queryForExport();
  export['singleTable'] = await QuestionnaireSingleTable().queryForExport();
  return jsonEncode(export);
}
