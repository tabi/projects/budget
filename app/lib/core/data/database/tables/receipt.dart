import '../../../model/receipt.dart';
import '../../server/builtvalues/sync_transaction.dart';
import '../database_helper.dart';
import 'receipt_product.dart';

class ReceiptTable {
  String tableName = 'tbl_receipt';

  Future<void> createTable() async {
    final db = await DatabaseHelper.instance.database;
    await db.execute('''
          CREATE TABLE $tableName (
            id TEXT,
            date INT,
            storeName TEXT,
            storeCategory TEXT,
            isAbroad INT,
            isOnline INT,
            discountAmount REAL,
            discountPercentage REAL,
            imagePath TEXT)
          ''');
  }

  Future<void> insert(Receipt receipt) async {
    final db = await DatabaseHelper.instance.database;
    await db.insert(tableName, receipt.toMap());
    for (final product in receipt.products.content) {
      await ReceiptProductTable().insert(product, receipt.id);
    }
  }

  Future<void> delete(Receipt receipt) async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(tableName, where: 'id = ?', whereArgs: [receipt.id]);
    for (final product in receipt.products.content) {
      await ReceiptProductTable().delete(product);
    }
  }

  Future<List<Receipt>> query({
    String id,
    int startDate,
    int endDate,
    String searchInput,
  }) async {
    final db = await DatabaseHelper.instance.database;
    final query = _createQuery(
      id: id,
      startDate: startDate,
      endDate: endDate,
      searchInput: searchInput,
    );
    final List<Map<String, dynamic>> maps = await db.rawQuery(query);
    final receipts = <Receipt>[];
    for (final map in maps) {
      receipts.add(await Receipt.fromMap(map));
    }
    return receipts;
  }

  String _createQuery({
    String id,
    int startDate,
    int endDate,
    String searchInput,
  }) {
    final whereClauses = <String>[];
    var innerJoin = '';
    var groupBy = '';

    if (id != null) {
      whereClauses.add('id = "$id"');
    }
    if (startDate != null) {
      whereClauses.add('date >= $startDate');
    }
    if (endDate != null) {
      whereClauses.add('date <= $endDate');
    }
    if (searchInput != null) {
      if (searchInput != '') {
        innerJoin = 'INNER JOIN ${ReceiptProductTable().tableName} ON $tableName.transactionID = ${ReceiptProductTable().tableName}.transactionID';
        groupBy = 'GROUP BY $tableName.transactionID';
        whereClauses.add('(product LIKE "%$searchInput%" OR store LIKE "%$searchInput%" OR storeType LIKE "%$searchInput%")');
      }
    }

    var query = 'SELECT  * FROM $tableName $innerJoin';
    var joiner = ' WHERE ';
    for (final where in whereClauses) {
      query += joiner + where;
      joiner = ' AND ';
    }
    query = '$query $groupBy ORDER BY date DESC';
    return query;
  }

  //This is a temporary solution to fit the datamodel on device with the datamodel on the backend, as there is not enough time to implement this properly..
  static Future<List<Map>> syncQuery(String id) async {
    final db = await DatabaseHelper.instance.database;
    final maps = await db.rawQuery('SELECT * FROM tbl_receipt WHERE id = "$id"');
    final results = <Map<String, dynamic>>[];
    for (final Map row in maps) {
      final _row = <String, dynamic>{};
      _row['date'] = (row['date'] / 1000.0).round();
      _row['discountAmount'] = (row['discountAmount'] as double).toString();
      _row['discountPercentage'] = (row['discountPercentage'] as double).toString();
      _row['discountText'] = '';
      _row['expenseAbroad'] = row['isAbroad'] == 0 ? 'false' : 'true';
      _row['expenseOnline'] = row['isOnline'] == 0 ? 'false' : 'true';
      _row['receiptLocation'] = row['imagePath'] ?? '';
      _row['receiptProductType'] = '';
      _row['store'] = row['storeName'] as String;
      _row['storeType'] = row['storeCategory'] as String;
      _row['totalPrice'] = 0.0;
      _row['transactionID'] = row['id'] as String;
      results.add(_row);
    }
    return results;
  }

  Future<void> insertFromBackend(SyncTransaction syncTransaction) async {
    final db = await DatabaseHelper.instance.database;
    final backendData = syncTransaction.toJson();
    final insert = <String, dynamic>{};
    insert['date'] = backendData['date'] * 1000;
    insert['discountAmount'] = double.tryParse(backendData['discountAmount'] as String) ?? 0.0;
    insert['discountPercentage'] = double.tryParse(backendData['discountPercentage'] as String) ?? 0.0;
    insert['isAbroad'] = backendData['expenseAbroad'] == 'false' ? 0 : 1;
    insert['isOnline'] = backendData['expenseOnline'] == 'false' ? 0 : 1;
    insert['imagePath'] = backendData['receiptLocation'] == '' ? null : backendData['receiptLocation'];
    insert['storeName'] = backendData['store'];
    insert['storeCategory'] = backendData['storeType'];
    insert['id'] = backendData['transactionID'];
    await db.insert('tbl_receipt', insert);
  }
}
