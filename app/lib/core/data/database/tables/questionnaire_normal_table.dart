import 'package:questionnaire/questionnaire.dart';

import '../database_helper.dart';

class QuestionnaireNormalTable {
  static const tableName = 'tbl_questionnaire_normal';

  static Future<void> createTable() async {
    final database = await DatabaseHelper.instance.database;
    final result = await database.query('sqlite_master', where: 'name = ?', whereArgs: [tableName]);
    if (result.isEmpty) {
      await database.execute('''
          CREATE TABLE $tableName (
            questionNumber INT,
            answer TEXT)
          ''');
    }
  }

  Future<void> insert(NormalResult result) async {
    final database = await DatabaseHelper.instance.database;
    await database.insert(tableName, result.toJson());
  }

  Future<List<NormalResult>> query() async {
    final database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> maps = await database.query(tableName);
    final normalQuestionList = <NormalResult>[];
    for (final map in maps) {
      normalQuestionList.add(NormalResult.fromJson(map));
    }
    return normalQuestionList;
  }

  Future<bool> hasEntry() async {
    final database = await DatabaseHelper.instance.database;
    final _result = await database.query(tableName);
    return _result.isNotEmpty;
  }

  Future<List<Map<String, dynamic>>> queryForExport() async => await (await DatabaseHelper.instance.database).query(tableName);
}
