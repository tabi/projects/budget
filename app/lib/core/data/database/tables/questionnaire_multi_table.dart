import 'package:questionnaire/questionnaire.dart';

import '../database_helper.dart';

class QuestionnaireMultiTable {
  static const tableName = 'tbl_questionnaire_multi';

  static Future<void> createTable() async {
    final database = await DatabaseHelper.instance.database;
    final result = await database.query('sqlite_master', where: 'name = ?', whereArgs: [tableName]);
    if (result.isEmpty) {
      await database.execute('''
          CREATE TABLE $tableName (
            questionNumber INT,
            resultList TEXT)
          ''');
    }
  }

  Future<void> insert(MultiChoiceResult result) async {
    final database = await DatabaseHelper.instance.database;
    await database.insert(tableName, result.toJson());
  }

  Future<List<MultiChoiceResult>> query() async {
    final database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> maps = await database.query(tableName);
    final multiQuestionList = <MultiChoiceResult>[];
    for (final map in maps) {
      multiQuestionList.add(MultiChoiceResult.fromJson(map));
    }
    return multiQuestionList;
  }

  Future<bool> hasEntry() async {
    final database = await DatabaseHelper.instance.database;
    final _result = await database.query(tableName);
    return _result.isNotEmpty;
  }

  Future<List<Map<String, dynamic>>> queryForExport() async => await (await DatabaseHelper.instance.database).query(tableName);
}
