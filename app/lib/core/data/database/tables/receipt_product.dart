import '../../../model/receipt_product.dart';
import '../../../state/translations.dart';
import '../../server/builtvalues/sync_product.dart';
import '../database_helper.dart';

class ReceiptProductTable {
  String tableName = 'tbl_receipt_product';

  Future<void> createTable() async {
    final db = await DatabaseHelper.instance.database;
    await db.execute('''
          CREATE TABLE $tableName (
            receiptId TEXT,
            id TEXT,
            count INT,
            isReturn INT,
            hasDiscount INT,
            discount REAL,
            name TEXT,
            category TEXT,
            coicop TEXT,
            price REAL,
            productDate TEXT)
          ''');
  }

  Future<void> insert(ReceiptProduct product, String receiptId) async {
    final db = await DatabaseHelper.instance.database;
    await db.insert(tableName, product.toMap(receiptId));
  }

  Future<void> delete(ReceiptProduct product) async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(tableName, where: 'receiptId = ?', whereArgs: [product.receiptId]);
  }

  Future<List<ReceiptProduct>> query({String receiptId}) async {
    final db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> maps;
    if (receiptId != null) {
      maps = await db.query(
        tableName,
        where: 'receiptId = ?',
        whereArgs: [receiptId],
      );
    } else {
      maps = await db.query(tableName);
    }
    final content = <ReceiptProduct>[];
    for (final map in maps) {
      content.add(ReceiptProduct.fromMap(map));
    }
    return content;
  }

  Future<String> getCoicop(String category) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> maps =
        await db.query('tblProduct_${LanguageSetting.tablePreference}', where: 'Coicop = ?', whereArgs: [category], columns: ['code']);
    if (maps != null) {
      if (maps.isNotEmpty) {
        return maps[0]['code'] as String;
      } else {
        final List<Map<String, dynamic>> queryResultCoicop =
            await db.query('tblCoicop_${LanguageSetting.tablePreference}', where: 'Coicop = ?', whereArgs: [category], columns: ['code']);
        return queryResultCoicop[0]['code'] as String;
      }
    } else {
      return '';
    }
  }

  //This is a temporary solution to fit the datamodel on device with the datamodel on the backend, as there is not enough time to implement this properly..
  static Future<List<Map<String, dynamic>>> syncQuery(String receiptId) async {
    final db = await DatabaseHelper.instance.database;
    final maps = await db.rawQuery('SELECT * FROM tbl_receipt_product WHERE receiptId = "$receiptId"');
    final results = <Map<String, dynamic>>[];
    for (final Map row in maps) {
      final _row = <String, dynamic>{};
      var price = row['price'] as double;
      final discount = (row['discount'] ?? 0) as double;
      final isReturn = !(row['isReturn'] == 0);

      if (discount > price) {
        price = 0.0;
      } else {
        price = price - discount;
      }

      if (isReturn) {
        price = price * -1;
      }

      _row['price'] = price;
      _row['product'] = row['name'] as String;
      _row['productCategory'] = row['category'] as String;
      _row['productCode'] = row['coicop'] as String;
      _row['productDate'] = row['productDate'] as String;
      _row['productGroupID'] = row['receiptId'] as String;
      _row['transactionID'] = row['id'] as String;

      final count = row['count'] as int;

      for (var i = 0; i < count; i++) {
        results.add(_row);
      }
    }
    return results;
  }

  Future<void> insertFromBackend(SyncProduct syncProduct) async {
    final db = await DatabaseHelper.instance.database;
    final backendData = syncProduct.toJson();
    final insert = <String, dynamic>{};
    insert['price'] = backendData['price'];
    insert['name'] = backendData['product'];
    insert['category'] = backendData['productCategory'];
    insert['coicop'] = backendData['productCode'];
    insert['productDate'] = backendData['productDate'];
    insert['receiptId'] = backendData['productGroupID'];
    insert['id'] = backendData['transactionID'];
    insert['count'] = 1;
    insert['isReturn'] = 0;
    insert['hasDiscount'] = 0;
    insert['discount'] = 0.0;
    await db.insert('tbl_receipt_product', insert);
  }
}
