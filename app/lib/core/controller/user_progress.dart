import '../data/database/tables/user_progress.dart';
import '../model/enum/date_status.dart';
import '../model/progress_lists.dart';
import '../model/user_progress.dart';
import 'util/date.dart';

class UserProgressController {
  Future<void> initDaysOfExperiment(int days) async {
    final now = DateTime.now();
    for (var i = 0; i <= days; i++) {
      final normalizedDate = DateUtil.getNormalizedDayStart(now.add(Duration(days: i)));
      final userProgress = UserProgress(normalizedDate, false);
      await UserProgressTable().insert(userProgress);
    }
  }

  Future<bool> isInitialized() async {
    final userProgressList = await UserProgressTable().query();
    if (userProgressList.isNotEmpty) {
      return true;
    }
    return false;
  }

  Future<void> setDateStatus(DateTime date, bool isComplete) async {
    final userProgress = UserProgress(date, isComplete);
    await UserProgressTable().update(date, userProgress);
  }

  Future<DateStatus> getDateStatus(DateTime date) async {
    final userProgress = await UserProgressTable().queryDate(date);
    if (userProgress == null) {
      return DateStatus.notInExperiment;
    }
    if (userProgress.isComplete) {
      return DateStatus.alreadyComplete;
    }
    if (date.isBefore(DateTime.now())) {
      return DateStatus.canComplete;
    } else {
      return DateStatus.inFuture;
    }
  }

  Future<ProgressLists> getProgressLists() async {
    final daysInExperiment = <DateTime>[];
    final daysCompleted = <DateTime>[];
    final daysMissing = <DateTime>[];
    final daysRemaining = <DateTime>[];

    final userProgressList = await UserProgressTable().query();
    for (final userProgress in userProgressList) {
      daysInExperiment.add(userProgress.date);
      if (userProgress.isComplete) {
        daysCompleted.add(userProgress.date);
      } else {
        if (DateTime.now().isAfter(userProgress.date)) {
          daysMissing.add(userProgress.date);
        }
      }
      if (DateTime.now().isBefore(userProgress.date)) {
        daysRemaining.add(userProgress.date);
      }
    }
    return ProgressLists(daysInExperiment, daysCompleted, daysMissing, daysRemaining);
  }
}
