class InputUtil {
  //Remove all characters except for numbers
  static String valueSanitizer(String _str) {
    final str = _str.replaceAll(RegExp(','), '.').replaceAll(RegExp('[^0-9.,]+'), '');

    try {
      if (str == '') {
        return '';
      }
      double.parse(str).toString();
      return str;
    } catch (e) {
      return null;
    }
  }

  //Remove everything except letters and spaces
  //Makes sure that the string is formatted to have the first letter upper case and the rest lower case.
  static String textSanitizer(String _str) {
    final str = _str.replaceAll(RegExp('[^a-zA-Z 0-9\x80-\xFF]+'), '');
    if (str.length > 1) {
      return '${str[0].toUpperCase()}${str.substring(1).toLowerCase()}';
    } else {
      return str;
    }
  }

  static String sanitize(String _str) {
    return _str.replaceAll(RegExp('[^a-zA-Z 0-9\x80-\xFF]+'), '');
  }

  //Needs to match a CoiCop classification.
  static bool textValidator(String str) {
    if (str is String) {
      return true;
    }
    return false;
  }

  static bool intValidator(String str) {
    if (str == null) {
      return false;
    }

    if (str.replaceAll(RegExp('[^0-9]+'), '').length == str.length) {
      if (int.parse(str) >= 0) {
        return true;
      }
    }
    return false;
  }

  //Make sure that the value is a positive double
  static bool valueValidator(String str) {
    str = str.replaceAll(',', '.');
    if (str == null) {
      return false;
    }

    if (str.substring(0, 1) == '-') {
      str = str = str.substring(1, str.length);
    }

    if (str.length == 1) {
      if (str[0] == '.') {
        return true;
      }
    }

    if (str.length >= 2) {
      if (str[0] == '0' && str[1] != '.') {
        return false;
      }
    }

    if (str.isEmpty) {
      return true;
    }

    if (str.contains(' ')) {
      return false;
    }

    try {
      final value = double.parse(str);
      if (value >= -10000000 && value <= 10000000) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static bool percentageValidator(String str) {
    if (str == null) {
      return false;
    }

    final value = double.parse(str);
    try {
      if (value >= 0 && value <= 100) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}
