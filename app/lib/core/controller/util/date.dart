import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';

import '../../model/international.dart';
import '../../state/translations.dart';

class DateUtil {
  static DateTime getNormalizedDayStart(DateTime date) {
    final startOfDay = DateTime.parse(DateFormat('yyyy-MM-dd').format(date));
    return startOfDay;
  }

  static bool isSameDay(DateTime d1, DateTime d2) {
    if (getNormalizedDayStart(d1) == getNormalizedDayStart(d2)) {
      return true;
    } else {
      return false;
    }
  }

  static String getDateString(DateTime date) {
    return DateFormat('yyyy-MM-dd').format(date);
  }

  static String dateTimeToDateString(DateTime dt) {
    return dt.toString().substring(0, 10);
  }

  static int dateTimeToDateInt(DateTime dt) {
    return dateStringToDateInt(dateTimeToDateString(dt));
  }

  static int dateStringToDateInt(String date) {
    return int.parse(date.replaceAll('-', ''));
  }

  static String dateIntToDateString(int date) {
    final dd = date.toString();
    final result = '${dd.substring(0, 4)}-${dd.substring(4, 6)}-${dd.substring(6, 8)}';
    return result;
  }

  static DateTime dateStringToDateTime(String date) {
    final y = int.parse(date.substring(0, 4));
    final m = int.parse(date.substring(5, 7));
    final d = int.parse(date.substring(8, 10));
    return DateTime(y, m, d);
  }

  static DateTime dateIntToDateTime(int date) {
    return dateStringToDateTime(dateIntToDateString(date));
  }

  static String dayOfWeek(int weekday) {
    var result = '';
    switch (weekday) {
      case 1:
        result = Translations.textStatic('monday', 'Calendar');
        break;
      case 2:
        result = Translations.textStatic('tuesday', 'Calendar');
        break;
      case 3:
        result = Translations.textStatic('wednesday', 'Calendar');
        break;
      case 4:
        result = Translations.textStatic('thursday', 'Calendar');
        break;
      case 5:
        result = Translations.textStatic('friday', 'Calendar');
        break;
      case 6:
        result = Translations.textStatic('saturday', 'Calendar');
        break;
      case 7:
        result = Translations.textStatic('sunday', 'Calendar');
        break;
      default:
        result = Translations.textStatic('monday', 'Calendar');
    }
    return result;
  }

  static String firstDateOfMonth(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 1 - dt.day));
    return dateTimeToDateString(dt);
  }

  static String lastDateOfMonth(String date) {
    DateTime dt;
    dt = DateTime.parse(firstDateOfMonth(date));
    dt = dt.add(const Duration(days: 35));
    dt = DateTime.parse(firstDateOfMonth(dateTimeToDateString(dt)));
    dt = dt.add(const Duration(hours: -1));
    return dateTimeToDateString(dt);
  }

  static String firstDateOfWeek(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 1 - dt.weekday));
    return dateTimeToDateString(dt);
  }

  static String lastDateOfWeek(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 7 - dt.weekday));
    return dateTimeToDateString(dt);
  }

  static String monthString(DateTime dt) {
    final result = DateFormat.MMMM(International.languageFromId(LanguageSetting.key).localeKey).format(DateTime(dt.year, dt.month));
    return result[0].toUpperCase() + result.substring(1);
  }

  static bool nextPeriodExists(DateTime nextDate, int absMinDate, int absMaxDate, String period, int swipe) {
    final dd = dateTimeToDateString(nextDate);
    DateTime firstDay;
    DateTime lastDay;

    if (period == 'week') {
      firstDay = DateTime.parse(firstDateOfWeek(dd));
      lastDay = DateTime.parse(lastDateOfWeek(dd));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dd));
      lastDay = DateTime.parse(lastDateOfMonth(dd));
    }

    if (absMinDate > absMaxDate) {
      return false;
    }
    if (swipe == -1 && absMinDate > dateStringToDateInt(dateTimeToDateString(lastDay))) {
      return false;
    }
    if (swipe == 1 && absMaxDate < dateStringToDateInt(dateTimeToDateString(firstDay))) {
      return false;
    }
    return true;
  }

  static String barChartTitle(DateTime dayInPeriod, String period) {
    String result;
    final dd = dateTimeToDateString(dayInPeriod);
    DateTime firstDay;
    DateTime lastDay;

    if (period == 'week') {
      firstDay = DateTime.parse(firstDateOfWeek(dd));
      lastDay = DateTime.parse(lastDateOfWeek(dd));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dd));
      lastDay = DateTime.parse(lastDateOfMonth(dd));
    }

    if (period == 'week') {
      result = '${firstDay.day}/${firstDay.month} - ${lastDay.day}/${lastDay.month}';
    } else {
      result = '${monthString(lastDay)} ${lastDay.year}';
    }

    return result;
  }

  static String barChartWeekday(String date) {
    final dt = DateTime.parse(date);
    return '${DateUtil.dayOfWeek(dt.weekday).substring(0, 2)}-${dt.day}';
  }

  static String barChartMonthday(String date) {
    return DateTime.parse(date).day.toString();
  }

  static List<charts.TickSpec<String>> barChartTickSpec(DateTime dayInPeriod, String period) {
    final result = <charts.TickSpec<String>>[];

    final labels = barChartLabels(dayInPeriod, period);

    for (final lab in labels.keys) {
      result.add(charts.TickSpec(lab, label: labels[lab]));
    }

    return result;
  }

  static Map<String, String> barChartLabels(DateTime dayInPeriod, String period) {
    final labels = <String, String>{};

    final dd = dateTimeToDateString(dayInPeriod);
    DateTime firstDay;
    DateTime lastDay;

    if (period == 'week') {
      firstDay = DateTime.parse(firstDateOfWeek(dd));
      lastDay = DateTime.parse(lastDateOfWeek(dd));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dd));
      lastDay = DateTime.parse(lastDateOfMonth(dd));
    }

    var dt = firstDay;
    var tick = 1;
    while (dt.compareTo(lastDay) <= 0) {
      if (period == 'week') {
        final lab = barChartWeekday(dateTimeToDateString(dt));
        labels[lab] = barChartWeekday(dateTimeToDateString(dt)); //dayOfWeek(tick);
      } else {
        final lab = barChartMonthday(dateTimeToDateString(dt));
        labels[lab] = tick == 1 || tick % 5 == 0 ? tick.toString() : '';
      }
      tick++;
      dt = dt.add(const Duration(days: 1));
    }

    return labels;
  }

  static String questionnaireDateRepresentation(DateTime dt) {
    return '${DateUtil.dayOfWeek(dt.weekday)} ${dt.day} ${DateUtil.monthString(dt)} ${dt.year}';
  }
}
