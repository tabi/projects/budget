import '../../state/translations.dart';

extension AddCurrencyFormat on String {
  String addCurrencyFormat() {
    final currencySymbol = Translations.textStatic('currencySymbol', 'CurrencySetting');
    if (Translations.textStatic('currencyAffix', 'CurrencySetting') == 'postfix') {
      return '${this.replaceAll('.', ',')} $currencySymbol';
    } else {
      return '$currencySymbol${this.replaceAll('.', ',')}';
    }
  }
}
