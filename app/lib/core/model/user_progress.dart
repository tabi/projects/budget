class UserProgress {
  DateTime date;
  bool isComplete;

  UserProgress(this.date, this.isComplete);

  Map<String, dynamic> toMap() {
    return {'date': date.millisecondsSinceEpoch, 'complete': isComplete ? 1 : 0};
  }

  static UserProgress fromMap(Map<String, dynamic> map) {
    return UserProgress(
      DateTime.fromMillisecondsSinceEpoch(map['date']),
      map['complete'] == 0 ? false : true,
    );
  }
}
