import 'package:uuid/uuid.dart';

import '../../screens/insights/controller/util/type_converter.dart';

class ReceiptProduct {
  String id = const Uuid().v1();
  int count = 1;
  bool isReturn = false;
  bool hasDiscount = false;
  String name, category, coicop, receiptId;
  double price;
  double discount;
  String productDate; //added for backward compatiblity with old insight_screen methods

  ReceiptProduct(
      {String id,
      int count,
      bool isReturn,
      String productDate,
      bool hasDiscount,
      this.name,
      this.category,
      this.coicop,
      this.price,
      this.receiptId,
      this.discount})
      : this.id = id ?? const Uuid().v1(),
        this.count = count ?? 1,
        this.isReturn = isReturn ?? false,
        this.hasDiscount = hasDiscount ?? false,
        this.productDate = productDate ?? dateTimeToString(DateTime.now());

  ReceiptProduct.empty();

  double getTotalPrice() {
    if (isReturn) {
      return count * getPrice() * -1;
    }
    return count * getPrice();
  }

  double getPrice() {
    if (price == null) {
      return 0;
    }
    if (discount == null) {
      return price;
    }
    if (hasDiscount == false) {
      return price;
    }
    if (discount > price) {
      return 0;
    }
    return price - discount;
  }

  void subtractItem() {
    if (count > 1) {
      count--;
    }
  }

  void addItem() {
    count++;
  }

  bool isComplete() {
    return price != null;
  }

  Map<String, dynamic> toMap(String receiptId) {
    return {
      'receiptId': receiptId,
      'id': id,
      'count': count,
      'isReturn': isReturn ? 1 : 0,
      'hasDiscount': hasDiscount ? 1 : 0,
      'discount': discount ?? 0,
      'name': name,
      'category': category,
      'coicop': coicop,
      'price': price,
      'productDate': productDate,
    };
  }

  static ReceiptProduct fromMap(Map<String, dynamic> map) {
    return ReceiptProduct(
        receiptId: map['receiptId'],
        id: map['id'],
        count: map['count'],
        isReturn: map['isReturn'] == 0 ? false : true,
        hasDiscount: map['hasDiscount'] == 0 ? false : true,
        discount: map['discount'],
        name: map['name'],
        category: map['category'],
        coicop: map['coicop'],
        price: map['price'],
        productDate: map['productDate']);
  }
}
