import 'package:uuid/uuid.dart';

import '../data/database/tables/receipt_product.dart';
import 'location.dart';
import 'products.dart';
import 'store.dart';

class Receipt {
  String id;
  DateTime date;
  Location location;
  Store store;
  Products products;
  String imagePath;

  Receipt(this.id, this.date, this.location, this.store, this.products, [this.imagePath]);

  Receipt.empty() {
    id = const Uuid().v1();
    date = DateTime.now();
    location = Location.empty();
    store = Store.empty();
    products = Products.empty();
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'date': date.millisecondsSinceEpoch,
      'storeName': store.name,
      'storeCategory': store.category,
      'isAbroad': location.isAbroad ? 1 : 0,
      'isOnline': location.isOnline ? 1 : 0,
      'discountAmount': products.discountAmount,
      'discountPercentage': products.discountPercentage,
      'imagePath': imagePath,
    };
  }

  static Future<Receipt> fromMap(Map<String, dynamic> map) async {
    final location = Location(
      map['isAbroad'] == 0 ? false : true,
      map['isOnline'] == 0 ? false : true,
    );
    final content = await ReceiptProductTable().query(receiptId: map['id']);
    final products = Products(
      map['discountAmount'],
      map['discountPercentage'],
      content,
    );
    final store = Store(
      map['storeName'],
      map['storeCategory'],
    );
    return Receipt(
      map['id'],
      DateTime.fromMillisecondsSinceEpoch(map['date']),
      location,
      store,
      products,
      map['imagePath'],
    );
  }
}
