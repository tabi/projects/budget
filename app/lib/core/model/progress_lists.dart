class ProgressLists {
  List<DateTime> daysInExperiment, daysCompleted, daysMissing, daysRemaining;

  ProgressLists(this.daysInExperiment, this.daysCompleted, this.daysMissing, this.daysRemaining);
}
