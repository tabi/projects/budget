import 'package:flutter/material.dart';
import 'package:questionnaire/questionnaire.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../core/data/database/tables/questionnaire_multi_table.dart';
import '../../core/data/database/tables/questionnaire_normal_table.dart';
import '../../core/data/database/tables/questionnaire_person_info_table.dart';
import '../../core/data/database/tables/questionnaire_single_table.dart';

class StartQuestionnaireModel extends Model {
  static StartQuestionnaireModel of(BuildContext context) => ScopedModel.of<StartQuestionnaireModel>(context);

  QuestionnaireNormalTable _tableNormal;
  QuestionnaireSingleTable _tableSingle;
  QuestionnaireMultiTable _tableMulti;
  QuestionnairePersonInfoTable _tablePersonInfo;

  StartQuestionnaireModel() {
    _tableNormal = QuestionnaireNormalTable();
    _tableSingle = QuestionnaireSingleTable();
    _tableMulti = QuestionnaireMultiTable();
    _tablePersonInfo = QuestionnairePersonInfoTable();
  }

  Future<bool> hasStartQuestionnaireEntry() async {
    return await _tableNormal.hasEntry() || await _tableSingle.hasEntry() || await _tableMulti.hasEntry() || await _tablePersonInfo.hasEntry();
  }

  Future<void> insertNormal(NormalResult result) async {
    await _tableNormal.insert(result);
  }

  Future<void> insertSingle(SingleChoiceResult result) async {
    await _tableSingle.insert(result);
  }

  Future<void> insertMulti(MultiChoiceResult result) async {
    await _tableMulti.insert(result);
  }

  Future<void> insertPersonInfo(PersonInfoResult result) async {
    await _tablePersonInfo.insert(result);
  }
}
