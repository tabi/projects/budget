import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import '../../core/data/server/builtvalues/serializers.dart';

part 'para_data.g.dart';

abstract class ParaData implements Built<ParaData, ParaDataBuilder> {
  static Serializer<ParaData> get serializer => _$paraDataSerializer;

  DateTime get timestamp;
  String get objectName;
  String get action;

  factory ParaData([Function(ParaDataBuilder b) updates]) = _$ParaData;

  ParaData._();

  factory ParaData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
