import 'package:flutter/widgets.dart';

import 'para_data_scoped_model.dart';

class ParaDataNavigatorObserver extends NavigatorObserver {
  final ParaDataScopedModel model;

  ParaDataNavigatorObserver(this.model);

  @override
  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) {
    print('didPop: $route');
    model.closeScreen(route.settings.name);
    // TODO: implement didPop
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic> previousRoute) {
    print('didPush: $route');
    model.openScreen(route.settings.name);
    // TODO: implement didPush
  }

  @override
  void didRemove(Route<dynamic> route, Route<dynamic> previousRoute) {
    print('didRemove: $route');
    model.closeScreen(route.settings.name);
  }

  @override
  void didReplace({Route<dynamic> newRoute, Route<dynamic> oldRoute}) {
    print('didReplace: $newRoute');
    model.closeScreen(oldRoute.settings.name);
    model.openScreen(newRoute.settings.name);
  }

  @override
  void didStartUserGesture(Route<dynamic> route, Route<dynamic> previousRoute) {
    // TODO: implement didStartUserGesture
  }

  @override
  void didStopUserGesture() {
    // TODO: implement didStopUserGesture
  }
}
