import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

import '../../core/controller/util/input.dart';
import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/date_range.dart';
import '../../core/model/international.dart';
import '../../core/model/value_range.dart';
import '../../core/state/translations.dart';
import '../../screens/receipt_list/state/receipt_list_state.dart';
import '../para_data/para_data_scoped_model.dart';
import 'controller/filter.dart';
import 'state/filter.dart';

Translations translations;

class FliterDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    translations = Translations(context, 'Filter');
    return SizedBox(
      width: 295.0 * x,
      child: Drawer(
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  height: 50.0 * y,
                  color: ColorPallet.primaryColor,
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 25.0 * x),
                      Text(
                        translations.text('filterExpenses'),
                        style: TextStyle(color: Colors.white, fontSize: 22.0 * f, fontWeight: FontWeight.w600),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.only(left: 8.0 * x, top: 8 * y, bottom: 8 * y),
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 30.0 * x,
                          ),
                        ),
                      ),
                      SizedBox(width: 25.0 * x),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  _DateFilterWidget(),
                  _PriceRangeFilterWidget(),
                ],
              ),
            ),
            SizedBox(
              height: 15.0 * y,
            ),
            ScopedModelDescendant<FilterState>(
              builder: (_, __, filterState) => Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: filterState.isOff() ? ColorPallet.midGray : ColorPallet.primaryColor,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    ),
                    onPressed: () {
                      ParaDataScopedModel.of(context).onTap('InkWell', 'removeFilters');
                      FilterState.of(context).reset();
                    },
                    child: Text(
                      translations.text('removeFilters'),
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16 * f),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 25.0 * y,
            ),
          ],
        ),
      ),
    );
  }
}

class _DateFilterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<FilterState>(builder: (context, child, filterState) {
      return FutureBuilder<DateRange>(
          future: FilterController().getDateRange(),
          builder: (context, snapshot) {
            if (snapshot?.data == null) {
              return Container();
            }
            return InkWell(
              onTap: () async {
                final dateTimeRange = await showDateRangePicker(
                  context: context,
                  firstDate: DateTime.now().subtract(const Duration(days: 30)),
                  lastDate: DateTime.now().add(const Duration(days: 30)),
                );

                FilterState.of(context).startDate = dateTimeRange.start;
                FilterState.of(context).endDate = dateTimeRange.end;
                FilterState.of(context).notify();
                ReceiptListState.of(context).notify();
              },
              child: ExpansionTile(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(translations.text('date'), style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 18.0 * f, fontWeight: FontWeight.w600)),
                  ],
                ),
                leading: Icon(Icons.calendar_today, color: ColorPallet.darkTextColor, size: 24.0 * x),
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0 * x, vertical: 16.0 * y),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(translations.text('beginDate'), style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 16 * f)),
                        Text(
                          DateFormat('E d MMM', International.languageFromId(LanguageSetting.key).localeKey)
                              .format(filterState.startDate ?? snapshot.data.start),
                          style: TextStyle(
                              color: filterState.startDate != snapshot.data.start && filterState.startDate != null
                                  ? ColorPallet.primaryColor
                                  : ColorPallet.midGray,
                              fontWeight: FontWeight.w600,
                              fontSize: 16 * f),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0 * x, vertical: 16.0 * y),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(translations.text('endDate'), style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 16 * f)),
                        Text(
                          DateFormat('E d MMM', International.languageFromId(LanguageSetting.key).localeKey).format(filterState.endDate ?? snapshot.data.end),
                          style: TextStyle(
                              color: filterState.endDate != snapshot.data.end && filterState.endDate != null ? ColorPallet.primaryColor : ColorPallet.midGray,
                              fontWeight: FontWeight.w600,
                              fontSize: 16 * f),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          });
    });
  }
}

class TextEditingControllerWorkaroud extends TextEditingController {
  TextEditingControllerWorkaroud({String text}) : super(text: text);

  void setTextAndPosition(String newText, {int caretPosition}) {
    final offset = caretPosition ?? newText.length;
    value = value.copyWith(text: newText, selection: TextSelection.collapsed(offset: offset), composing: TextRange.empty);
  }
}

class _PriceRangeFilterWidget extends StatefulWidget {
  @override
  _PriceRangeFilterWidgetState createState() => _PriceRangeFilterWidgetState();
}

class _PriceRangeFilterWidgetState extends State<_PriceRangeFilterWidget> {
  String maxValue = '';
  String minValue = '';
  TextEditingControllerWorkaroud maxValueController = TextEditingControllerWorkaroud();
  TextEditingControllerWorkaroud minValueController = TextEditingControllerWorkaroud();

  void checkUserInput(String newPrice, BuildContext context, bool isMinPrice) {
    if (!InputUtil.intValidator(newPrice)) {
      Toast.show(translations.text('invalidAmount'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

      if (isMinPrice) {
        minValueController.setTextAndPosition(minValue);
      } else {
        maxValueController.setTextAndPosition(maxValue);
      }
    } else {
      if (isMinPrice) {
        minValue = newPrice;
      } else {
        maxValue = newPrice;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        minValueController.text = '';
        maxValueController.text = '';

        final valueRange = await FilterController().getValueRange();

        if (FilterState.of(context).minValue != null) {
          valueRange.min = FilterState.of(context).minValue;
        }

        if (FilterState.of(context).maxValue != null) {
          valueRange.max = FilterState.of(context).maxValue;
        }

        minValue = valueRange.min.toStringAsFixed(2);
        maxValue = valueRange.max.toStringAsFixed(2);

        await showDialog(
            context: context,
            routeSettings: const RouteSettings(name: 'FilterAmountDialog'),
            builder: (BuildContext context) {
              return SimpleDialog(
                contentPadding: const EdgeInsets.all(0),
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        color: ColorPallet.primaryColor,
                        height: 68,
                        width: 400,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 20 * y),
                            Padding(
                              padding: EdgeInsets.only(left: 35.0 * x),
                              child: Text(translations.text('filterAmount'),
                                  style: TextStyle(
                                    fontSize: 25 * f,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white,
                                  )),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 20 * y),
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 60 * y,
                              width: 400 * x,
                            ),
                            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                              Column(
                                children: <Widget>[
                                  const Text(
                                    'Min',
                                    style: TextStyle(color: ColorPallet.primaryColor, fontWeight: FontWeight.w700, fontSize: 21),
                                  ),
                                  SizedBox(
                                    width: 80 * x,
                                    height: 40 * y,
                                    child: TextField(
                                      textAlign: TextAlign.center,
                                      controller: minValueController,
                                      style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500, fontSize: 18 * f),
                                      autocorrect: false,
                                      keyboardType: const TextInputType.numberWithOptions(decimal: true),
                                      onChanged: (newPrice) {
                                        setState(() {
                                          checkUserInput(newPrice, context, true);
                                        });
                                      },
                                      onSubmitted: (value) {},
                                      decoration: InputDecoration(
                                        hintText: valueRange.min.toStringAsFixed(2),
                                        hintStyle: TextStyle(color: ColorPallet.midGray, fontWeight: FontWeight.w500, fontSize: 18 * f),
                                        filled: true,
                                        fillColor: Colors.transparent,
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 80 * x),
                              Column(
                                children: <Widget>[
                                  Text(
                                    'Max',
                                    style: TextStyle(color: ColorPallet.primaryColor, fontWeight: FontWeight.w700, fontSize: 21 * f),
                                  ),
                                  SizedBox(
                                    width: 80 * x,
                                    height: 40 * y,
                                    child: TextField(
                                      textAlign: TextAlign.center,
                                      controller: maxValueController,
                                      style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w500, fontSize: 18 * f),
                                      autocorrect: false,
                                      keyboardType: const TextInputType.numberWithOptions(decimal: true),
                                      onChanged: (newPrice) {
                                        setState(() {
                                          checkUserInput(newPrice, context, false);
                                        });
                                      },
                                      onSubmitted: (value) {},
                                      decoration: InputDecoration(
                                        hintText: valueRange.max.toStringAsFixed(2),
                                        hintStyle: TextStyle(color: ColorPallet.midGray, fontWeight: FontWeight.w500, fontSize: 18 * f),
                                        filled: true,
                                        fillColor: Colors.transparent,
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ]),
                            SizedBox(height: 60 * y),
                            Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                              TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text(
                                  translations.text('cancel').toUpperCase(),
                                  style: const TextStyle(color: ColorPallet.primaryColor),
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  FilterState.of(context).minValue = double.parse(minValue);
                                  FilterState.of(context).maxValue = double.parse(maxValue);
                                  FilterState.of(context).notify();
                                },
                                child: Text(
                                  translations.text('ok').toUpperCase(),
                                  style: const TextStyle(color: ColorPallet.primaryColor),
                                ),
                              )
                            ]),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              );
            });
      },
      child: ScopedModelDescendant<FilterState>(
        builder: (context, child, filterState) => FutureBuilder<ValueRange>(
            future: FilterController().getValueRange(),
            builder: (context, valueRange) {
              if (valueRange?.data == null) {
                return Container();
              }
              return ExpansionTile(
                title:
                    Text(translations.text('priceRange'), style: TextStyle(color: ColorPallet.darkTextColor, fontSize: 18.0 * f, fontWeight: FontWeight.w600)),
                leading: Text(Translations.textStatic('currencySymbol', 'CurrencySetting'),
                    style: TextStyle(
                        color: ColorPallet.darkTextColor,
                        fontSize: 20.0 * f,
                        fontWeight: FontWeight.w600)), //Icon(Icons.euro_symbol, color: ColorPallet.darkTextColor, size: 24.0 * x),
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0 * x, vertical: 16.0 * y),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          translations.text('minimum'),
                          style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 16 * f),
                        ),
                        Text(
                          filterState.minValue?.toStringAsFixed(2) ?? valueRange.data.min.toStringAsFixed(2),
                          style: TextStyle(
                              color:
                                  filterState.minValue != valueRange.data.min && filterState.minValue != null ? ColorPallet.primaryColor : ColorPallet.midGray,
                              fontWeight: FontWeight.w600,
                              fontSize: 16 * f),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0 * x, vertical: 16.0 * y),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          translations.text('maximum'),
                          style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 16 * f),
                        ),
                        Text(
                          filterState.maxValue?.toStringAsFixed(2) ?? valueRange.data.max.toStringAsFixed(2),
                          style: TextStyle(
                              color:
                                  filterState.maxValue != valueRange.data.max && filterState.maxValue != null ? ColorPallet.primaryColor : ColorPallet.midGray,
                              fontWeight: FontWeight.w600,
                              fontSize: 16 * f),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }
}
