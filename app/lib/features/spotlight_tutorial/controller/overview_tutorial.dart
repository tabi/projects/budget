import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../widget/animated_focus_light.dart';
import '../widget/content_target.dart';
import '../widget/target_focus.dart';
import '../widget/tutorial_coach_mark.dart';

List<TargetFocus> targets = [];

TextStyle titleStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.white,
  fontSize: 22.0 * f,
  height: 1.4,
);
TextStyle bodyStyle = TextStyle(
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 15.0 * f,
  height: 1.4,
);

void initTargets(GlobalKey keyButton1, GlobalKey keyButton2, GlobalKey keyButton3, GlobalKey keyButton4, BuildContext context) {
  if (targets.isEmpty) {
    targets.add(
      TargetFocus(
        identify: 'blue',
        keyTarget: keyButton1,
        contents: [
          ContentTarget(
              child: SizedBox(
            height: MediaQuery.of(context).size.height - 150 * y,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(children: <Widget>[
                  Text(
                    Translations.textStatic('mainOverviewTutorial1', 'Calendar_T'),
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 7 * y),
                  Text(
                    Translations.textStatic('mainOverviewTutorial2', 'Calendar_T'),
                    style: bodyStyle,
                    textAlign: TextAlign.center,
                  ),
                ]),
                Column(
                  children: <Widget>[
                    Text(
                      Translations.textStatic('mainOverviewTutorial3', 'Calendar_T'),
                      style:
                          TextStyle(fontWeight: FontWeight.w700, color: Colors.white, fontSize: 22.0 * f, height: 1.4, shadows: const [Shadow(blurRadius: 1)]),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 7 * y),
                    Text(
                      Translations.textStatic('mainOverviewTutorial4', 'Calendar_T'),
                      style:
                          TextStyle(fontWeight: FontWeight.w400, color: Colors.white, fontSize: 15.0 * f, height: 1.4, shadows: const [Shadow(blurRadius: 1)]),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    SizedBox(height: 30 * y),
                    Image.asset(
                      'assets/images/tab_symbol.png',
                      height: 100 * y,
                    ),
                  ],
                ),
              ],
            ),
          ))
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton2,
        contents: [
          ContentTarget(
            child: Column(
              children: <Widget>[
                Text(
                  Translations.textStatic('mainOverviewTutorial5', 'Calendar_T'),
                  style: titleStyle,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 7 * y),
                Text(
                  Translations.textStatic('mainOverviewTutorial6', 'Calendar_T'),
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          )
        ],
        shape: ShapeLightFocus.RRect,
      ),
    );

    if (LanguageSetting.key == 'nl' || LanguageSetting.key == 'en') {
      targets.add(
        TargetFocus(
          identify: 'normal',
          keyTarget: keyButton3,
          contents: [
            ContentTarget(
                align: AlignContent.top,
                child: Column(
                  children: <Widget>[
                    Text(
                      Translations.textStatic('mainOverviewTutorial7', 'Calendar_T'),
                      style: titleStyle,
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0 * y),
                      child: Text(
                        Translations.textStatic('mainOverviewTutorial8', 'Calendar_T'),
                        style: bodyStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 15 * y),
                  ],
                ))
          ],
          shape: ShapeLightFocus.RRect,
        ),
      );
    }

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton4,
        contents: [
          ContentTarget(
              align: AlignContent.top,
              child: Column(
                children: <Widget>[
                  Text(
                    Translations.textStatic('mainOverviewTutorial9', 'Calendar_T'),
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0 * y),
                    child: Text(
                      Translations.textStatic('mainOverviewTutorial10', 'Calendar_T'),
                      style: bodyStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 30 * y),
                ],
              ))
        ],
        shape: ShapeLightFocus.Circle,
      ),
    );
  }
}

void showTutorial(BuildContext context) {
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(statusBarColor: ColorPallet.darkTextColor),
  );
  showGestureLogo = true;
  TutorialCoachMark(
    context,
    targets: targets,
    colorShadow: ColorPallet.darkTextColor,
    textSkip: Translations.textStatic('skip', '?'),
    textStyleSkip: TextStyle(color: Colors.white, fontSize: 16 * f),
    alignSkip: Alignment.topLeft,
    opacityShadow: .98,
    clickSkip: () {
      showGestureLogo = false;
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    finish: () {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
      );
    },
    clickTarget: (TargetFocus target) {
      showGestureLogo = false;
    },
  ).show();
}
