import 'dart:ui';

class TargetPosition {
  TargetPosition(this.size, this.offset);

  final Offset offset;
  final Size size;
}
