import 'dart:async';

import 'package:flutter/material.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../controller/util/util.dart';
import 'light_paint.dart';
import 'light_paint_rect.dart';
import 'target_focus.dart';
import 'target_position.dart';

enum ShapeLightFocus { Circle, RRect }

bool showGestureLogo = false;

class AnimatedFocusLight extends StatefulWidget {
  const AnimatedFocusLight({
    Key key,
    this.targets,
    this.focus,
    this.finish,
    this.removeFocus,
    this.clickTarget,
    this.paddingFocus = 10,
    this.colorShadow = Colors.black,
    this.opacityShadow = 0.8,
    this.streamTap,
  }) : super(key: key);

  final Color colorShadow;
  final double opacityShadow;
  final double paddingFocus;
  final Function removeFocus;
  final Stream<void> streamTap;
  final List<TargetFocus> targets;

  @override
  _AnimatedFocusLightState createState() => _AnimatedFocusLightState();

  final Function(TargetFocus) focus;

  final Function(TargetFocus) clickTarget;

  final Function() finish;
}

class _AnimatedFocusLightState extends State<AnimatedFocusLight> with TickerProviderStateMixin {
  int currentFocus = -1;
  bool finishFocus = false;
  bool goingForward = true;
  bool initReverse = false;
  Offset positioned = const Offset(0, 0);
  double progressAnimated = 0;
  double sizeCircle = 100;
  TargetPosition targetPosition;
  Animation tweenPulse;

  AnimationController _controller;
  AnimationController _controllerPulse;
  CurvedAnimation _curvedAnimation;

  @override
  void dispose() {
    _controllerPulse.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 300));
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          finishFocus = true;
        });
        widget?.focus(widget.targets[currentFocus]);
        _controllerPulse.forward();
      }
      if (status == AnimationStatus.dismissed) {
        setState(() {
          finishFocus = false;
          initReverse = false;
        });
        _nextFocus();
      }

      if (status == AnimationStatus.reverse) {
        widget?.removeFocus();
      }
    });

    _curvedAnimation = CurvedAnimation(parent: _controller, curve: Curves.ease);

    _controllerPulse = AnimationController(vsync: this, duration: const Duration(milliseconds: 500));
    _controllerPulse.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controllerPulse.reverse();
      }

      if (status == AnimationStatus.dismissed) {
        if (initReverse) {
          setState(() {
            finishFocus = false;
          });
          _controller.reverse();
        } else if (finishFocus) {
          _controllerPulse.forward();
        }
      }
    });

    tweenPulse = Tween(begin: 1, end: 0.99).animate(CurvedAnimation(parent: _controllerPulse, curve: Curves.ease));

    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);

    widget.streamTap.listen((_) {
      _tapHandler();
    });

    super.initState();
  }

  void _tapHandler() {
    setState(() {
      initReverse = true;
      _controllerPulse.reverse(from: _controllerPulse.value);
    });
    if (currentFocus > -1) {
      widget?.clickTarget(widget.targets[currentFocus]);
    }
  }

  void _nextFocus() {
    if (currentFocus >= widget.targets.length - 1) {
      this._finish();
      return;
    }

    currentFocus++;

    final targetPosition = getTargetCurrent(widget.targets[currentFocus]);
    if (targetPosition == null) {
      this._finish();
      return;
    }

    setState(() {
      finishFocus = false;
      this.targetPosition = targetPosition;

      positioned = Offset(
        targetPosition.offset.dx + (targetPosition.size.width / 2),
        targetPosition.offset.dy + (targetPosition.size.height / 2),
      );

      if (targetPosition.size.height > targetPosition.size.width) {
        sizeCircle = targetPosition.size.height * 0.6 + widget.paddingFocus;
      } else {
        sizeCircle = targetPosition.size.width * 0.6 + widget.paddingFocus;
      }
    });

    _controller.forward();
  }

  void _finish() {
    setState(() {
      currentFocus = -1;
    });

    widget.finish();
  }

  void _afterLayout(Duration timeStamp) {
    _nextFocus();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          _tapHandler();
        },
        child: AnimatedBuilder(
            animation: _controller,
            builder: (_, chuild) {
              progressAnimated = _curvedAnimation.value;
              return AnimatedBuilder(
                animation: _controllerPulse,
                builder: (_, child) {
                  if (finishFocus) {
                    progressAnimated = tweenPulse.value;
                  }
                  return Stack(
                    children: <Widget>[
                      SizedBox(
                        width: double.maxFinite,
                        height: double.maxFinite,
                        child: currentFocus != -1
                            ? CustomPaint(
                                painter: widget?.targets[currentFocus]?.shape == ShapeLightFocus.RRect
                                    ? LightPaintRect(
                                        colorShadow: widget.colorShadow,
                                        positioned: positioned,
                                        progress: progressAnimated,
                                        offset: widget.paddingFocus,
                                        target: targetPosition,
                                        radius: 15,
                                        opacityShadow: widget.opacityShadow,
                                      )
                                    : LightPaint(
                                        progressAnimated,
                                        positioned,
                                        sizeCircle,
                                        colorShadow: widget.colorShadow,
                                        opacityShadow: widget.opacityShadow,
                                      ),
                              )
                            : Container(),
                      ),
                      Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                              padding: const EdgeInsets.all(41),
                              child: Text('${currentFocus + 1}/${widget.targets.length}', style: TextStyle(color: Colors.white, fontSize: 22 * f)))),
                    ],
                  );
                },
              );
            }),
      ),
    );
  }
}
