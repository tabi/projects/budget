library tutorial_coach_mark;

import 'package:flutter/material.dart';

import 'target_focus.dart';
import 'tutorial_coach_mark_widget.dart';

class TutorialCoachMark {
  TutorialCoachMark(
    this._context, {
    @required this.targets,
    this.colorShadow = Colors.black,
    this.clickTarget,
    this.finish,
    this.paddingFocus = 10,
    this.clickSkip,
    this.alignSkip = Alignment.bottomRight,
    this.textSkip = 'SKIP',
    this.textStyleSkip = const TextStyle(color: Colors.white, fontSize: 16),
    this.opacityShadow = 0.8,
  }) : assert(targets != null, opacityShadow >= 0 && opacityShadow <= 1);

  final AlignmentGeometry alignSkip;
  final Color colorShadow;
  final double opacityShadow;
  final double paddingFocus;
  final List<TargetFocus> targets;
  final String textSkip;
  final TextStyle textStyleSkip;

  final BuildContext _context;
  OverlayEntry _overlayEntry;

  final Function(TargetFocus) clickTarget;

  final Function() finish;

  final Function() clickSkip;

  OverlayEntry _buildOverlay() {
    return OverlayEntry(builder: (context) {
      return TutorialCoachMarkWidget(
        targets: targets,
        clickTarget: clickTarget,
        paddingFocus: paddingFocus,
        clickSkip: clickSkip,
        alignSkip: alignSkip,
        textSkip: textSkip,
        textStyleSkip: textStyleSkip,
        colorShadow: colorShadow,
        opacityShadow: opacityShadow,
        finish: () {
          hide();
        },
      );
    });
  }

  void show() {
    if (_overlayEntry == null) {
      _overlayEntry = _buildOverlay();
      Overlay.of(_context).insert(_overlayEntry);
    }
  }

  void hide() {
    if (finish != null) finish();
    _overlayEntry?.remove();
    _overlayEntry = null;
  }
}
