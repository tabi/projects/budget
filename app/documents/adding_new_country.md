To add a new country to the HBS app project, follow these steps:

- Deploy the backend. Instructions on how to accomplish this can be found [in the backend README.md](https://gitlab.com/tabi/projects/budget/-/blob/master/backend/readme.md).

- You will need to add a set of assets to the frontend app, these are:
    - Several tables in [the local SQLite database](https://gitlab.com/tabi/projects/budget/-/blob/master/app/assets/database.db) need to be added:
        - `tblTranslate`: translations for the UI elements in the app
        - `tblProduct`: add a list of products and coicop categorizations (only the deepest level)
        - `tblCoicop`: add the full coicop structure (all levels)
        - `tblShop`: a list of shop names and shop categories
    - Some dart code needs to be be updated: 
        - Add the endpoint of your backend [here](https://gitlab.com/tabi/projects/budget/-/blob/master/app/lib/core/data/server/sync.dart)
        - Add some localization information [here](https://gitlab.com/tabi/projects/budget/-/blob/master/app/lib/core/model/international.dart).
    - You'll need to add some image and video assets:
        - Logo's and other images [here](https://gitlab.com/tabi/projects/budget/-/tree/master/app/assets/images)
        - Optional: If decide to use in-app OCR, you can add an instructional video [here](https://gitlab.com/tabi/projects/budget/-/tree/master/app/assets/videos)





