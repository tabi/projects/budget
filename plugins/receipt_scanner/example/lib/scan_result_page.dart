// import 'package:receipt_scanner/resources/color_pallet.dart';
import 'package:flutter/material.dart';
import 'package:receipt_scanner/models/Quality.dart';

import 'dart:typed_data';
import 'package:receipt_scanner/models/receipt_data.dart';
import 'package:receipt_scanner/models/product_group.dart';


class ScanResultPage extends StatefulWidget {
  final ReceiptData receiptData;

  ScanResultPage({Key key, @required this.receiptData}) : super(key: key);
  _ScanResultPageState createState() => _ScanResultPageState();
}

class _ScanResultPageState extends State<ScanResultPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Results'),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 300.0,
                child: _buildProductSummary(),
              ),
              Row(
                children: <Widget>[
                  if (this.widget.receiptData.totalValueContentLine != null)
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Total: ${this.widget.receiptData.totalValueContentLine.mainPrice != null ? this.widget.receiptData.totalValueContentLine.mainPrice.item2 : ""}",
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    )
                ],
              ),
              if (this.widget.receiptData.croppedImageData != null) this._buildImage(this.widget.receiptData.croppedImageData),
              if (this.widget.receiptData.originalImageData != null) this._buildImage(this.widget.receiptData.originalImageData),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.clear, color: Colors.white, size: 35.0,),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  Widget _buildProductSummary() {
    if (this.widget.receiptData.products == null) {
      return SizedBox.shrink();
    }

    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        if (i.isOdd) return Divider();
        final index = i ~/ 2;
        if (index < this.widget.receiptData.products.length) return _buildRow(this.widget.receiptData.products[index]);
        return null;
      },
    );
  }

  Widget _buildRow(ProductGroup product) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List.generate(product.products.length, (index) {
            return Text(product.products[index].text);
          })
        ),
        if (product.mainPrice != null) Text(product.mainPrice.item1.text),
        if (product.mainPrice != null) Text(product.mainPrice.item2.toString(), style: TextStyle(fontWeight: FontWeight.bold),),

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Errors on product
          if (product.qualityAssessments.contains(Quality.missingPrice)) Text("Missing Price", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
          if (product.qualityAssessments.contains(Quality.missingTitle)) Text("Missing Title", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
          if (product.qualityAssessments.contains(Quality.oneSided)) Text("One Sided", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
          if (product.qualityAssessments.contains(Quality.poorAnalysis)) Text("Poor analysis", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
          if (product.qualityAssessments.contains(Quality.good)) Text("Good", style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold)),
          ],
        ),
      ],
    );
  }

  Widget _buildImage(Uint8List data) {
    // Image newImage = Image.memory(data);

    return Stack(
      children: <Widget>[
        Image.memory(data),
      ],
    );
  }
}
