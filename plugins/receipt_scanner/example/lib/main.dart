import 'package:flutter/material.dart';
import 'dart:async';

import 'package:receipt_scanner/receipt_scanner.dart';
import 'package:receipt_scanner_example/scan_result_page.dart';
import 'package:receipt_scanner/util/language_codes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHome()
    );
  }
}

class MyHome extends StatelessWidget {

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> openDocumentScanner(BuildContext context) async {
    await ReceiptScanner.scanReceipt(
      context,
      ImplementedLanguageCode.FI_fi,
      Color(0xFFAF0E80),
      '["TestTip", "Vermijd een witte achtergrond, ook als dit maar een klein stukje is.", "Als een bon kreukels of vouwen heeft probeer hem dan zo plat mogelijk te maken.", "Vermijd schaduwwerking in de scan.", "Scan bij voorkeur bij daglicht.", "Zorg dat de hoeken van de bon recht en vrij zijn.", "Probeer de bon zo plat mogelijk te houden."]'
    ).then((data) {
      if (data == null) {
        return;
      }

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) =>
              new ScanResultPage(receiptData: data),
        ),
      );
    }).catchError((e) {
      print("FAILED");
      // throw new ReceiptScanFailed();
    }, test: (e) => e is ReceiptScanFailed)
    .catchError((e) {
      print("CANCELLED");
      // throw new ReceiptScanCancelled();
    }, test: (e) => e is ReceiptScanCancelled)
    .catchError((e) {
      print("NO CAMERA ACCESS");
      // throw new NoCameraAccess();
    }, test: (e) => e is NoCameraAccess);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: FlatButton(
            onPressed: () {
              this.openDocumentScanner(context);
            },
            child: Text(
              "Open Scanner",
            ),
          )
        ),
    );
  }
}