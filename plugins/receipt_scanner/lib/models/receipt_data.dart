import 'package:receipt_scanner/models/product_group.dart';
import 'dart:typed_data';

class ReceiptData {
  Uint8List originalImageData;
  Uint8List croppedImageData;

  ProductGroup totalValueContentLine;
  List<ProductGroup> products;

  ReceiptData(
    this.originalImageData,
    this.croppedImageData,
    this.totalValueContentLine,
    this.products,
  );
}