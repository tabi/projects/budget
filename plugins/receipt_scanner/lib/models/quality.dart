enum Quality { 
   good,
   missingPrice,
   missingTitle,
   poorAnalysis,
   oneSided
}

class QualityConstants {
  static int minimumTitleLenght = 5;
  static double maxVerticalDistanceBetweenTitleAndPrice = 0.2;
}