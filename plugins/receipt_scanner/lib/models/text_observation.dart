import 'dart:convert';


// The grouped vision result
class VisionResult {
  final List<TextObservation> textObservations;

  VisionResult(this.textObservations);

  factory VisionResult.fromJson(Map<String, dynamic> json){
    var list = json['textObservations'] as List;
    List<TextObservation> observationList = list.map((i) =>
      TextObservation.fromJson(i)
    ).toList();

    return VisionResult(
      observationList
    );
  }

  Map<String, dynamic> toJson() => {
    'textObservations': jsonEncode(textObservations),
  };
}


// A single text observation
class TextObservation {
  final String text;
  final double confidence;
  final VisionRect normalizedRect;

  TextObservation(this.text, this.confidence, this.normalizedRect);

  factory TextObservation.fromJson(Map<String, dynamic> json){
    return TextObservation(
      json['text'],
      json['confidence'].toDouble(),
      VisionRect.fromJson(json['normalizedRect'])
    );
  }

  Map<String, dynamic> toJson() => {
    'text': text,
    'confidence': confidence,
    'normalizedRect': normalizedRect.toJson(),
  };
}


// A cusom Rect object
class VisionRect {
  double xPos;
  double yPos;
  VisionSize size;

  VisionRect(this.xPos, this.yPos, this.size);

  double get xMin => xPos;
  double get yMin => yPos;
  double get xMax => xPos + size.width;
  double get yMax => yPos + size.height;

  factory VisionRect.fromJson(Map<String, dynamic> json){
    return VisionRect(
      json['xPos'].toDouble(),
      json['yPos'].toDouble(),
      VisionSize.fromJson(json['size'])
    );
  }

  Map<String, dynamic> toJson() => {
    'xPos': xPos,
    'yPos': yPos,
    'size': size.toJson(),
  };

  // Math Functions
  static VisionRect union(List<VisionRect> rects) {
    if (rects.length <= 0) {
      return new VisionRect(0, 0, new VisionSize(0, 0));
    }
    // assert(rects.length > 0, "Unable to union 0 rects");
    // Rect baseRect = rects.first;

    // rects.first.xPos
    VisionRect firstRect = rects.first;
    double xMin = firstRect.xMin;
    double yMin = firstRect.yMin;
    double xMax = firstRect.xMax;
    double yMax = firstRect.yMax;


    for (VisionRect rect in rects.sublist(1)) {
      if (rect.xMin < xMin) { xMin = rect.xMin; }
      if (rect.yMin < yMin) { yMin = rect.yMin; }
      
      if (rect.xMax > xMax) { xMax = rect.xMax; }
      if (rect.yMax > yMax) { yMax = rect.yMax; }
    }

    return new VisionRect(xMin, yMin, new VisionSize(xMax - xMin, yMax - yMin));
  }
}


// A custom Size object
class VisionSize {
  double width;
  double height;

  VisionSize(this.width, this.height);

  factory VisionSize.fromJson(Map<String, dynamic> json){
    return VisionSize(
      json['width'].toDouble(), 
      json['height'].toDouble()
    );
  }

  Map<String, dynamic> toJson() => {
    'width': width,
    'heigth': height,
  };
}