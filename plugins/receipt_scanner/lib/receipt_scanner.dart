import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:receipt_scanner/pages/receipt_processing_page.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:receipt_scanner/models/receipt_data.dart';
import 'package:receipt_scanner/util/language_codes.dart';

import 'util/algorithms/receipt_data_parser.dart';

class ReceiptScanFailed implements Exception {
  ReceiptScanFailed();
}

class ReceiptScanCancelled implements Exception {
  ReceiptScanCancelled();
}

class NoCameraAccess implements Exception {
  NoCameraAccess();
}

class ReceiptScanner {
  static const MethodChannel _channel = const MethodChannel('receipt_scanner');

  static Future<ReceiptData> scanReceipt(BuildContext context, ImplementedLanguageCode language, Color mainColor, String tips) async {
    ReceiptDataParser.setCountryCode(language);

    return _hasCameraPermission().then((hasPermission) {
      if (hasPermission) {
        return _openNativeReceiptScanner(tips).then((jsonDataString) {
          var processingPage = ReceiptProcessingPage(
            jsonDataString,
            mainColor,
          );
          Future<ReceiptData> data = Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => processingPage,
            ),
          );
          return data;
        }).catchError((e) {
          throw new ReceiptScanFailed();
        }, test: (e) => e is ReceiptScanFailed).catchError((e) {
          throw new ReceiptScanCancelled();
        }, test: (e) => e is ReceiptScanCancelled);
      } else {
        throw new NoCameraAccess();
      }
    });
  }

  static Future<bool> _hasCameraPermission() async {
    if (await Permission.camera.request().isGranted) {
      return true;
    } else {
      return false;
    }
  }

  static Future<String> _openNativeReceiptScanner(String tips) async {
    String jsonDataString;
    try {
      // jsonDataString = await _channel.invokeMethod('openReceiptScanner', { 'tips': tips });
      jsonDataString = await _channel.invokeMethod('openReceiptScanner', {'tips': '[]'});
      print(tips);
    } on PlatformException {
      throw new ReceiptScanCancelled();
    }

    if (jsonDataString == "scanFailed") {
      throw new ReceiptScanFailed();
    } else if (jsonDataString == "scanCancelled") {
      throw new ReceiptScanCancelled();
    } else {
      return jsonDataString;
    }
  }
}
