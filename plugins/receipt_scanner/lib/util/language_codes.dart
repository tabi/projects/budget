enum ImplementedLanguageCode {
  NL_nl,
  UK_en,
  HU_hu,
  SK_sk,
  FI_fi,
  ES_es,
}

class LanguageSupport {
  static String languageCodeToString(ImplementedLanguageCode code) {
    if (code == ImplementedLanguageCode.NL_nl) {
      return "NL-nl";
    } else if (code == ImplementedLanguageCode.UK_en) {
      return "UK-en";
    } else if (code == ImplementedLanguageCode.HU_hu) {
      return "HU-hu";
    } else if (code == ImplementedLanguageCode.SK_sk) {
      return "SK-sk";
    } else if (code == ImplementedLanguageCode.FI_fi) {
      return "FI-fi";
    } else if (code == ImplementedLanguageCode.ES_es) {
      return "ES-es";
    } else {
      return "unk";
    }
  }

  static final Map receiptKeywordData = {
    "keywords_per_country": {
      "NL-nl" : {
        "total_pattern": r"^(t(o|0)taal|t(o|0)tal|t(o|0)t|t(o|0)t\.|te (8|b)etalen)$",
        "date_pattern": r"([0-2][0-9]|(3)[0-1]|[0-9])(-)(((0)[0-9])|((1)[0-2])|([1-9]))(-)\d{4}",
        "price_pattern": r"-?\d+(\. {0,5})\d{1,2}",
        "text-direction": "lr",
      },
      "UK-en" : {
        "total_pattern": r"^(t(o|0)tal|am(o|0)unt due)$",
        "date_pattern": r"([0-2][0-9]|(3)[0-1]|[0-9])(-)(((0)[0-9])|((1)[0-2])|([1-9]))(-)\d{4}",
        "price_pattern": r"-?\d+(\. {0,5})\d{1,2}",
        "text-direction": "lr",
      },
      "HU-hu" : {
        "total_pattern": r"^((ö|o|0)sszesen|(ö|o|0)sszeg|fizetend(ő|o|0))$",
        "date_pattern": r"((\d{4}|\d{2})(-|\/|\.)(((0)[0-9])|((1)[0-2])|([1-9]))(-|\/|\.)([0-2][0-9]|(3)[0-1]|[0-9])|(\d{4}|\d{2})(-|\/|\.)(([A-Z]{0,10}))(-|\/|\.)([0-2][0-9]|(3)[0-1]|[0-9]))",
        "price_pattern": r"(-?\d? ?\d+)",
        "text-direction": "lr",
      },
      "SK-sk" : {
        "total_pattern": r"^(skupaj|za pa(č|c)il(o|0)|znesek)$",
        "date_pattern": r"((((0)[0-9])|((1)[0-2])|([1-9]))(-|\.|\/)[0-2][0-9]|(3)[0-1]|[0-9])(-|\.|\/)\d{4}",
        "price_pattern": r"-?\d+(\. {0,5})\d{1,2}",
        "text-direction": "lr",
      },
      "FI-fi" : {
        "total_pattern": r"^(yhteens(ä|a)|yht|summa)$",
        "date_pattern": r"([0-2]?[0-9]|(3)[0-1]|[0-9])(-|\.|\/)(((0)?[0-9])|((1)[0-2])|([1-9]))(-|\.|\/)\d{4}",
        "price_pattern": r"-?\d+(\. {0,5})\d{1,2}",
        "text-direction": "lr",
      },
      "ES-es" : {
        "total_pattern": r"^(t(o|0)tal|t(o|0)tal c(o|0)mpra|t(o|0)t|t(0|o)tal a pagar|im(0|o)rte|tra\.)$",
        "date_pattern": r"(([0-2][0-9]|(3)[0-1]|[0-9])(-|\/|\.)(((0)[0-9])|((1)[0-2])|([1-9]))(-|\/|\.)(\d{4}|\d{2})|([0-2][0-9]|(3)[0-1]|[0-9])(-|\/|\.|)([A-Z]{3})(-|\/|\.|)(\d{4}|\d{2}))",
        "price_pattern": r"-?\d+(\. {0,5})\d{1,2}",
        "text-direction": "lr",
      },
      "LU-de" : {
        "total_pattern": r"^(summe|summe in eur|summe eur|gesamtbetrag|kaufsumme|zu hahlen)$",
        "date_pattern": r"(([0-2][0-9]|(3)[0-1]|[0-9])(-|\/|\.)(((0)[0-9])|((1)[0-2])|([1-9]))(-|\/|\.)(\d{4}|\d{2})|([0-2][0-9]|(3)[0-1]|[0-9])(-|\/|\.|)([A-Z]{3})(-|\/|\.|)(\d{4}|\d{2}))",
        "price_pattern": r"-?\d+(\. {0,5})\d{1,2}",
        "text-direction": "lr",
      },
    },
    "currencies": [
      { "eur": "euro" },
      { "€": "euro" },
      { "euro": "euro" },
      { "pound": "pound" },
      { "£": "pound" },
      { "g(8|b)p": "pound" },
      { "Ft" : "forint" },
      { "forint" : "forint" },
    ]
  };
}