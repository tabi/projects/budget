import 'dart:math';
import 'package:receipt_scanner/models/product_group.dart';
import 'package:receipt_scanner/util/language_codes.dart';
import 'package:receipt_scanner/models/text_observation.dart';
import 'package:string_validator/string_validator.dart';
import 'package:tuple/tuple.dart';

class ReceiptDataParser {
  static String _countryCode = "NL-nl";

  static void setCountryCode(ImplementedLanguageCode code) {
    _countryCode = LanguageSupport.languageCodeToString(code);
  }

  static Tuple2<List<ProductGroup>, ProductGroup> getReceiptData(VisionResult visionResults) {
    // Here we assume that the first text we see belongs to a product (i.e. the user cropped down the info)
    ReceiptDataParser rda = new ReceiptDataParser();

    // Get the text direction for the selected language
    String textDirectionForRun = LanguageSupport.receiptKeywordData["keywords_per_country"][ReceiptDataParser._countryCode]["text-direction"];

    // Compute the average text height for the receipt
    double averageTextHeight = _averageTextHeight(visionResults);

    double margin = (-3 * averageTextHeight) + 0.35;
    margin = max(0.01, margin);
    margin = min(0.3, margin);

    // This gives us the lines sorted on both the y and x axes
    List<List<TextObservation>> receiptLines = _groupVisionResultOnRow(visionResults, averageTextHeight, margin, textDirectionForRun);
    List<TextObservation> xySortedTextObservations = receiptLines.expand((i) => i).toList();

    // Get the grouped observations
    List<ProductGroup> groups = rda.createGroups(xySortedTextObservations);

    // Loop over the groups and determine the price for each
    groups.forEach((group) {
      group.determinePrice(textDirectionForRun);
    });

    // Find the total price and product
    Tuple2 totalProductGroupAndIndex = rda._getProductGroupContainingTotalValue(groups);

    // Remove everything after the total product from the list
    int totalIndex = totalProductGroupAndIndex.item2;
    if (totalIndex != -1) {
      groups = groups.sublist(0, totalIndex);
    }

    // For each product, we first determine the total area covered on the receipt
    // Then we determine the quality of each individual product
    groups.forEach((element) {
      element.determineArea();
      element.determineProductGroupQuality();
    });

    return new Tuple2(groups, totalProductGroupAndIndex.item1);
  }

  /// Calculates the average height of all provided text observations
  static double _averageTextHeight(VisionResult visionResults) {
    if (visionResults.textObservations.length == 0) {
      return 0.0;
    }
    double allHeights = visionResults.textObservations.map((e) => e.normalizedRect.size.height).reduce((a, b) => a + b);
    return allHeights / visionResults.textObservations.length;
  }

  /// Creates TextObservation groups based on their position and content
  List<ProductGroup> createGroups(List<TextObservation> textObservations) {
    // Create some place to store the results
    ProductGroup currentGroup = new ProductGroup();
    List<ProductGroup> groups = [];

    // Create the regex used to extract prices
    RegExp priceRegex = new RegExp(
      LanguageSupport.receiptKeywordData["keywords_per_country"][ReceiptDataParser._countryCode]["price_pattern"],
      caseSensitive: false,
      multiLine: false,
    );

    // Loop over all products
    textObservations.forEach((textObervation) {
      String productText = textObervation.text.toLowerCase().replaceAll(',', '.');

      // Check if the string contains a value
      RegExpMatch match = priceRegex.firstMatch(productText);
      double alphaPercentage = this.alphaPercentage(productText);

      if (alphaPercentage > 0.5) {
        // The text contains many non-numerical characters. We thus characterize it as part of a product
        if (currentGroup.empty()) {
          currentGroup.addProduct(textObervation);
        } else {
          String textNoSpaces = productText;
          textNoSpaces.replaceAll(" ", "");

          if (textNoSpaces.length >= 4) {
            groups.add(currentGroup);
            currentGroup = new ProductGroup();
            currentGroup.addProduct(textObervation);
          } else {
            currentGroup.addProduct(textObervation);
          }
        }
      } else if (match != null) {
        // There is a match
        String text = match.group(0);

        try {
          text.replaceAll(match.group(1), '.');
        } on RangeError {}

        try {
          currentGroup.addPrice(textObervation, double.parse(text));
        } on FormatException {}
      } else if (alphaPercentage == 1) {
        // The text is all numbers
        currentGroup.addPrice(textObervation, double.parse(productText));
      } else {
        // There is no match, it is thus part of a product.
        // But is it a new one or the existing one... We will check that now
        if (currentGroup.empty()) {
          currentGroup.addProduct(textObervation);
        } else {
          List<String> splitted = productText.split(' ');
          int maxLen = splitted.map((e) => e.length).reduce(max);

          if ((maxLen > 5 || alphaPercentage > 0.5) && alphaPercentage >= 0.1) {
            groups.add(currentGroup);
            currentGroup = new ProductGroup();
            currentGroup.addProduct(textObervation);
          } else {
            currentGroup.addProduct(textObervation);
          }
        }
      }
    });

    // If there is a final group, add it
    if (!currentGroup.empty()) {
      groups.add(currentGroup);
    }

    return groups;
  }

  /// Returns a tuple containing a ProductGroup and indexing which are found to be the total amount
  /// If there is no match (null, -1) is returned
  Tuple2 _getProductGroupContainingTotalValue(List<ProductGroup> productGroups) {
    String totalRegexPattern = LanguageSupport.receiptKeywordData["keywords_per_country"][ReceiptDataParser._countryCode]["total_pattern"];
    RegExp priceRegex = new RegExp(
      totalRegexPattern,
      caseSensitive: false,
      multiLine: false,
    );
    List<Tuple2> matchingRows = [];

    int index = 0;
    productGroups.forEach((productGroup) {
      // Loop over the products
      productGroup.products.forEach((product) {
        // Check if there is a match anywhere in the string
        // Note: This could be done using best match - currently it uses Regex which is more restrictive
        String productText = product.text.toLowerCase();
        if (priceRegex.hasMatch(productText)) {
          matchingRows.add(new Tuple2(productGroup, index));
        }
      });
      index++;
    });

    if (matchingRows.length == 0) {
      return new Tuple2(null, -1);
    }

    return matchingRows.last;
  }

  /// Returns a number indicating the percentage of alpha characters
  double alphaPercentage(String string) {
    int numberOfChars = string.length;
    List<String> alphaChars = string.split('').where((element) => isAlpha(element)).toList();
    int numberOfAlphaChars = alphaChars.length;

    if (numberOfAlphaChars == 0) {
      return 0.0;
    }

    return numberOfAlphaChars.toDouble() / numberOfChars.toDouble();
  }

  /// Groups the text observations in the vision results in lines based on the average text
  /// line and a small adjustment percentage of the average height
  /// Note: alignmentAdjustmentPercentage: Value between 0 and 1
  static List<List<TextObservation>> _groupVisionResultOnRow(
      VisionResult visitonResults, double averageTextHeight, double alignmentAdjustmentPercentage, String textDirection) {
    assert(alignmentAdjustmentPercentage >= 0.0 && alignmentAdjustmentPercentage <= 1.0, "alignmentAdjustmentPercentage should be between 0 (0%) and 1 (100%)");

    List<List<TextObservation>> groups = [];
    List<TextObservation> groupItems = [];
    TextObservation previous;

    double adjustedVerticalThreshold = (averageTextHeight / 2) + ((averageTextHeight / 2) * alignmentAdjustmentPercentage);

    // sort the array such that the data is sorted on the y axis (small to large)
    List<TextObservation> textObservations = visitonResults.textObservations;
    textObservations.sort((a, b) => a.normalizedRect.yPos.compareTo(b.normalizedRect.yPos));

    // Loop trough the items and group the rows based on adjustedVerticalThreshold
    for (TextObservation item in textObservations) {
      if (groupItems.isEmpty) {
        groupItems.add(item);
        previous = item;
      } else {
        // Check if the last item in the list is on (about) the same height
        // as the new vision observation

        if ((item.normalizedRect.yPos - previous.normalizedRect.yPos).abs() < adjustedVerticalThreshold) {
          groupItems.add(item);
        } else {
          // sort the array such that the data is sorted on the x axis (small to large)
          groupItems.sort((a, b) => a.normalizedRect.xPos.compareTo(b.normalizedRect.xPos));

          if (textDirection == "rl") {
            groupItems = groupItems.reversed;
          }

          groups.add(groupItems);

          groupItems = [item];
        }
        previous = item;
      }
    }

    // Add last items
    groupItems.sort((a, b) => a.normalizedRect.xPos.compareTo(b.normalizedRect.xPos));
    if (textDirection == "rl") {
      groupItems = groupItems.reversed;
    }
    groups.add(groupItems);

    return groups;
  }
}
