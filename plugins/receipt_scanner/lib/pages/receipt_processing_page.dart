import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:tuple/tuple.dart';
import 'package:receipt_scanner/util/algorithms/receipt_data_parser.dart';
import 'package:receipt_scanner/models/scan_result.dart';
import 'package:receipt_scanner/models/text_observation.dart';
import 'package:receipt_scanner/models/receipt_data.dart';

class ReceiptProcessingPage extends StatefulWidget {
  final String jsonReceiptDataString;
  final Color mainColor;

  ReceiptProcessingPage(this.jsonReceiptDataString, this.mainColor);

  _ReceiptProcessingState createState() => _ReceiptProcessingState();
}

class _ReceiptProcessingState extends State<ReceiptProcessingPage> {
  Uint8List _originalImageData;
  Uint8List _croppedImageData;

  VisionResult extractJsonStringData(String jsonData) {
    // Decode the json string
    var collection = json.decode(jsonData);
    ScanResult _results = ScanResult.fromJson(collection);

    // Decode the images (if they are available)
    Uint8List decodedOriginal, decodedCropped;
    if (_results.originalImageBase64 != null) decodedOriginal = base64.decode(_results.originalImageBase64);
    if (_results.croppedImageBase64 != null) decodedCropped = base64.decode(_results.croppedImageBase64);

    setState(() {
      _originalImageData = decodedOriginal;
      _croppedImageData = decodedCropped;
    });

    return _results.visionResult;
  }

  Tuple2 extractProductsFromVisionResults(VisionResult visionResults) {
    return ReceiptDataParser.getReceiptData(visionResults);
  }

  Future<void> extractData() async {
    VisionResult visionResult = extractJsonStringData(widget.jsonReceiptDataString);
    Tuple2 receiptDataResultsTuple = extractProductsFromVisionResults(visionResult);
    if (receiptDataResultsTuple == null) {
      Navigator.pop(context, null);
      return;
    }
    Navigator.pop(
      context,
      ReceiptData(
        _originalImageData,
        _croppedImageData,
        receiptDataResultsTuple.item2,
        receiptDataResultsTuple.item1,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    extractData();
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
          child: Stack(
        children: <Widget>[
          ColorFiltered(
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.darken),
            child: Image.memory(
              _originalImageData,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
              alignment: Alignment.center,
            ),
          ),
          Positioned.fill(
            child: Align(
              alignment: Alignment.center,
              child: LoadingIndicator(),
            ),
          ),
        ],
      )),
    );
  }
}

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150.0,
      width: 150.0,
      color: Colors.transparent,
      child: Container(
          decoration: BoxDecoration(color: Colors.black.withOpacity(0.6), borderRadius: new BorderRadius.all(const Radius.circular(10.0))),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 50,
                  height: 50,
                  child: CircularProgressIndicator(
                    strokeWidth: 5,
                  ),
                ),
                Text(
                  "Looking for products",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 14.0),
                ),
              ],
            ),
          )),
    );
  }
}
