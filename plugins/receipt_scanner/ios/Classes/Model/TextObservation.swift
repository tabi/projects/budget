//
//  TextObservation.swift
//  Runner
//
//  Created by Marc Wiggerman on 21/02/2020.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import Foundation
import Vision


/// A codable container for vision results
struct VisionResult: Codable {
    /// A list containing the text observations resulting from the OCR step
    var textObservations: [TextObservation]

    /// A single text observation
    struct TextObservation: Codable {
        var text: String
        var confidence: Float
        var normalizedRect: VisionRect
    }

    struct VisionRect: Codable {
        var xPos: Float
        var yPos: Float
        var size: VisionSize

        init (rect: CGRect, visionMethod: VisionMethod) {
            // Note that incoming positions go from 1.0 (top) to 0.0 (bottom)
            // This is converted the other way around
            self.xPos = Float(rect.minX)
            self.yPos = Float(rect.minY)
            self.size = VisionSize(size: rect.size)

            if visionMethod == .appleVision {
                // Invert the coordinated verically
                self.yPos = 1.0 - self.yPos

                // Apple qualifies the left-top corner as origin, but we require the left-bottom (the google MLKit implementation)
                self.yPos -= self.size.height
            }
        }
    }

    struct VisionSize: Codable {
        var width: Float
        var height: Float

        init (size: CGSize) {
            self.width = Float(size.width)
            self.height = Float(size.height)
        }
    }
}
