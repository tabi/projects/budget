//
//  ReceiptScannerViewController.swift
//  Runner
//
//  Created by Marc Wiggerman on 20/02/2020.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import UIKit
import AVKit
import WeScanHBS
import MaterialComponents.MaterialActivityIndicator

enum ScannerError: String {
    case noCameraPermission
    case scanCancelled
    case scanFailed
}

/// The main document scanner view controller
/// - Warning: Completion handlers are used to communicate back the scanner results.
class ReceiptScannerViewController: UIViewController {
    // Completion
    fileprivate var completionHandler: ((String) -> Void)?

    @available(iOS 13.0, *)
    fileprivate var visionImageToText: VisionImageToText {
        let vitt = VisionImageToText { [weak self] (resultsString) in
            self?.completionHandler?(resultsString)
        }
        return vitt
    }

    fileprivate var googleMLKitImageToText: GoogleMLKitImageToText {
        let fitt = GoogleMLKitImageToText { [weak self] (resultsString) in
            self?.completionHandler?(resultsString)
        }
        return fitt
    }
    
    fileprivate lazy var screenshotImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    fileprivate lazy var activityIndicator: MDCActivityIndicator = {
        let activityIndicator = MDCActivityIndicator()
        activityIndicator.sizeToFit()
        activityIndicator.cycleColors = [.cbsPurple]
        activityIndicator.radius = 25
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }()
    
    fileprivate lazy var loadingOverlay: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        screenshotImageView.image = nil
        view.backgroundColor = .clear
        setupConstraints()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        resetLoadingView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    // MARK: - Interfacing
    /// Starts the document scanner
    /// - Parameter completion: The completion handler will return a ScannerError if something went wrong. Otherwise
    ///                         it will return the Json String
    public func startDocumentScanner(withTips: [String]? = nil, completion: @escaping (String) -> Void) {
        completionHandler = completion
        AVCaptureDevice.requestAccess(for: .video) { (response) in
            if !response {
                completion(ScannerError.noCameraPermission.rawValue)
            } else {
                DispatchQueue.main.async { [weak self] in
                    let scannerViewController = ImageScannerController(delegate: self, tips: withTips)
                    scannerViewController.modalPresentationStyle = .fullScreen
                    self?.present(scannerViewController, animated: true)
                }
            }
        }
    }

    // MARK: - UI

    /// Adds all views to the view controller
    fileprivate func setupViews() {
        screenshotImageView.isHidden = true
        screenshotImageView.isUserInteractionEnabled = false
        
        loadingOverlay.isHidden = true
        
        view.addSubview(screenshotImageView)
        view.addSubview(loadingOverlay)
        loadingOverlay.addSubview(activityIndicator)
    }

    /// Sets the constraints required for the views
    fileprivate func setupConstraints() {
        let screenshotImageViewConstraints = [
            screenshotImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            screenshotImageView.topAnchor.constraint(equalTo: view.topAnchor),
            screenshotImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            screenshotImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        
        let loadingOverlayConstraints = [
            loadingOverlay.widthAnchor.constraint(equalToConstant: 100),
            loadingOverlay.heightAnchor.constraint(equalToConstant: 100),
            loadingOverlay.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingOverlay.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ]
        
        let activityIndicatorConstraints = [
            activityIndicator.leadingAnchor.constraint(equalTo: loadingOverlay.leadingAnchor, constant: 16),
            activityIndicator.topAnchor.constraint(equalTo: loadingOverlay.topAnchor, constant: 16),
            activityIndicator.trailingAnchor.constraint(equalTo: loadingOverlay.trailingAnchor, constant: -16),
            activityIndicator.bottomAnchor.constraint(equalTo: loadingOverlay.bottomAnchor, constant: -16),
        ]
        
        NSLayoutConstraint.activate(screenshotImageViewConstraints + loadingOverlayConstraints + activityIndicatorConstraints)
    }
    
    fileprivate func enableScreenshotImageView() {
        let screenshot = takeScreenshot()
        screenshotImageView.image = screenshot
        screenshotImageView.isHidden = false
        
        enableLoadingView()
    }

    /// Enable the loading view (image and label)
    /// - Parameter imageScan: the image presented in the loading view
    fileprivate func enableLoadingView() {
        DispatchQueue.main.async { [weak self] in
            self?.loadingOverlay.isHidden = false
            self?.activityIndicator.startAnimating()
        }
    }

    /// Hide the loading view
    fileprivate func resetLoadingView() {
        DispatchQueue.main.async { [weak self] in
            self?.loadingOverlay.isHidden = true
            self?.screenshotImageView.image = nil
            self?.activityIndicator.stopAnimating()
        }
    }
}

extension ReceiptScannerViewController: ImageScannerControllerDelegate {
    func imageScannerController(_ scanner: ImageScannerController, didFinishScanningWithResults results: ImageScannerResults) {
        
        enableScreenshotImageView()

        // Get the original image and the cropped image
        // Note: The cropped image will be enhanced if available
        let originalImage: UIImage = results.originalScan.image
        let croppedImage: UIImage
        if let enhancedImage = results.enhancedScan,
           results.doesUserPreferEnhancedScan {
            croppedImage = enhancedImage.image
        } else {
            croppedImage = results.croppedScan.image
        }

        // Present the loading view
        enableLoadingView()

        // Dismiss the WeScan scanner and process the images with OCR
        scanner.dismiss(animated: false) {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                // Use vision for iOS 13 and up, otherwise use googleMLKit
                if #available(iOS 13.0, *) {
                    self?.visionImageToText.processImage(croppedImage: croppedImage, originalImage: originalImage)
                } else {
                    self?.googleMLKitImageToText.processImage(croppedImage: croppedImage, originalImage: originalImage)
                }
            }
        }
    }

    func imageScannerControllerDidCancel(_ scanner: ImageScannerController) {
        scanner.dismiss(animated: true)
        completionHandler?(ScannerError.scanCancelled.rawValue)
    }

    func imageScannerController(_ scanner: ImageScannerController, didFailWithError error: Error) {
        scanner.dismiss(animated: true)
        completionHandler?(ScannerError.scanFailed.rawValue)
    }
    
    /// Takes the screenshot of the screen and returns the corresponding image
    ///
    /// - Parameter shouldSave: Boolean flag asking if the image needs to be saved to user's photo library. Default set to 'true'
    /// - Returns: (Optional)image captured as a screenshot
    private func takeScreenshot() -> UIImage? {
        var screenshotImage: UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale

        UIGraphicsBeginImageContextWithOptions(layer.frame.size, true, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        return screenshotImage
    }
}
