#import "ReceiptScannerPlugin.h"
#if __has_include(<receipt_scanner/receipt_scanner-Swift.h>)
#import <receipt_scanner/receipt_scanner-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "receipt_scanner-Swift.h"
#endif

@implementation ReceiptScannerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftReceiptScannerPlugin registerWithRegistrar:registrar];
}
@end
