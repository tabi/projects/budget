//
//  ImageToText.swift
//  Runner
//
//  Created by Marc Wiggerman on 21/04/2020.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import Foundation

public enum VisionMethod {
    case appleVision
    case googleMLKitVision
}

protocol ImageToTextProtocol {
    var currentImageBeingAnalysedCropped: UIImage? { get set }
    var currentImageBeingAnalysedOriginal: UIImage? { get set }
    var completionHandler: ((String) -> Void) { get set }

    init(onComplete completionHandler: @escaping ((String) -> Void))
    func processImage(croppedImage: UIImage, originalImage: UIImage)
}
