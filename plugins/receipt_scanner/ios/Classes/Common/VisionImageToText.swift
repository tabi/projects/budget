//
//  VisionImageToText.swift
//
//  Created by Marc Wiggerman on 15/06/2020.
//

import Foundation
import Vision

@available(iOS 13.0, *)
public class VisionImageToText: ImageToTextProtocol {
    internal var currentImageBeingAnalysedCropped: UIImage?
    internal var currentImageBeingAnalysedOriginal: UIImage?
    internal var completionHandler: ((String) -> Void)

    // MARK: - Vision
    fileprivate var textRecognitionRequest: VNRecognizeTextRequest!

    required init(onComplete completionHandler: @escaping ((String) -> Void)) {
        self.completionHandler = completionHandler
        self.configureVision()
    }

    /// Configures the Vision API
    public func configureVision() {
        self.textRecognitionRequest = VNRecognizeTextRequest(completionHandler: { (request, error) in
            if error == nil {
                self.textRecognizedHandler(request: request)
            } else {
                self.completionHandler(ScannerError.scanFailed.rawValue)
                return
            }
        })

        self.textRecognitionRequest.recognitionLevel = .accurate
        self.textRecognitionRequest.usesLanguageCorrection = true
    }

    /// Handles the VNRequest results. The function unpacks the results and converts it to a Json String
    /// - Parameter request: the results of the Vision API
    private func textRecognizedHandler(request: VNRequest) {
        if let results = request.results as? [VNRecognizedTextObservation],
            !results.isEmpty,
            let originalImage = self.currentImageBeingAnalysedOriginal,
            let croppedImage = self.currentImageBeingAnalysedCropped {

            if let resultString = self.getJsonStringResults(results, croppedImage: croppedImage, originalImage: originalImage) {
                self.completionHandler(resultString)
            }
        }
    }

    /// Preprocesses the results by getting the top candidate (with the most confidence)
    /// after which they are turned into TextObservations
    /// - Parameters:
    ///   - results: Vision results
    ///   - sourceImage: The image in which the vision observations are made
    fileprivate func preprocessResults(_ results: [VNRecognizedTextObservation], sourceImage: UIImage) -> VisionResult {
        var textObservations = [VisionResult.TextObservation]()

        let boundingBoxes = results.compactMap { $0.boundingBox }
        let topCandidates = results.compactMap { $0.topCandidates(1).first }

        for (candidate, boundingBox) in zip(topCandidates, boundingBoxes) {
            textObservations.append(
                VisionResult.TextObservation(
                    text: candidate.string,
                    confidence: candidate.confidence,
                    normalizedRect: VisionResult.VisionRect(rect: boundingBox, visionMethod: .appleVision)
                )
            )
        }

        return VisionResult(textObservations: textObservations)
    }

    /// Starts the text extraction process
    /// - Parameter image: The image which will be used to extract data
    /// Note: croppedImage is used for the OCR
    public func processImage(croppedImage: UIImage, originalImage: UIImage) {
        guard let cgImage = croppedImage.cgImage else {
            print("Failed to get cgimage from input image")
            return
        }

        guard let textRecognitionRequest = self.textRecognitionRequest else {
            print("Text Recognition Request not configured")
            return
        }

        let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        self.currentImageBeingAnalysedCropped = croppedImage
        self.currentImageBeingAnalysedOriginal = originalImage

        do {
            try handler.perform([textRecognitionRequest])
        } catch {
            print(error)
        }
    }

    /// Creates a JSON string from VNRecognizedTextObservation
    /// - Parameters:
    ///   - results: Vision results
    ///   - sourceImage: The image in which the vision observations are made
    fileprivate func getJsonStringResults(_ results: [VNRecognizedTextObservation], croppedImage: UIImage, originalImage: UIImage) -> String? {
        let visionResults = self.preprocessResults(results, sourceImage: croppedImage)
        let scanResult = ScanResult(visionResult: visionResults, croppedImage: croppedImage, originalImage: originalImage)

        do {
            let jsonData = try JSONEncoder().encode(scanResult)
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        } catch {
            NSLog("The results could not be converted to JSON")
            return nil
        }
    }
}

