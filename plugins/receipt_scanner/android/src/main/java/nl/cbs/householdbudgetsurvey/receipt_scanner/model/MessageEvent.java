package nl.cbs.householdbudgetsurvey.receipt_scanner.model;

public class MessageEvent {
    public final String message;
    public MessageEvent(String message) {
        this.message = message;
    }
}
