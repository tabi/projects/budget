package nl.cbs.householdbudgetsurvey.receipt_scanner.view;

public interface PaperRectangleCallback {
    public void paperRectangleIsAllowed(boolean allowed);
}
