package nl.cbs.householdbudgetsurvey.receipt_scanner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import nl.cbs.householdbudgetsurvey.receipt_scanner.model.MessageEvent;

/** ReceiptScannerPlugin */
public class ReceiptScannerPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;

  private Context context;
  private Activity attachedActivity;

  private Result currentResult;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "receipt_scanner");
    channel.setMethodCallHandler(this);

    this.context = flutterPluginBinding.getApplicationContext();
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("openReceiptScanner")) {
      String tips = call.argument("tips");
      String[] tipsList = prepareTipsArgument(tips);

      Intent mainActivityIntent = new Intent(this.attachedActivity, CameraView.class);
      mainActivityIntent.putExtra("tipsList", tipsList);
      this.attachedActivity.startActivity(mainActivityIntent);

      currentResult = result;
      EventBus.getDefault().register(this);

    } else {
      result.notImplemented();
    }
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(MessageEvent event) {
    CameraView.getInstance().finish();

    currentResult.success(event.message);

    EventBus.getDefault().unregister(this);
  };

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  // Activity Aware
  @Override
  public void onAttachedToActivity(ActivityPluginBinding activityPluginBinding) {
    this.attachedActivity = activityPluginBinding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() { }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding activityPluginBinding) {
    // after a configuration change.
    this.attachedActivity = activityPluginBinding.getActivity();
  }

  @Override
  public void onDetachedFromActivity() { }

  // MARK: - Helpers

  /**
   * Prepares the string tips array (i.e. '["X", "Y"]') to a string array
   * @param tipsArg The string which has to be transformed to a string array
   * @return The string array
   */
  private String[] prepareTipsArgument(String tipsArg) {
    String[] tips = tipsArg.replaceAll("(\\[|\\])", "").split(",");
    List<String> tipsList = Arrays.asList(tips);
    String[] transformedTipsList = new String[tipsList.size()];

    int index = 0;
    for (String tip : tipsList) {
      tip = tip.trim();
      tip = tip.replace("\"", "");
      transformedTipsList[index] = tip;
      index += 1;
    }

    return transformedTipsList;
  }
}
