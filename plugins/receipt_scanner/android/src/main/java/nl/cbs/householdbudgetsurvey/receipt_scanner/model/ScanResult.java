package nl.cbs.householdbudgetsurvey.receipt_scanner.model;

import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class ScanResult {
    public VisionResult visionResult;

    public VisionResult.Size croppedImageSize;
    public String originalImageBase64;

    public VisionResult.Size originalImageSize;
    public String croppedImageBase64;

    public ScanResult(VisionResult visionResults, Bitmap originalImage, Bitmap croppedImage) {
        this.visionResult = visionResults;

        this.originalImageSize = new VisionResult.Size(originalImage.getWidth(), originalImage.getHeight());
        this.originalImageBase64 = encodeBitmapBase64(originalImage);

        this.croppedImageSize = new VisionResult.Size(croppedImage.getWidth(), croppedImage.getHeight());
        this.croppedImageBase64 = encodeBitmapBase64(croppedImage);
    }

    private String encodeBitmapBase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        return Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
    }
}
