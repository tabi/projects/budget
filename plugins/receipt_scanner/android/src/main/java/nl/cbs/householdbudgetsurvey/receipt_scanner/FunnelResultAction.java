package nl.cbs.householdbudgetsurvey.receipt_scanner;

public enum FunnelResultAction {
    SHOW,
    SHOW_AND_AUTO_SCAN
}
