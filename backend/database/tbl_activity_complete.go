package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Activity_Complete(tx *sql.Tx, sync_id int64, p *global.ActivityComplete) error {
	sqlStatement := "" +
		"INSERT INTO tbl_activity_complete (sync_id, year, month, day) " +
		"VALUES ($1, $2, $3, $4) "

	_, err = tx.Exec(sqlStatement, sync_id, p.Year, p.Month, p.Day )
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Activity_Complete")
	return nil
}

func Delete_Activity_Completes(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_activity_complete " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Activity_Complete")
	return nil
}

func Exists_Activity_Completes(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM tbl_activity_complete WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scan_Activity_Complete_Row(rows *sql.Rows) global.ActivityComplete {	
	var sync_id int64
	var year string
	var month string
	var day string

	err = rows.Scan(&sync_id, &year, &month, &day)
	if err != nil {
		panic(err)
	}
	return global.ActivityComplete{
		Year:   year,
		Month:  month,
		Day: 	day}
}

func Select_Activity_Completes(sync_id int64) ([]global.ActivityComplete, error) {
	var activityCompletes []global.ActivityComplete

	sqlStatement := "" +
		"SELECT sync_id, year, month, day " +
		"FROM tbl_activity_complete " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return activityCompletes, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		activityCompletes = append(activityCompletes, scan_Activity_Complete_Row(rows))
		found = true
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return activityCompletes, err
	}

	if found {
		return activityCompletes, nil
	} else {
		return activityCompletes, errors.New("Error: Activity_Completes not found!")
	}
}
