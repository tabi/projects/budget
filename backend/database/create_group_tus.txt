

CREATE TEMP TABLE lot (v text);
insert into lot(v) values('20201106');

--delete from public.tbl_group_info where group_id in (select id from public.tbl_group where substr(name,1,8) = (select v from lot));
--delete from public.tbl_user	    where group_id in (select id from public.tbl_group where substr(name,1,8) = (select v from lot));
delete from public.tbl_group where substr(name,1,8) = (select v from lot);

CREATE TEMP TABLE app (v text);
insert into app(v) values('TUS');

CREATE TEMP TABLE feedback (v text);
insert into feedback(v) values('directly');
insert into feedback(v) values('afterExperiment');
insert into feedback(v) values('none');

CREATE TEMP TABLE plausibility (v text);
insert into plausibility(v) values('soft');
insert into plausibility(v) values('hard');

CREATE TEMP TABLE experience (v text);
insert into experience(v) values('heavy');
insert into experience(v) values('mild');
insert into experience(v) values('none');

CREATE TEMP TABLE input (v text);
insert into input(v) values('closed');
insert into input(v) values('half_open');

CREATE TEMP TABLE main_tree (v text);
insert into main_tree(v) values('T20200908');

CREATE TEMP TABLE side_tree (v text);
insert into side_tree(v) values('T20200908');

CREATE TEMP TABLE combine (
lot text, app text, feedback text, plausibility text, experience text, input text, main_tree text, side_tree text, name text);
insert into combine(lot , app , feedback , plausibility , experience , input , main_tree , side_tree , name)
select l.v, a.v, f.v, p.v, e.v, i.v, m.v, s.v, 
concat(l.v, ' ', a.v, ' F.', substr(f.v,1,1),  ' P.',  substr(p.v,1,1), ' E.', substr(e.v,1,1), ' I.', substr(i.v,1,1), ' M.', m.v, ' S.', s.v)
from lot l
join app a on true
join feedback f on true
join plausibility p on true
join experience e on true 
join input i on true
join main_tree m on true
join side_tree s on true;

CREATE TEMP TABLE combine_users (id BIGSERIAL PRIMARY KEY, 
lot text, app text, feedback text, plausibility text, experience text, input text, main_tree text, side_tree text, name text, nn int);
insert into combine_users(lot , app , feedback , plausibility , experience , input , main_tree , side_tree , name, nn)
select c.lot , c.app , c.feedback , c.plausibility , c.experience , c.input , c.main_tree , c.side_tree , c.name, nn.nn
from combine c
join (select generate_series(1,10) as nn) nn on true
order by c.name, nn.nn;

CREATE TEMP TABLE users (n int, u text);
insert into users(n) select generate_series(1,10000);
update users set u = substr((1000000 + floor(random() * 1000000)::int)::varchar(8), 2,6);

CREATE TEMP TABLE passwords (id BIGSERIAL PRIMARY KEY, u text, p text);
insert into passwords(u) select distinct u from users where u not in (SELECT name FROM public.tbl_user);
update passwords set p = substr((1000000 + floor(random() * 1000000)::int)::varchar(8), 2,6);

CREATE TEMP TABLE combine_passwords (id BIGSERIAL PRIMARY KEY, 
lot text, app text, feedback text, plausibility text, experience text, input text, main_tree text, side_tree text, name text, nn int, uu text, pp text);
insert into combine_passwords(lot , app , feedback , plausibility , experience , input , main_tree , side_tree , name, nn, uu, pp)
select c.lot , c.app , c.feedback , c.plausibility , c.experience , c.input , c.main_tree , c.side_tree , c.name, c.nn,
p.u, p.p
from combine_users c
join passwords p on c.id = p.id;


INSERT INTO public.tbl_group(name, status)
select c.name, 'enabled' from combine c;

INSERT INTO public.tbl_group_info(group_id, key, value)
select (select id from public.tbl_group where name = g.name), 'app', g.app from combine g
union 
select (select id from public.tbl_group where name = g.name), 'input', g.input from combine g
union 
select (select id from public.tbl_group where name = g.name), 'main_tree', g.main_tree from combine g
union 
select (select id from public.tbl_group where name = g.name), 'side_tree', g.side_tree from combine g
union 
select (select id from public.tbl_group where name = g.name), 'feedback', g.feedback from combine g
union 
select (select id from public.tbl_group where name = g.name), 'plausibility', g.plausibility from combine g
union 
select (select id from public.tbl_group where name = g.name), 'experience', g.experience from combine g;

INSERT INTO public.tbl_user(group_id, name, password, sync_order)
select (select id from public.tbl_group where name = u.name), u.uu, crypt(u.pp, gen_salt('bf', 8)), 0 from combine_passwords u;


select * from combine_passwords