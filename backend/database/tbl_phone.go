package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)


func Create_Phone(user global.User, phone global.Phone) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO tbl_phone (user_id, name, sync_order) " +
		"VALUES ($1, $2, $3) " +
		"RETURNING id "

	var id int64 = -1

	row := con.QueryRow(sqlStatement, user.Id, phone.Name, phone.Sync_Order)
	switch err = row.Scan(&id); err {
	case nil:
		return id, nil
	default:
		return id, err
	}
}

func Update_Phone(tx *sql.Tx, phone global.Phone) error {
	sqlStatement := "" +
		"UPDATE tbl_phone " +
		"SET name=$1, sync_order=$2 " +
		"WHERE id=$3 "


	_, err = tx.Exec(sqlStatement, phone.Name, phone.Sync_Order, phone.Id)
	if err != nil {
		return err
	}

	global.MyPrint("& Updated Phone")
	return nil
}

func scan_Phone_Row(rows *sql.Rows) global.Phone {
	var id int64
	var user_id int64
	var name string
	var sync_order int64
	err = rows.Scan(&id, &user_id, &name, &sync_order)
	if err != nil {
		panic(err)
	}
	return global.Phone{
		Id:         id,
		User_Id:	user_id,
		Name:       name,
		Sync_Order:		sync_order}
}

func Select_Phone_ByUserIdName(phone global.Phone) (global.Phone, error) {
	var foundPhone global.Phone

	sqlStatement := "" +
		"SELECT id, user_id, name, sync_order " +
		"FROM tbl_phone " +
		"WHERE user_id=$1 AND name=$2 LIMIT 1 "

	rows, err := con.Query(sqlStatement, phone.User_Id, phone.Name)
	if err != nil {
		return foundPhone, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		foundPhone = scan_Phone_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return foundPhone, err
	}

	if found {
		return foundPhone, nil
	} else {
		return foundPhone, errors.New("Error: Phone not found")
	}
}

func Select_Phone_ById(id int64) (global.Phone, error) {
	var phone global.Phone

	sqlStatement := "" +
	    "SELECT id, user_id, name, sync_order " +
		"FROM tbl_phone " +
		"WHERE id=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, id)
	if err != nil {
		return phone, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		phone = scan_Phone_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return phone, err
	}

	if found {
		return phone, nil
	} else {
		return phone, errors.New("Error: Phone not found")
	}
}
