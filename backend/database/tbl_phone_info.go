package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_PhoneInfo(phone_info global.Phone_Info) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO tbl_phone_info (phone_id, key, value) " +
		"VALUES ($1, $2, $3) " +
		"RETURNING id "

	var id int64 = -1

	row := con.QueryRow(sqlStatement, phone_info.Phone_Id, phone_info.Key, phone_info.Value)
	switch err = row.Scan(&id); err {
	case nil:
		return id, nil
	default:
		return id, err
	}
}

func Update_PhoneInfo(phone_info global.Phone_Info) error {
	sqlStatement := "" +
		"UPDATE tbl_phone_info " +
		"SET value=$1 " +
		"WHERE key=$2 AND phone_id=$3"

	_, err = con.Exec(sqlStatement, phone_info.Value, phone_info.Key, phone_info.Phone_Id)
	if err != nil {
		return err
	}

	return nil
}

func Delete_PhoneInfos(phone global.Phone) error {
	sqlStatement := "" +
		"DELETE FROM tbl_phone_info " +
		"WHERE phone_id=$1 "

	_, err = con.Exec(sqlStatement, phone.Id)
	if err != nil {
		return err
	}

	return nil
}

func scan_PhoneInfo_Row(rows *sql.Rows) global.Phone_Info {
	var id int64
	var phone_id int64
	var key string
	var value string
	err = rows.Scan(&id, &phone_id, &key, &value)
	if err != nil {
		panic(err)
	}
	return global.Phone_Info{
		Id: 		id,
		Phone_Id:   phone_id,
		Key:   		key,
		Value:  	value}
}

func Select_PhoneInfo_ByPhoneIdKey(phone_info global.Phone_Info) (global.Phone_Info, error) {
	var foundPhoneInfo global.Phone_Info

	sqlStatement := "" +
		"SELECT id, phone_id, key, value " +
		"FROM tbl_phone_info " +
		"WHERE phone_id=$1 AND key=$2 LIMIT 1 "

	rows, err := con.Query(sqlStatement, phone_info.Phone_Id, phone_info.Key)
	if err != nil {
		return foundPhoneInfo, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		foundPhoneInfo = scan_PhoneInfo_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return foundPhoneInfo, err
	}

	if found {
		return foundPhoneInfo, nil
	} else {
		return foundPhoneInfo, errors.New("Error: No PhoneInfo found")
	}
}

func Select_PhoneInfos_ByPhoneId(phone_id int64) ([]global.Phone_Info, error) {
	var phone_infos []global.Phone_Info

	sqlStatement := "" +
		"SELECT id, phone_id, key, value " +
		"FROM tbl_phone_info " +
		"WHERE phone_id=$1 "

	rows, err := con.Query(sqlStatement, phone_id)
	if err != nil {
		return phone_infos, err
	}
	defer rows.Close()

	for rows.Next() {
		phone_infos = append(phone_infos, scan_PhoneInfo_Row(rows))
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return phone_infos, err
	}

	return phone_infos, nil
}

