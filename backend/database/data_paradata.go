package database

import (
	_ "github.com/lib/pq"
	"backend/global"
)

// ### ACTIVITY_DATA ######################################################################################

func PushNextParadataData(body global.ParadataBody) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errP error

	user, _ = Select_User_ById(body.Synchronisation.User_Id)
	phone, _ = Select_Phone_ById(body.Synchronisation.Phone_Id)

	var pushed int64 = body.Synchronisation.Sync_Order
	var latest int64 = user.Sync_Order
	latest = latest + 1
	body.Synchronisation.Sync_Order = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = Select_Synchronisation_ByDataIdentifier(tx,
			body.Synchronisation.Data_Type,
			body.Synchronisation.Data_Identifier, body.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = Create_Synchronisation(tx, body.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			body.Synchronisation.Id = synchronisation.Id
			updated, err = Update_Synchronisation(tx, body.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					//DO NOT DELETE PARADATA
					addData = true
					
				}
			}
		}

		if validTransaction {
			if addData {
				for _, paradata := range body.Paradatas {
					errP = Create_Paradata(tx, sync_id, &paradata)
					if errP != nil {
						break
					}
				}
			}
		}

		if validTransaction {
			user.Sync_Order = latest
			err = Update_UserSyncOrder(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Sync_Order = pushed
				err = Update_Phone(tx, phone)
				if err != nil {

					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}


func GetParadataData(sync_id int64) global.ParadataData {
	var synchronisation global.Synchronisation
	var paradatas []global.Paradata //dummy return

	synchronisation, _ = Select_Synchronisation_ById(sync_id)

	var data global.ParadataData
	data.Synchronisation = &synchronisation
	data.Paradatas = paradatas

	return data
}
