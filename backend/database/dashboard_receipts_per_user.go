package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func scan_ReceiptsPerUser_Row(rows *sql.Rows) global.ReceiptsPerUser {
	var groupName string
	var userName string
	var firstDay int
	var lastDay int
	var days int64
	var receipts int64
	var photos int64
	var totalPrice float32

	err = rows.Scan(&groupName, &userName, &firstDay, &lastDay, &days, &receipts, &photos, &totalPrice)
	if err != nil {
		panic(err)
	}
	return global.ReceiptsPerUser{
		GroupName: 	groupName,
		UserName:   userName,
		FirstDay:   firstDay,
		LastDay:   	lastDay,
		Days:   	days,
		Receipts:   receipts,
		Photos:   	photos,
		TotalPrice: totalPrice}
}

func select_ReceiptsPerUser(group_id int64) ([]global.ReceiptsPerUser, error) {
	var receiptsPerUsers []global.ReceiptsPerUser
	
	sqlStatement := "" +				
	"SELECT g.name as Group_Name, u.name as User_Name, " + 
	" 		coalesce(fd.first_day, 0) as first_day, coalesce(ld.last_day, 0) as last_day,  " +
	"		coalesce(nd.days, 0) as days, coalesce(nr.receipts, 0) as receipts, " + 
	"       coalesce(np.photos, 0) as photos, coalesce(tp.total_price, 0) as total_price " +
	"FROM tbl_user u   " +
	"left join tbl_group g on u.group_id = g.id  " +
	"left join ( SELECT st.user_id, min(date) as first_day " +
	"			FROM tbl_receipt_transaction t  " +
	"			left join tbl_sync st on t.sync_id = st.id " + 
	"			group by st.user_id " +
	"		) fd on fd.user_id = u.id  " +
	"left join ( SELECT st.user_id, max(date) as last_day " +
	"			FROM tbl_receipt_transaction t  " +
	"			left join tbl_sync st on t.sync_id = st.id " + 
	"			group by st.user_id " +
	"		) ld on ld.user_id = u.id  " +
	"left join ( SELECT user_id, count(date) as days " +
	"			FROM (	SELECT distinct st.user_id, date " +
	"					FROM tbl_receipt_transaction t  " +
	"					left join tbl_sync st on t.sync_id = st.id " + 
	"				) x " +
	"			group by user_id " +
	"		) nd on nd.user_id = u.id " + 		
	"left join ( SELECT st.user_id, count(transaction_id) as receipts " +
	"			FROM tbl_receipt_transaction t  " +
	"			left join tbl_sync st on t.sync_id = st.id " + 
	"			group by st.user_id " +
	"		) nr on nr.user_id = u.id " +	
	"left join ( SELECT st.user_id, count(transaction_id) as photos " +
	"			FROM tbl_receipt_transaction t  " +
	"			left join tbl_sync st on t.sync_id = st.id " + 
	"			where t.receipt_location <> '' " +
	"			group by st.user_id " +
	"		) np on np.user_id = u.id  " +
	"left join ( SELECT st.user_id, sum(total_price) as total_price " +
	"			FROM tbl_receipt_transaction t  " +
	"			left join tbl_sync st on t.sync_id = st.id " + 
	"			group by st.user_id " +
	"		) tp on tp.user_id = u.id  " +
	"where g.id = $1 " +
	"order by u.name " 

	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return receiptsPerUsers, err
	}
	defer rows.Close()
	
	var found bool = false
	for rows.Next() {
		receiptsPerUsers = append(receiptsPerUsers, scan_ReceiptsPerUser_Row(rows))
		found = true
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return receiptsPerUsers, err
	}

	if found {
		return receiptsPerUsers, nil
	} else {
		return receiptsPerUsers, errors.New("Error: No ReceiptsPerUsers foud")
	}
}


func Get_ReceiptsPerUser(group_id int64) (global.ReceiptsPerUserData, error) {
	var receiptsPerUsers []global.ReceiptsPerUser
	receiptsPerUsers, err = select_ReceiptsPerUser(group_id)

	var data global.ReceiptsPerUserData
	data.ReceiptsPerUsers = receiptsPerUsers

	return data, nil
}


