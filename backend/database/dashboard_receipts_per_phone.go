package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func scan_ReceiptsPerPhone_Row(rows *sql.Rows) global.ReceiptsPerPhone {
	var groupName string
	var userName string
	var phoneName string
	var phoneType string
	var phoneManufacturer string
	var phoneModel string
	var syncOrder int64
	var receipts int64
	var products int64
	var photos int64

	err = rows.Scan(&groupName, &userName, &phoneName, &phoneType, 
		&phoneManufacturer, &phoneModel, &syncOrder, &receipts, &products, &photos )
	if err != nil {
		panic(err)
	}
	return global.ReceiptsPerPhone{
		GroupName: groupName, 
		UserName: userName, 
		PhoneName: phoneName,  
		PhoneType: phoneType, 
		PhoneManufacturer: phoneManufacturer, 
		PhoneModel: phoneModel, 
		SyncOrder: syncOrder, 
		Receipts: receipts, 
		Products: products, 
		Photos: photos}
}

func select_ReceiptsPerPhone(group_id int64) ([]global.ReceiptsPerPhone, error) {
	var receiptsPerPhones []global.ReceiptsPerPhone
	
	sqlStatement := "" +	
	"SELECT g.name as Group_Name, u.name as User_Name, p.name as Phone_Name,  " +
	"pityp.value as Phone_Type, piman.value as Phone_Manufacturer, pimod.value as Phone_Model, " +
	"coalesce(p.sync_order, 0) as sync_order, coalesce(tran.receipts, 0) as receipts, " + 
	"coalesce(prod.products, 0) as products, coalesce(imag.photos, 0) as photos " +
	"	FROM public.tbl_phone p  " +
	"	left join tbl_user u on p.user_id = u.id " +
	"	left join tbl_group g on u.group_id = g.id " +
	"	left join tbl_phone_info pimod on p.id = pimod.phone_id and pimod.key = 'model' " +
	"	left join tbl_phone_info piman on p.id = piman.phone_id and piman.key = 'manufacturer' " +
	"	left join tbl_phone_info pityp on p.id = pityp.phone_id and pityp.key = 'phoneType' " +
	"	left join ( SELECT st.user_id, count(sync_id) as receipts " +
	"				FROM tbl_receipt_transaction t " +
	"				left join tbl_sync st on t.sync_id = st.id " +
	"				group by st.user_id " +
	"			) tran on tran.user_id = u.id " +
	"	left join ( SELECT sp.user_id, count(sync_id) as products " +
	"				FROM tbl_receipt_product p " +
	"				left join tbl_sync sp on p.sync_id = sp.id " +
	"				group by sp.user_id " +
	"			) prod on prod.user_id = u.id " +
	"	left join ( SELECT si.user_id, count(sync_id) as photos " +
	"				FROM tbl_receipt_image i " +
	"				left join tbl_sync si on i.sync_id = si.id " +
	"				group by si.user_id " +
	"			) imag on imag.user_id = u.id " +
	"	where g.id = $1 " +
	"	order by u.name, p.name "
	
	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return receiptsPerPhones, err
	}
	defer rows.Close()
	
	var found bool = false
	for rows.Next() {
		receiptsPerPhones = append(receiptsPerPhones, scan_ReceiptsPerPhone_Row(rows))
		found = true
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return receiptsPerPhones, err
	}

	if found {
		return receiptsPerPhones, nil
	} else {
		return receiptsPerPhones, errors.New("Error: No ReceiptsPerPhones foud")
	}
}


func Get_ReceiptsPerPhone(group_id int64) (global.ReceiptsPerPhoneData, error) {
	var receiptsPerPhones []global.ReceiptsPerPhone
	receiptsPerPhones, err = select_ReceiptsPerPhone(group_id)

	var data global.ReceiptsPerPhoneData
	data.ReceiptsPerPhones = receiptsPerPhones

	return data, nil
}


