package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func Create_Group(group global.Group) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO tbl_group (name, status) " +
		"VALUES ($1, $2) " +
		"RETURNING id "

	var id int64 = -1

	row := con.QueryRow(sqlStatement, group.Name, group.Status)
	switch err = row.Scan(&id); err {
	case nil:
		return id, nil
	default:
		return id, err
	}
}

func Update_Group(group global.Group) error {
	sqlStatement := "" +
		"UPDATE tbl_group " +
		"SET name=$1, status=$2 " +
		"WHERE id=$3 "

	_, err = con.Exec(sqlStatement, group.Name, group.Status, group.Id)
	if err != nil {
		return err
	}

	return nil
}

func Delete_Group(group_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_user " +
		"WHERE group_id=$1 "

	_, err = con.Exec(sqlStatement, group_id)
	if err != nil {
		return err
	}

	sqlStatement = "" +
		"DELETE FROM tbl_group " +
		"WHERE id=$1 "

	_, err = con.Exec(sqlStatement, group_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Group")
	return nil
}

func scan_Group_Row(rows *sql.Rows) global.Group {
	var id int64
	var name string
	var status string
	var users int64
	err = rows.Scan(&id, &name, &status, &users)
	if err != nil {
		panic(err)
	}
	return global.Group{
		Id: 		id,
		Name:   	name,
		Status:  	status,
		Users:		users}
}

func Select_Group_ByName(name string) (global.Group, error) {
	var group global.Group

	sqlStatement := "" +
		"SELECT id, name, status, COALESCE(users, 0) as users " +
		"FROM tbl_group g " +
		"LEFT JOIN (SELECT group_id, count(name) as users FROM tbl_user GROUP BY group_id) u on g.id = u.group_id " + 
		"WHERE name=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, name)

	if err != nil {
		return group, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		group = scan_Group_Row(rows)
		found = true
		break
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return group, err
	}

	if found {
		return group, nil
	} else {
		return group, errors.New("Error: No Group foud")
	}
}

func Select_Group_ById(id int64) (global.Group, error) {
	var group global.Group

	sqlStatement := "" +
		"SELECT id, name, status, COALESCE(users, 0) as users " +
		"FROM tbl_group g " +
		"LEFT JOIN (SELECT group_id, count(name) as users FROM tbl_user GROUP BY group_id) u on g.id = u.group_id " + 
		"WHERE id=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, id)

	if err != nil {
		return group, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		group = scan_Group_Row(rows)
		found = true
		break
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return group, err
	}

	if found {
		return group, nil
	} else {
		return group, errors.New("Error: No Group foud")
	}
}

func Select_Groups() ([]global.Group, error) {
	var groups []global.Group

	sqlStatement := "" +
		"SELECT id, name, status, COALESCE(users, 0) as users " +
		"FROM tbl_group g " +
		"LEFT JOIN (SELECT group_id, count(name) as users FROM tbl_user GROUP BY group_id) u on g.id = u.group_id " 

	rows, err := con.Query(sqlStatement)
	if err != nil {
		return groups, err
	}
	defer rows.Close()

	for rows.Next() {
		groups = append(groups, scan_Group_Row(rows))
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return groups, err
	}

	return groups, nil
}



