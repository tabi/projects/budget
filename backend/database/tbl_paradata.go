package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Paradata(tx *sql.Tx, sync_id int64, p *global.Paradata) error {
	sqlStatement := "" +
		"INSERT INTO tbl_paradata (sync_id, action, objectName, timestamp) " +
		"VALUES ($1, $2, $3, $4) "

	_, err = tx.Exec(sqlStatement, sync_id, p.Action, p.ObjectName, p.Timestamp )
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Paradata")
	return nil
}

func Delete_Paradatas(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_paradata " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Paradatas")
	return nil
}

func Exists_Paradatas(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM tbl_paradata WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scan_Paradata_Row(rows *sql.Rows) global.Paradata {	
	var sync_id int64
	var action string
	var objectName string
	var timestamp int64

	err = rows.Scan(&sync_id, &action, &objectName, &timestamp)
	if err != nil {
		panic(err)
	}
	return global.Paradata{
		Action:   	action,
		ObjectName: objectName,
		Timestamp: 	timestamp}
}

func Select_Paradatas(sync_id int64) ([]global.Paradata, error) {
	var paradatas []global.Paradata

	sqlStatement := "" +
		"SELECT sync_id, action, objectName, timestamp " +
		"FROM tbl_paradata " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return paradatas, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		paradatas = append(paradatas, scan_Paradata_Row(rows))
		found = true
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return paradatas, err
	}

	if found {
		return paradatas, nil
	} else {
		return paradatas, errors.New("Error: Paradatas not found!")
	}
}
