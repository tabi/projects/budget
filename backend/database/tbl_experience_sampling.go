package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Experience_Sampling(tx *sql.Tx, sync_id int64, p *global.ExperienceSampling) error {
	sqlStatement := "" +
		"INSERT INTO tbl_experience_sampling (sync_id, sendtime, readtime, " + 
		"isfeelingquestionair, isfirstattempt, happy, energetic, relaxed, cheerful, " + 
		"socialmedia, texting, games, newsonline, readingpaper) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) "

	_, err = tx.Exec(sqlStatement, sync_id,	p.SendTime, p.ReadTime, p.IsFeelingQuestionair, p.IsFirstAttempt,
		p.Happy, p.Energetic, p.Relaxed, p.Cheerful, p.SocialMedia, p.Texting, p.Games, p.NewsOnline, p.ReadingPaper )
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Experience_Sampling")
	return nil
}

func Delete_Experience_Sampling(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_experience_sampling " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Experience_Sampling")
	return nil
}

func Exists_Experience_Sampling(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM tbl_experience_sampling WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scan_Experience_Sampling_Row(rows *sql.Rows) global.ExperienceSampling {
	var sync_id int64
	var sendTime string  
	var readTime string  	
	var isFeelingQuestionair int  
	var isFirstAttempt int  
	var happy int  		
	var energetic int  	
	var relaxed int  			
	var cheerful int  			
	var socialMedia int  		
	var texting int  			
	var games int  			
	var newsOnline int  	
	var readingPaper int  	

	err = rows.Scan(&sync_id, &sendTime, &readTime, &isFeelingQuestionair, &isFirstAttempt,
		&happy, &energetic, &relaxed, &cheerful, &socialMedia, &texting, &games, &newsOnline, &readingPaper)
	if err != nil {
		panic(err)
	}
	return global.ExperienceSampling{
		SendTime: 					sendTime,
		ReadTime:					readTime,
		IsFeelingQuestionair:		isFeelingQuestionair,
		IsFirstAttempt:				isFirstAttempt,
		Happy:						happy,
		Energetic:					energetic,
		Relaxed:					relaxed,
		Cheerful:					cheerful,
		SocialMedia:				socialMedia,
		Texting:					texting,
		Games:						games,
		NewsOnline:					newsOnline,
		ReadingPaper:				readingPaper}
}

func Select_Experience_Sampling(sync_id int64) (global.ExperienceSampling, error) {
	var experienceSampling global.ExperienceSampling	

	sqlStatement := "" +
		"SELECT sync_id, sendtime, readtime, " + 
		"isfeelingquestionair, isfirstattempt, happy, energetic, relaxed, cheerful, " + 
		"socialmedia, texting, games, newsonline, readingpaper " +
		"FROM tbl_experience_sampling " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return experienceSampling, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		experienceSampling = scan_Experience_Sampling_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return experienceSampling, err
	}

	if found {
		return experienceSampling, nil
	} else {
		return experienceSampling, errors.New("Error: Experience_Sampling not found!")
	}
}



