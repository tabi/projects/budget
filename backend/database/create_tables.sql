DROP TABLE public.tbl_activity_info;
DROP TABLE public.tbl_activity_property;
DROP TABLE public.tbl_activity_complete;
DROP TABLE public.tbl_start_questionnaire;
DROP TABLE public.tbl_experience_sampling;
DROP TABLE public.tbl_paradata;


DROP TABLE public.tbl_receipt_image;
DROP TABLE public.tbl_receipt_product;
DROP TABLE public.tbl_receipt_transaction;
DROP TABLE public.tbl_search_suggestions_products;
DROP TABLE public.tbl_search_suggestions_stores;
DROP TABLE public.tbl_sync;

DROP TABLE public.tbl_phone_info;
DROP TABLE public.tbl_phone;
DROP TABLE public.tbl_user;
DROP TABLE public.tbl_group_info;
DROP TABLE public.tbl_group;

-- ###########################################################

CREATE TABLE public.tbl_group
(
    id BIGSERIAL PRIMARY KEY,
    name text NOT NULL,
    status text NOT NULL
)

TABLESPACE pg_default;

ALTER TABLE public.tbl_group
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";
    
-- ###########################################################	
	
CREATE TABLE public.tbl_group_info
(
    id BIGSERIAL PRIMARY KEY,
    group_id bigint NOT NULL,
    key text NOT NULL,
    value text NOT NULL,
    CONSTRAINT tbl_group_info_fkey FOREIGN KEY (group_id)
        REFERENCES public.tbl_group (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tbl_group_info
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";
	
-- ###########################################################

CREATE TABLE public.tbl_user
(
    id BIGSERIAL PRIMARY KEY,
    group_id bigint NOT NULL,
    name text NOT NULL,
    password text NOT NULL,
    sync_order bigint NOT NULL,
    CONSTRAINT tbl_user_fkey FOREIGN KEY (group_id)
        REFERENCES public.tbl_group (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tbl_user
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";
	
-- ###########################################################	
		
CREATE TABLE public.tbl_phone
(
    id BIGSERIAL PRIMARY KEY,
    user_id bigint NOT NULL,
    name text NOT NULL,
    sync_order bigint NOT NULL,
    CONSTRAINT tbl_phone_fkey FOREIGN KEY (user_id)
        REFERENCES public.tbl_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tbl_phone
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";	

-- ###########################################################

CREATE TABLE public.tbl_phone_info
(
    id BIGSERIAL PRIMARY KEY,
    phone_id bigint NOT NULL,
    key text NOT NULL,
    value text NOT NULL,
    CONSTRAINT tbl_phone_info_fkey FOREIGN KEY (phone_id)
        REFERENCES public.tbl_phone (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tbl_phone_info
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";
	
-- ###########################################################	
	
CREATE TABLE public.tbl_sync
(
    id BIGSERIAL PRIMARY KEY,
    data_type integer NOT NULL,
    data_identifier TEXT NOT NULL,
    user_id bigint NOT NULL,
    phone_id bigint NOT NULL,
    sync_order bigint NOT NULL,
    sync_time bigint NOT NULL,
    action integer NOT NULL,
    CONSTRAINT tbl_sync_phone_id_fkey FOREIGN KEY (phone_id)
        REFERENCES public.tbl_phone (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT tbl_sync_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.tbl_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_sync
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";



CREATE TABLE public.tbl_receipt_transaction 
(
    sync_id bigint NOT NULL,
    transaction_id TEXT,
    store TEXT,
    store_type TEXT,
    "date" int,
    discount_amount TEXT,
    discount_percentage TEXT,
    expense_abroad TEXT,
    expense_online TEXT,
    total_price float,
    discount_text TEXT,
    receipt_location TEXT,
    receipt_producttype TEXT,
    CONSTRAINT tbl_receipt_transaction_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_receipt_transaction
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";



CREATE TABLE public.tbl_receipt_product 
(
    sync_id bigint NOT NULL,
    transaction_id TEXT,
    product TEXT,
    product_category TEXT,
    price float,
    product_code TEXT,
    product_date TEXT,
    product_group_id TEXT,
    CONSTRAINT tbl_receipt_product_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_receipt_product
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";



CREATE TABLE public.tbl_receipt_image 
(
    sync_id bigint NOT NULL,
    transaction_id TEXT,
    "image" bytea,
    CONSTRAINT tbl_receipt_image_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_receipt_image
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";




CREATE TABLE public.tbl_search_suggestions_products
(
    sync_id bigint NOT NULL,
    product TEXT,
    productCategory TEXT,
    productCode TEXT,
    lastAdded bigint NOT NULL,
    "count" bigint NOT NULL,
    CONSTRAINT tbl_search_suggestions_products_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_search_suggestions_products
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";



CREATE TABLE public.tbl_search_suggestions_stores
(
    sync_id bigint NOT NULL,
    storeName TEXT,
    storeType TEXT,
    lastAdded bigint NOT NULL,
    "count" bigint NOT NULL,
    CONSTRAINT tbl_search_suggestions_stores_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_search_suggestions_stores
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";





CREATE TABLE public.tbl_activity_info
(
    sync_id bigint NOT NULL,
    treeCode TEXT,
    dateId bigint NOT NULL,
    activityType bigint NOT NULL,
    nodeCode TEXT,
    "description" TEXT,
    tick bigint NOT NULL,
    height bigint NOT NULL,
    startHour bigint NOT NULL,
    startMinute bigint NOT NULL,
    stopHour bigint NOT NULL,
    stopMinute bigint NOT NULL,
    CONSTRAINT tbl_activity_info_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_activity_info
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";



CREATE TABLE public.tbl_activity_complete
(
    sync_id bigint NOT NULL,
    year TEXT,
    month TEXT,
    day TEXT,
    CONSTRAINT tbl_activity_complete_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_activity_complete
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";



CREATE TABLE public.tbl_activity_property
(
    sync_id bigint NOT NULL,
    treeCode TEXT,
    dateId bigint NOT NULL,
    activityType bigint NOT NULL,
    startHour bigint NOT NULL,
    startMinute bigint NOT NULL,
    key TEXT,
    value TEXT,
    CONSTRAINT tbl_activity_property_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_activity_property
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";


    



CREATE TABLE public.tbl_start_questionnaire 
(
    sync_id bigint NOT NULL,
    results TEXT,
    CONSTRAINT tbl_start_questionnairen_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_start_questionnaire
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";


CREATE TABLE public.tbl_experience_sampling 
(
    sync_id bigint NOT NULL,
            sendTime TEXT,
            readTime TEXT,
            isFeelingQuestionair INT,
            isFirstAttempt INT,            
            happy INT,
            energetic INT,
            relaxed INT,
            cheerful INT,
            socialMedia INT,
            texting INT,
            games INT,
            newsOnline INT,
            readingPaper INT,
    CONSTRAINT tbl_experience_sampling_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_experience_sampling
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";


CREATE TABLE public.tbl_paradata
(
    sync_id bigint NOT NULL,
    action TEXT,
    objectName TEXT,
    timestamp bigint NOT NULL,
    CONSTRAINT tbl_paradata_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_activity_property
    OWNER to "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4";