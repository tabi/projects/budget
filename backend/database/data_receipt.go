package database

import (
	"backend/global"
	_ "github.com/lib/pq"
)

func PushNextReceiptData(body global.ReceiptBody, replaceImage bool) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errT error
	var errP error
	var errI error

	user, _ = Select_User_ById(body.Synchronisation.User_Id)
	phone, _ = Select_Phone_ById(body.Synchronisation.Phone_Id)

	var pushed int64 = body.Synchronisation.Sync_Order
	var latest int64 = user.Sync_Order
	latest = latest + 1
	body.Synchronisation.Sync_Order = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = Select_Synchronisation_ByDataIdentifier(tx,
			body.Synchronisation.Data_Type,
			body.Synchronisation.Data_Identifier, body.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = Create_Synchronisation(tx, body.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			body.Synchronisation.Id = synchronisation.Id
			updated, err = Update_Synchronisation(tx, body.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					if replaceImage {
						errI = Delete_Image(tx, sync_id)
					} else {
						errI = nil
					}
					errP = Delete_Products(tx, sync_id)
					errT = Delete_Transaction(tx, sync_id)
					if errT != nil || errP != nil || errI != nil {
						validTransaction = false
					} else {
						addData = true
					}
				}
			}
		}

		if validTransaction {
			if addData {
				errT = Create_Transaction(tx, sync_id, body.Transaction)
				for _, product := range body.Products {
					errP = Create_Product(tx, sync_id, &product)
					if errP != nil {
						break
					}
				}
				if body.Transaction.ReceiptLocation != "" && replaceImage {
					errI = Create_Image(tx, sync_id, body.Image)
				} else {
					errI = nil
				}
				if errT != nil || errP != nil || errI != nil {
					validTransaction = false
				}
			}
		}

		if validTransaction {
			user.Sync_Order = latest
			err = Update_UserSyncOrder(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Sync_Order = pushed
				err = Update_Phone(tx, phone)
				if err != nil {
					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}

func PullNextReceiptData(user_id int64, phone_id int64, pulled_sync_order int64) (global.ReceiptData, error) {
	var synchronisation global.Synchronisation
	var transaction global.Transaction
	var products []global.Product
	var image global.Image

	synchronisation, err = Select_Synchronisation_NextToPull(global.DATA_RECEIPT, user_id, phone_id, pulled_sync_order)

	if synchronisation.Action != global.ACTION_DELETE {
		transaction, err = Select_Transaction(synchronisation.Id)
		products, err = Select_Products(synchronisation.Id)
		if transaction.ReceiptLocation != "" {
			image, err = Select_Image(synchronisation.Id)
		} else {
			image.TransactionID = transaction.TransactionID
			image.Base64image = ""
		}
	}

	var data global.ReceiptData
	data.Synchronisation = &synchronisation
	data.Transaction = &transaction
	data.Products = products
	data.Image = &image

	return data, nil
}

func PullNextReceiptDataForProcess(phone_id int64, group_id int64, sync_id int64, sync_time int64) (global.ReceiptData, error) {
	var synchronisation global.Synchronisation
	var transaction global.Transaction
	var products []global.Product
	var image global.Image

	synchronisation, err = Select_Synchronisation_NextToPullForProcess(global.DATA_RECEIPT, phone_id, group_id, sync_id, sync_time)

	if synchronisation.Action != global.ACTION_DELETE {
		transaction, err = Select_Transaction(synchronisation.Id)
		products, err = Select_Products(synchronisation.Id)
		if transaction.ReceiptLocation != "" {
			image, err = Select_Image(synchronisation.Id)
		} else {
			image.TransactionID = transaction.TransactionID
			image.Base64image = ""
		}
	}

	var data global.ReceiptData
	data.Synchronisation = &synchronisation
	data.Transaction = &transaction
	data.Products = products
	data.Image = &image

	return data, nil
}

func GetReceiptData(sync_id int64) global.ReceiptData {
	var synchronisation global.Synchronisation
	var transaction global.Transaction
	var products []global.Product
	var image global.Image

	synchronisation, _ = Select_Synchronisation_ById(sync_id)
	transaction, _ = Select_Transaction(synchronisation.Id)
	products, _ = Select_Products(synchronisation.Id)
	if transaction.ReceiptLocation != "" {
		image, _ = Select_Image(synchronisation.Id)
	} else {
		image.TransactionID = transaction.TransactionID
		image.Base64image = ""
	}

	var data global.ReceiptData
	data.Synchronisation = &synchronisation
	data.Transaction = &transaction
	data.Products = products
	data.Image = &image

	return data
}

func PullReceiptAtIndex(index int64) (global.ReceiptData, error) {
	var image global.Image
	image, err = Select_Image_By_Index(index)
	var data global.ReceiptData
	data.Image = &image
	return data, nil
}
