package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func Create_GroupInfo(group_info global.Group_Info) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO tbl_group_info (group_id, key, value) " +
		"VALUES ($1, $2, $3) " +
		"RETURNING id "

	var id int64 = -1

	row := con.QueryRow(sqlStatement, group_info.Group_Id, group_info.Key, group_info.Value)
	switch err = row.Scan(&id); err {
	case nil:
		return id, nil
	default:
		return id, err
	}
}

func Update_GroupInfo(group_info global.Group_Info) error {
	sqlStatement := "" +
		"UPDATE tbl_group_info " +
		"SET value=$1 " +
		"WHERE key=$2 AND group_id=$3"

	_, err = con.Exec(sqlStatement, group_info.Value, group_info.Key, group_info.Group_Id)
	if err != nil {
		return err
	}

	return nil
}

func Delete_GroupInfos(group global.Group) error {
	sqlStatement := "" +
		"DELETE FROM tbl_group_info " +
		"WHERE group_id=$1 "

	_, err = con.Exec(sqlStatement, group.Id)
	if err != nil {
		return err
	}

	return nil
}

func scan_GroupInfo_Row(rows *sql.Rows) global.Group_Info {
	var id int64
	var group_id int64
	var key string
	var value string
	err = rows.Scan(&id, &group_id, &key, &value)
	if err != nil {
		panic(err)
	}
	return global.Group_Info{
		Id: 		id,
		Group_Id:   group_id,
		Key:   		key,
		Value:  	value}
}

func Select_GroupInfo_ByGroupIdKey(group_info global.Group_Info) (global.Group_Info, error) {
	var foundGroupSetting global.Group_Info

	sqlStatement := "" +
		"SELECT id, group_id, key, value " +
		"FROM tbl_group_info " +
		"WHERE group_id=$1 AND key=$2 LIMIT 1 "

	rows, err := con.Query(sqlStatement, group_info.Group_Id, group_info.Key)
	if err != nil {
		return foundGroupSetting, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		foundGroupSetting = scan_GroupInfo_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return foundGroupSetting, err
	}

	if found {
		return foundGroupSetting, nil
	} else {
		return foundGroupSetting, errors.New("Error: No GroupSetting foud")
	}
}

func Select_GroupInfos_ByGroup(group_id int64) ([]global.Group_Info, error) {
	var group_infos []global.Group_Info

	sqlStatement := "" +
		"SELECT id, group_id, key, value " +
		"FROM tbl_group_info " +
		"WHERE group_id=$1 "

	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return group_infos, err
	}
	defer rows.Close()

	for rows.Next() {
		group_infos = append(group_infos, scan_GroupInfo_Row(rows))
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return group_infos, err
	}

	return group_infos, nil
}

