package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Synchronisation(tx *sql.Tx, synchronisation *global.Synchronisation) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO tbl_sync (data_type, data_identifier, user_id, phone_id, sync_order, sync_time, action) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7) " +
		"RETURNING id "

	var id int64 = -1

	row := tx.QueryRow(sqlStatement,
		synchronisation.Data_Type,
		synchronisation.Data_Identifier,
		synchronisation.User_Id,
		synchronisation.Phone_Id,
		synchronisation.Sync_Order,
		synchronisation.Sync_Time,
		synchronisation.Action)
	switch err = row.Scan(&id); err {
	case nil:
		global.MyPrint("& Inserted Sync")
		return id, nil
	default:
		return id, err
	}
}

func Update_Synchronisation(tx *sql.Tx, synchronisation *global.Synchronisation) (bool, error) {
	sqlStatement := "" +
		"UPDATE tbl_sync " +
		"SET phone_id=$1, sync_order=$2, sync_time=$3, action=$4 " +
		"WHERE id=$5 AND sync_time<$6 "

	result, err := tx.Exec(sqlStatement,
		synchronisation.Phone_Id,
		synchronisation.Sync_Order,
		synchronisation.Sync_Time,
		synchronisation.Action,
		synchronisation.Id,
		synchronisation.Sync_Time)
	if err != nil {
		return false, err
	}

	var affected int64
	affected, err = result.RowsAffected()
	if affected == 1 {
		global.MyPrint("& Updated Sync")
		return true, nil
	} else {
		return false, errors.New("Error: Could not update Sync!")
	}
}

func scan_Synchronisation_Row(rows *sql.Rows) global.Synchronisation {
	var id int64
	var data_type int
	var data_identifier string
	var user_id int64
	var phone_id int64
	var sync_order int64
	var sync_time int64
	var action int
	err = rows.Scan(&id, &data_type, &data_identifier, &user_id, &phone_id, &sync_order, &sync_time, &action)
	if err != nil {
		panic(err)
	}
	return global.Synchronisation{
		Id:              id,
		Data_Type:		 data_type,
		Data_Identifier: data_identifier,
		User_Id:         user_id,
		Phone_Id:        phone_id,
		Sync_Order:          sync_order,
		Sync_Time:       sync_time,
		Action:          action}
}

func Select_Synchronisation_NextToPull(data_type int, user_id int64, phone_id int64, pulled_sync_order int64) (global.Synchronisation, error) {
	var synchronisation global.Synchronisation

	sqlStatement := "" +
		"SELECT id, data_type, data_identifier, user_id, phone_id, sync_order, sync_time, action " +
		"FROM tbl_sync " +
		"WHERE data_type=$1 AND user_id=$2 AND phone_id<>$3 AND sync_order>$4 " +
		"ORDER BY sync_order "

	rows, err := con.Query(sqlStatement, data_type, user_id, phone_id, pulled_sync_order)
	if err != nil {
		return synchronisation, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		synchronisation = scan_Synchronisation_Row(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return synchronisation, err
	}

	if found {
		return synchronisation, nil
	} else {
		return synchronisation, errors.New("Error: Next Sync not found!")
	}
}

func Select_Synchronisation_NextToPullForProcess(data_type int, phone_id int64, group_id int64, sync_id int64, sync_time int64) (global.Synchronisation, error) {
	var synchronisation global.Synchronisation

	sqlStatement := "" +
		"SELECT s.id, s.data_type, s.data_identifier, s.user_id, s.phone_id, s.sync_order, s.sync_time, s.action " +
		"FROM tbl_sync s " +
		"JOIN tbl_user u on s.user_id = u.id " +
		"JOIN (select sync_id, max(product_code) as code from tbl_receipt_product group by sync_id) p on p.sync_id = s.id " + 
		"WHERE s.data_type=$1 AND u.group_id=$2 AND phone_id<>$3 AND ((s.id>$4 AND s.sync_time=$5) OR (s.sync_time>$6)) " +
		"AND RIGHT(p.code, 6) = '.0.0.0' " + 
		"ORDER BY s.sync_time, s.id " +
		"LIMIT 1 "

	rows, err := con.Query(sqlStatement, data_type, group_id, phone_id, sync_id, sync_time, sync_time)
	if err != nil {
		return synchronisation, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		synchronisation = scan_Synchronisation_Row(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return synchronisation, err
	}

	if found {
		return synchronisation, nil
	} else {
		return synchronisation, errors.New("Error: Next Sync not found!")
	}
}

func Select_Synchronisation_ByDataIdentifier(tx *sql.Tx, data_type int, data_identifier string, user_id int64) (global.Synchronisation, error) {
	var synchronisation global.Synchronisation

	sqlStatement := "" +
		"SELECT id, data_type, data_identifier, user_id, phone_id, sync_order, sync_time, action " +
		"FROM tbl_sync " +
		"WHERE data_type=$1 AND data_identifier=$2 AND user_id=$3 "

	rows, err := tx.Query(sqlStatement, data_type, data_identifier, user_id)
	if err != nil {
		return synchronisation, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		synchronisation = scan_Synchronisation_Row(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return synchronisation, err
	}

	if found {
		return synchronisation, nil
	} else {
		return synchronisation, errors.New("Error: Sync DataIdentifier not found!")
	}
}

func Select_Synchronisation_ById(id int64) (global.Synchronisation, error) {
	var synchronisation global.Synchronisation

	sqlStatement := "" +
		"SELECT id, data_type, data_identifier, user_id, phone_id, sync_order, sync_time, action " +
		"FROM tbl_sync " +
		"WHERE id=$1 "

	rows, err := con.Query(sqlStatement, id)
	if err != nil {
		return synchronisation, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		synchronisation = scan_Synchronisation_Row(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return synchronisation, err
	}

	if found {
		return synchronisation, nil
	} else {
		return synchronisation, errors.New("Error: Sync Id not found!")
	}
}
