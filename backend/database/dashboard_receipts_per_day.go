package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func scan_ReceiptsPerDay_Row(rows *sql.Rows) global.ReceiptsPerDay {
	var groupName string
	var userName string
	var date int
	var receipts int64

	err = rows.Scan(&groupName, &userName, &date, &receipts)
	if err != nil {
		panic(err)
	}
	return global.ReceiptsPerDay{
		GroupName: 	groupName,
		UserName:   userName,
		Date:   	date,
		Receipts:  	receipts}
}

func select_ReceiptsPerDay(group_id int64) ([]global.ReceiptsPerDay, error) {
	var receiptsPerDays []global.ReceiptsPerDay
	
	sqlStatement := "" +		
	"SELECT g.name as Group_Name, u.name as User_Name,  " +
	"coalesce(tran.date, 0) as date, coalesce(tran.receipts, 0) as receipts  " +
	"FROM tbl_user u  " +
	"left join tbl_group g on u.group_id = g.id " +
	"left join ( " +
	"			SELECT st.user_id, date, count(sync_id) as receipts " +
	"			FROM tbl_receipt_transaction t " +
	"			left join tbl_sync st on t.sync_id = st.id " +
	"			group by st.user_id, date " +
	"		) tran on tran.user_id = u.id " +
	"where g.id = $1 " +
	"order by u.name, tran.date " 

	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return receiptsPerDays, err
	}
	defer rows.Close()
	
	var found bool = false
	for rows.Next() {
		receiptsPerDays = append(receiptsPerDays, scan_ReceiptsPerDay_Row(rows))
		found = true
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return receiptsPerDays, err
	}

	if found {
		return receiptsPerDays, nil
	} else {
		return receiptsPerDays, errors.New("Error: No ReceiptsPerDays foud")
	}
}


func Get_ReceiptsPerDay(group_id int64) (global.ReceiptsPerDayData, error) {
	var receiptsPerDays []global.ReceiptsPerDay
	receiptsPerDays, err = select_ReceiptsPerDay(group_id)

	var data global.ReceiptsPerDayData
	data.ReceiptsPerDays = receiptsPerDays

	return data, nil
}


