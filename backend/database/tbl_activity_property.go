package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Activity_Property(tx *sql.Tx, sync_id int64, p *global.ActivityProperty) error {
	sqlStatement := "" +
		"INSERT INTO tbl_activity_property (sync_id, treeCode, dateId, activityType, " +
		"	startHour, startMinute, key, value) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8) "

	_, err = tx.Exec(sqlStatement, sync_id,
		p.TreeCode, p.DateId, p.ActivityType, p.StartHour, p.StartMinute, p.Key, p.Value )
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted ActivityIProperty")
	return nil
}

func Delete_Activity_Properties(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_activity_property " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Activity_Properties")
	return nil
}

func Exists_Activity_Properties(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM tbl_activity_property WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scan_Activity_Property_Row(rows *sql.Rows) global.ActivityProperty {	
	var sync_id int64
	var treeCode string
	var dateId int64
	var activityType int
	var startHour int
	var startMinute int
	var key string
	var value string

	err = rows.Scan(&sync_id, &treeCode, &dateId, &activityType, &startHour, &startMinute, &key, &value)
	if err != nil {
		panic(err)
	}
	return global.ActivityProperty{
		TreeCode:   	treeCode,
		DateId:         dateId,
		ActivityType: 	activityType,
		StartHour:     	startHour,
		StartMinute:    startMinute,
		Key:     		key,
		Value:     		value}
}

func Select_Activity_Properties(sync_id int64) ([]global.ActivityProperty, error) {
	var activityProperties []global.ActivityProperty

	sqlStatement := "" +
		"SELECT sync_id, treeCode, dateId, activityType, " +
		"	startHour, startMinute, key, value " +
		"FROM tbl_activity_property " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return activityProperties, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		activityProperties = append(activityProperties, scan_Activity_Property_Row(rows))
		found = true
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return activityProperties, err
	}

	if found {
		return activityProperties, nil
	} else {
		return activityProperties, errors.New("Error: Activity_Properties not found!")
	}
}
