package database

import (
	_ "github.com/lib/pq"
	"backend/global"
)

func PushNextSearchStoreData(body global.SearchStoreBody) int64 {
	var user global.User
	var phone global.Phone
	var sync_id int64
	var updated bool
	var addData bool

	var errS error
	var errP error

	user, _ = Select_User_ById(body.Synchronisation.User_Id)
	phone, _ = Select_Phone_ById(body.Synchronisation.Phone_Id)

	var pushed int64 = body.Synchronisation.Sync_Order
	var latest int64 = user.Sync_Order
	latest = latest + 1
	body.Synchronisation.Sync_Order = latest

	//BEGIN TRANSACTION #####################################
	global.MyPrint("Begin Transaction")
	var validTransaction bool = true
	tx, err := con.Begin()
	if err != nil {
		validTransaction = false
	} else {

		addData = false

		var synchronisation global.Synchronisation
		synchronisation, errS = Select_Synchronisation_ByDataIdentifier(tx,
			body.Synchronisation.Data_Type,
			body.Synchronisation.Data_Identifier, body.Synchronisation.User_Id)

		if errS != nil {
			sync_id, errS = Create_Synchronisation(tx, body.Synchronisation)
			if errS != nil {
				validTransaction = false
			} else {
				addData = true
			}
		} else {
			sync_id = synchronisation.Id
			body.Synchronisation.Id = synchronisation.Id
			updated, err = Update_Synchronisation(tx, body.Synchronisation)
			if err != nil {
				validTransaction = false
			} else {
				if updated {
					errP = Delete_SearchStore(tx, sync_id)
					if errP != nil {
						validTransaction = false
					} else {
						addData = true
					}
				}
			}
		}

		if validTransaction {
			if addData {
				errP = Create_SearchStore(tx, sync_id, body.SearchStore)
				if errP != nil {
					validTransaction = false
				} 
			}
		}

		if validTransaction {
			user.Sync_Order = latest
			err = Update_UserSyncOrder(tx, user)
			if err != nil {
				validTransaction = false
			} else {
				phone.Sync_Order = pushed
				err = Update_Phone(tx, phone)
				if err != nil {
					validTransaction = false
				}
			}
		}
	}

	if validTransaction {
		tx.Commit()
		global.MyPrint("& Commit Transaction")
	} else {
		tx.Rollback()
		global.MyPrint("! Rollback Transaction")
	}
	//END TRANSACTION #######################################

	return sync_id
}

func PullNextSearchStoreData(user_id int64, phone_id int64, pulled_sync_order int64) (global.SearchStoreData, error) {
	var synchronisation global.Synchronisation
	var searchStore global.SearchStore

	synchronisation, err = Select_Synchronisation_NextToPull(global.DATA_SEARCH_STORE, user_id, phone_id, pulled_sync_order)

	if synchronisation.Action != global.ACTION_DELETE {
		searchStore, err = Select_SearchStore(synchronisation.Id)
	}

	var data global.SearchStoreData
	data.Synchronisation = &synchronisation
	data.SearchStore = &searchStore

	return data, nil
}

func GetSearchStoreData(sync_id int64) global.SearchStoreData {
	var synchronisation global.Synchronisation
	var searchStore global.SearchStore

	synchronisation, _ = Select_Synchronisation_ById(sync_id)
	searchStore, _ = Select_SearchStore(synchronisation.Id)

	var data global.SearchStoreData
	data.Synchronisation = &synchronisation
	data.SearchStore = &searchStore

	return data
}
