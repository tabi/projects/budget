= Statistical Service Implementation
:Author: Ole Mussmann
:Email: bo.mussmann@cbs.nl
:Date: 2020-03-19
:Document-Revision: 0.1
:Software-Revision: 0.1

*Physical description*, version _{document-revision}_

{date} by {author} {email}

== *@HBS App Backend*

.Version
{software-revision}

.Ownership
https://www.cbs.nl/[Statistics Netherlands (CBS)]

== Related Documents
=== Backend
link:01_Backend_Definition.adoc[Statistical Service Definition (Backend)]

link:02_Backend_Specification.adoc[Statistical Service Specification (Backend)]

=== App
link:../../app/CSPA/01_App_Definition.adoc[Statistical Service Definition (App)]

link:../../app/CSPA/02_App_Specification.adoc[Statistical Service Specification (App)]

link:../../app/CSPA/03_App_Implementation.adoc[Statistical Service Implementation (App)]

== Invocation Protocols
*REST Web Services* - Service exposes a REST interface and is addressed by a https URI.

== Service Interface
curl ..., swagger

== Technical Dependencies

== Installation Document

== TODO
* service interface curl command examples
* technical dependencies
* installation document
