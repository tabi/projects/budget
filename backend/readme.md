# Introduction

This readme describes how to setup the backend for the HBS app.

Make sure to replace values contained within square brackets, such as "[username]", with your own value, e.g. "postgres".

Development was done on MacOS. And this is what most of the instructions apply to. For Windows and Linux most of the instructions will be very similar, but you will have to make some small adjustments.

# Prerequisites

## **1. Golang**

To check if it is already installed, run:

```bash
go version
```

To install, go to https://golang.org/doc/install (version go1.17.8 darwin/amd64 was used for development)

## **2. Postgress**

To check if it is already installed, run:

```bash
psql -V
```

To install, go to https://www.postgresql.org/download/ (version 14.2 was used for development)

During the installation, make sure to write down the values you've set for the:
- Port
- Username
- Password

And make sure that the folder which contains the psql executable is in your PATH variable. For example, add the following line to your .baschrc or .zshrc file: ```export PATH="$PATH":"/Library/PostgreSQL/14/bin"```.


# Setting up the Postgres database



1. Use the psql tool to connect to your postgres server:
```bash
psql -U [username]
```

2. Create a new database named '[budget]'.
```sql
CREATE DATABASE [budget];
```
3. Enable encryption
```sql
CREATE EXTENSION pgcrypto;
```

4. Exit the psql CLI.
```bash
\q
```
5. Setup the database tables.

- Within the ```database/create_tables.sql``` file replace all occurences of "sb_e69c3469-71fa-48ca-9e82-9f11fe1e4aa4" with your postgress username.
- Then run:
```bash
sudo psql -U [username] -d [budget] -a -f "[path_to_file]/create_tables.sql"
```
# Running the server

1. The server uses enviroment variables to configure the connection with the postgres database. As such make sure to add the following variables to your enviroment variables:

```bash
export PORT="[8000]"
export DB_HOST="[localhost]"
export DB_NAME="[budget]"
export DB_PORT="[5432]"
export DB_USER="[postgres]"
export DB_PASSWORD="[password]"
```

2. Run the server:

```bash
go run backend.go
```

To kill an earlier instance of the server which is occupying your port (in this example 8000), run ```kill -9 $(lsof -t -i:8000)```

Confirm that the server is running, visit http://localhost:8000/budget/test

It is possible to enable and disable encryption of sensative data such as passwords. To modify this behavior update the ENCRYPT variable in ```database/tbl_user.go```.

# Populating the server with data

To use the backend you'll need to add groups, superusers, users, etc. To do so follow the instructions in ```endpoint_manual.md```. 

For more information about the data schema used by the backend or about the way it synchronizes data read `backend_description.md`.
