# Deployment with Docker

All commands must be run from the `backend` folder.

## Microservices
Instead of one big application, we can deploy several connected microservices.
This allows for independent development of the individual components and more fine-grained testing.

So far there are three services:
1. PostgreSQL database - the data store
2. PGadmin4 - to inspect and maintain the database
3. Go backend server - REST API endpoint for apps, using the PostgreSQL DB

## Build (optional)
There's no need to explicitly build the containers before the first run.
This will be done automatically by `docker-compose` if necessary.
If you still want to build separately, use the following command:
```
docker-compose -f docker/docker-compose.yml build
```

To re-build individual components, for example `go_server` and `pgadmin4`:
```
docker-compose -f docker/docker-compose.yml build go_server pgadmin4
```

## Start
1. Copy the `docker/budget.env.EXAMPLE` file.
   ```
   cp docker/budget.env.EXAMPLE docker/budget.env
   ```
   `*.env` files are in the `.gitignore`, so they should not be commited to the repository.
   Double-check that, just in case.

2. Then, edit it to your liking.
   **Make sure you change all usernames and passwords.**
   If you change ports here, make the same changes to `docker/docker-compose.yml`.

   For the first start, please set `INITIALIZE_DATABASE=true`.
   This will create all the necessary tables in the PostgreSQL DB.
   For subsequent runs, **please set it to `false`**.
   Otherwise you will lose all data.

3. Now you can start the services:
   ```
   docker-compose -f docker/docker-compose.yml up
   ```

## Stop
1. First, press `CTRL+C` in the terminal where docker is running.
   Wait for everything to finish and shut down.

2. Then, clean up everything that's left behind:
   ```
   docker-compose -f docker/docker-compose.yml down
   ```
   The database itself as well as configuration files from PGadmin4 are stored in persistent docker volumes.
   Killing the services does thus not remove any data.

   To also remove all data and start with a clean slate, use the `--volumes` flag to also remove the volumes:
   ```
   docker-compose -f docker/docker-compose.yml down --volumes
   ```

## Known issues
- So far there is only one DB user. Ideally you want to have a super-user for maintenance, and a user with restricted powers for daily operations.
- The PGadmin4 container tries to load the server config every time it is started. This is not necessary on subsequent runs where no configurations are changed.
