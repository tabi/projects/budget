package passwords

import (
	"math/rand"
	"strconv"
	"time"
)

func CreatePassword(passwordDefinition string) string {
    rand.Seed(time.Now().UTC().UnixNano())
	var pwd string = ""
	for i := 0; i < len(passwordDefinition); i++ {
		pwd = pwd + randomCharacter(passwordDefinition[i:i+1])
	}
	return pwd
}

func randomCharacter(characterDefinition string) string {
	var characterDefinitions []string = []string{"D", "U", "L", "M"}
	switch characterDefinition {
	case "D":
		return randomDigit()
	case "U":
		return randomUcase()
	case "L":
		return randomLcase()
	case "M":
		return randomMark()
	default:
		return randomCharacter(characterDefinitions[rand.Intn(4)])
	}
}

func randomDigit() string {
	return strconv.Itoa(rand.Intn(10))
}

func randomUcase() string {
	return string(int('A') + rand.Intn(26))
}

func randomLcase() string {
	return string(int('a') + rand.Intn(26))
}

func randomMark() string {
	switch rand.Intn(4) {
	case 0:
		return "-"
	case 1:
		return "."
	case 2:
		return "_"
	case 3:
		return "~"
	default:
		return "."
	}
}
