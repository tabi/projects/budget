# Backend documentation HBS

This document is an overview of the source code of the backend that is used for the Household Budget Survey app developed at CBS

The backend for these apps consists of:
- A web service written in GO language
- A PostgreSQL database


## PostgreSQL database setup

[1] Create PostgreSQL database

[2] Enable password encryption
The backend uses password encryption methods provided by PostgreSQL pgcrypto. To be able to use pgcrypto it has to be enabled after the creation of a new PostgreSQL database. You (or a super user) can do this with the following command:

```CREATE EXTENSION pgcrypto;```

If you want to check if pgcrypto is enabled run the following command:

```SELECT crypt('jjj', gen_salt('bf', 8));```

You can use the backend without password encryption.  You do not want this, but if there is no super user available to enable pgcrypto and you cannot wait to test some stuff it is possible to temporarily turn password encryption off. You can do this by editing the file:
```/backend/database/tbl_user.go```

To turn off password encryption replace true with false in the next line:
```const ENCRYPT bool = true```
Do not forget to turn password encryption on (true) when pgcrypto is enabled.

 [3] Create tables
The tables of the database can be created with the script: 
```/backend/database/create_tables.sql```
You have to change the owner of the tables before running this script. Replace TABLE_OWNER by the table owner of the PostgreSQL database in all command lines that look like:  

```ALTER TABLE public.tbl_... OWNER to "TABLE_OWNER";```

You can always find the table owner of a PostgreSQL database by creating a dummy table, than generate a creation script for the dummy table and see which table owner is used in this script.  


[4] Group, users and phone tables
Groups, users and phones are maintained with five tables:
- tbl_group
- id BIGSERIAL PRIMARY KEY
- name text NOT NULL
- status text NOT NULL
- tbl_group_info
    - id BIGSERIAL PRIMARY KEY
    - group_id bigint NOT NULL: FOREIGN KEY REFERENCES tbl_group (id)
    - key text NOT NULL
    - value text NOT NULL
- tbl_user
    - id BIGSERIAL PRIMARY KEY
    - group_id bigint NOT NULL: FOREIGN KEY REFERENCES tbl_group (id)
    - name text NOT NULL
    - password text NOT NULL
    - sync_order bigint NOT NULL
- tbl_phone
    - id BIGSERIAL PRIMARY KEY
    - user_id bigint NOT NULL: FOREIGN KEY REFERENCES tbl_user (id)
    - name text NOT NULL
    - sync_order bigint NOT NULL
- tbl_phone_info
    - id BIGSERIAL PRIMARY KEY
    - phone_id bigint NOT NULL: FOREIGN KEY REFERENCES tbl_phone (id)
    - key text NOT NULL
    - value text NOT NULL

A group has a name and a status (enabled or disabled). A group has a group_info set which is a set of key-value pairs. These key-value pairs can be used to configure the functionality of the app when a user first registers the usage of an app to the backend.
A users has a name (or number) and a password (encrypted) and belongs to a group. The sync_order of a user is used by the data synchronization method and is explained later. Users can only register the usage of an app when they exist (password check) and are part of a group that is enabled. 

A phone has a name (or number or timestamp) and belongs to a user. The sync_order of a phone is used by the data synchronization method and is explained later. Each phone has a phone_info set which is a set of key-value pairs. These key-value pairs are properties of a phone that are gathered on the phone when a user first registers the usage of an app to the backend. Users can access their data with multiple phones. When a user deletes the app (and data) from a phone, and later reinstalls the app on the same phone, this phone is seen by the backend as a new phone with a new phone name (timestamp).


[5] Synchronization table
The data synchronization mechanism is maintained with one table:
- tbl_sync
    - id BIGSERIAL PRIMARY KEY
    - data_type integer NOT NULL
    - data_identifier TEXT NOT NULL
    - user_id bigint NOT NULL: FOREIGN KEY REFERENCES tbl_user (id)
    - phone_id bigint NOT NULL: FOREIGN KEY REFERENCES tbl_phone (id) 
    - sync_order bigint NOT NULL
    - sync_time bigint NOT NULL
    - action integer NOT NULL

A sync is associated with a single unit of data. What this unit of data is depends on the data. For HBS a unit of data is a receipt filled with purchased products. For TUS a unit of data is one day filled with  activities. There are also other types of units of data used in HBS (searched stores, searched products) and TUS (start questionnaire, experience sampling).

A sync has a data_type (HBS_receipt, TUS_day, …) and a data_identifier that uniquely identifies the data of a data_type (HBS_receipt_timestamp, TUS_day_date, …). The user_id and phone_id are those of the user/phone where the data wat last created, updated or deleted. The sync_time is the timestamp (created on the phone) of the last data change. The action is create, update or delete.

A sync also has a sync_order. The sync_order and sync_time of a sync combined with the sync_order of a user and the sync_order of a phone are used to synchronise data between backend and possible multiple phones of a user.  This data synchronization method is explained later.

[6] Data tables

- tbl_receipt_image
    - sync_id bigint NOT NULL
    - transaction_id TEXT
    - image bytea

- tbl_receipt_product
    - sync_id bigint NOT NULL
    - transaction_id TEXT (for data analyses please ignore this value, use transaction_id from tbl_receipt_transaction instead, which can be joined to this table using the sync_id key)
    - product TEXT
    - product_category TEXT
    - price float
    - product_code TEXT
    - product_date TEXT
    - product_group_id TEXT
    - tbl_receipt_transaction
    - sync_id bigint NOT NULL
    - transaction_id TEXT (duplicate transaction id's implicate that the receipt has been updated by the user)
    - store TEXT
    - store_type TEXT
    - date int
    - discount_amount TEXT
    - discount_percentage TEXT
    - expense_abroad TEXT
    - expense_online TEXT
    - total_price float
    - discount_text TEXT
    - receipt_location TEXT (this temporarily also contains a score obtained from the local OCR plugin, which is concatenated to the end of the string, after '&score:')
    - receipt_producttype TEXT
    - tbl_start_questionnaire
    - sync_id bigint NOT NULL
    - age int
    - gender TEXT
    - livingSituation TEXT
    - partner int
    - paidWork int



## Synchronization mechanism

This is an explaination the data synchronization mechanism for one user and one data_type.

Initialization:

- Initially on each Phone of a user:
    - Phone.ID = unique phone ID
    - Phone.SyncOrder_Phone = 0 {created on phone}
    - Phone.SyncOrder_Backend = 0 {received from backend}
    - Phone.Sync = empty set

- Initially on the Backend database:
    -  Backend.SyncOrder_Backend = 0 {created on backend}
    - Backend.Phone.SyncOrder_Phone = 0 {received from phone}
    - Backend.Sync = empty set

-  Each unit of data is uniquely defined by an ID. The properties of Sync element Sync.ID are:
    - PhoneID {identifies phone that made the last change}
    - SyncOrder {different sequences used on Phones and Backend}
    - SyncTime {timestamp created on phone that made the last change}
    - Action (create/update/delete)


For each unit of data ID there are corresponding Sync.ID elements on Phones and Backend.

When unit of data ID gets created/updated/deleted on a Phone:

1. Phone.SyncOrder_Phone = Phone.SyncOrder_Phone + 1 

2. For Phone.Sync.ID:

    - PhoneID = Phone.ID

    - SyncOrder = Phone.SyncOrder_Phone {Note 2}

    - SyncTime = NOW()

    - Action = create/update/delete

3. Unit of data ID is created/updated/deleted on Phone


A Phone does not know if data has been received successfully by the Backend. The Backend knows the latest data that was successfully received from a phone: Backend.Phone.SyncOrder_Phone.

The Backend does not know if data has been received successfully by a Phone. A Phone knows the latest data that was successfully received from the backend: Phone.SyncOrder_Backend.


Send (Push) unit of data from Phone to Backend (called in a loop):

1. On Backend:
- Backend knows last successfully received data from  phone:
    - Backend.Phone.SyncOrder_Phone
2. On Phone:
- Phone gets Backend.Phone.SyncOrder_Phone from Backend
- Phone finds next Sync.ID where:
    - Sync.ID.SyncOrder > Backend.Phone.SyncOrder_Phone
- Phone sends unit of data ID plus Phone.Sync.ID to Backend
3. On Backend:
- Backend receives unit of data ID and Phone.Sync.ID from Phone
- Backend.Phone.SyncOrder_Phone = Phone.Sync.ID.SyncOrder
- IF Phone.Sync.ID.SyncTime > Backend.Sync.ID.SyncTime
    - Backend.SyncOrder_Backend = Backend.SyncOrder_Backend + 1 
    - For Backend.Sync.ID:
        - PhoneID = Phone.Sync.ID.PhoneID
        - SyncOrder = Backend.SyncOrder_Backend 
        - SyncTime = Phone.Sync.ID.SyncTime 
        - Action = Phone.Sync.ID.Action
     - Unit of data ID is created/updated/deleted on Backend


Receive (Pull) unit of data from Backend to Phone (called in a loop)

1. On Phone:
- Phone knows last successfully received data from backend:
    - Phone.SyncOrder_Backend
2. On Phone:
- Phone asks backend for the next unit of data where: 
        - Backend.Sync.ID.SyncOrder > Phone.SyncOrder_Backend
3. On Backend:
- Backend receives request form Phone for unit of data where: 
    - Backend.Sync.ID.SyncOrder > Phone.SyncOrder_Backend
- Backend sends data ID plus Backend.Sync.Id to Phone where: 
    - Backend.Sync.ID.SyncOrder > Phone.SyncOrder_Backend
    - AND Backend.Sync.ID.PhoneID <> Phone.ID {Note 1}
4.  On Phone:
- Phone receives unit of data ID plus Backend.Sync.ID from Backend
- Phone.SyncOrder_Backend = Backend.Sync.ID.SyncOrder
- IF Backend.Sync.ID.SyncTime > Phone.Sync.ID.SyncTime
    - For Phone.Sync.ID:
    - PhoneID = Backend.Sync.ID.PhoneID
    - SyncOrder = 0 {Note 2}
    - SyncTime = Backend.Sync.ID.SyncTime
    - Action = Backend.Sync.ID.Action
- Unit of data ID is created/updated/deleted on Phone


Note1: A Phone never receives a unit of data from the backend that was created/updated/deleted on the Phone itself.


Note 2: A Phone never sends a unit of data to the Backend that is has received from the Backend (without changing it on the Phone). It only sends units of data where Phone.Sync.ID.SyncOrder > 0.
