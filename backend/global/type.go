package global

type PullDataBody struct{
	User 		*User 		`json:"user"`
	Phone 		*Phone 		`json:"phone"`
	Sync_Order 	int64	    `json:"syncOrder"`
}

type PullProcessBody struct{
	User 		*User 		`json:"user"`
	Phone 		*Phone 		`json:"phone"`
	Group 		*Group 		`json:"group"`
	Sync_Id 	int64	    `json:"syncId"`
	Sync_Time 	int64	    `json:"syncTime"`
}

type Synchronisation struct {
	Id              int64  `json:"id"`
	Data_Type		int    `json:"dataType"`
	Data_Identifier string `json:"dataIdentifier"`
	User_Id         int64  `json:"userId"`
	Phone_Id        int64  `json:"phoneId"`
	Sync_Order      int64  `json:"syncOrder"`
	Sync_Time       int64  `json:"syncTime"`
	Action          int    `json:"action"`
}

type TestBody struct {
	Entry      string    `json:"entry"`
	Reply      string    `json:"reply"`
}

type PullReceiptIndexBody struct{
	User 		*User 		`json:"user"`
	Group 		*Group 		`json:"group"`
	Index 		int64 		`json:"index"`
}