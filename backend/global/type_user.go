package global

type User struct {
	Id          int64     `json:"id"`
	Group_Id  	int64     `json:"groupId"`
	Name        string    `json:"name"`
	Password    string    `json:"password"`
	Sync_Order 	int64     `json:"syncOrder"`
}

type Generate struct {
	Number      int       `json:"number"`
	Prefix      string    `json:"prefix"`
	Folder	    string    `json:"folder"`
}

type UserBody struct {
	Superuser	*User		`json:"superuser"`
	Group		*Group		`json:"group"`
	Regroup		*Group		`json:"regroup"`
	Users		[]User		`json:"users"`
	Generate	*Generate	`json:"generate"`
}

type UserName struct {
	Name        string    `json:"name"`
}

type UserNameData struct {
	UserNames	[]UserName	`json:"userNames"`
}
