package global


type StartQuestionnaire struct {
    Results        	string  `json:"results"`
}
	
type StartQuestionnaireData struct {
	Synchronisation 	*Synchronisation 	`json:"synchronisation"`
	StartQuestionnaire  *StartQuestionnaire `json:"startQuestionnaire"`
}

type StartQuestionnaireBody struct {
	User 				*User 				`json:"user"`
	Phone 				*Phone 				`json:"phone"`
	Sync_Order 			int64	     		`json:"syncOrder"`
	Synchronisation 	*Synchronisation 	`json:"synchronisation"`
	StartQuestionnaire  *StartQuestionnaire `json:"startQuestionnaire"`
}