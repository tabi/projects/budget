package global


type Transaction struct {
	TransactionID      string  `json:"transactionID"`
	Store              string  `json:"store"`
	StoreType          string  `json:"storeType"`
	Date               int     `json:"date"`
	DiscountAmount     string  `json:"discountAmount"`
	DiscountPercentage string  `json:"discountPercentage"`
	DiscountText       string  `json:"discountText"`
	ExpenseAbroad      string  `json:"expenseAbroad"`
	ExpenseOnline      string  `json:"expenseOnline"`
	TotalPrice         float32 `json:"totalPrice"`
	ReceiptLocation    string  `json:"receiptLocation"`
	ReceiptProductType string  `json:"receiptProductType"`
}

type Product struct {
	TransactionID   string  `json:"transactionID"`
	Product         string  `json:"product"`
	ProductCategory string  `json:"productCategory"`
	Price           float32 `json:"price"`
	ProductCode     string  `json:"productCode"`
	ProductDate     string  `json:"productDate"`
	ProductGroupID  string  `json:"productGroupID"`
}

type Image struct {
	TransactionID   string  `json:"transactionID"`
	Base64image     string  `json:"base64image"`
}

type ReceiptData struct {
	Synchronisation *Synchronisation `json:"synchronisation"`
	Transaction     *Transaction     `json:"transaction"`
	Products        []Product        `json:"products"`
	Image			*Image           `json:"image"`
}

type ReceiptBody struct {
	User 			*User 				`json:"user"`
	Phone 			*Phone 				`json:"phone"`
	Sync_Order 		int64	     		`json:"syncOrder"`
	Synchronisation *Synchronisation 	`json:"synchronisation"`
	Transaction     *Transaction     	`json:"transaction"`
	Products        []Product        	`json:"products"`
	Image			*Image           	`json:"image"`
}
