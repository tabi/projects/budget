package restapi

import (
	"fmt"
	"net/http"
	"backend/global"
)

var err error

func setSynchronisation(sync *global.Synchronisation, data_type int, user_id int64, phone_id int64, sync_order int64) {
	sync.Data_Type = data_type
	sync.User_Id = user_id
	sync.Phone_Id = phone_id
	sync.Sync_Order = sync_order
}

func badRequest(w http.ResponseWriter, isJson bool, err string) {
	if !isJson {
		http.Error(w, err, http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, `{"result":"","error":%q}`, err)
}
