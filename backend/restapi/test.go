package restapi

import (
	"fmt"
	"encoding/json"
	"net/http"
	"backend/global"
)

func Test_WebServiceA(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("TestApp A")
	
	w.Header().Set("Host", "budgetonderzoek.ontwikkel.cbs.nl")
	fmt.Fprintf(w, "Budget Hallo A.")
}

func Test_WebServiceB(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("TestApp B")

	var body global.TestBody
	body.Entry = "Hallo B."
	body.Reply = "GO Budget web-service is running."

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(body)
}

func Test_WebServiceC(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("TestApp C")

	var body global.TestBody
	body.Entry = "-"
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		body.Entry = "Hallo C."
	}

	body.Reply = "GO Budget web-service is running."

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(body)
}

func Test_Loaderio(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Test_Loaderio")

	data := []byte("loaderio-d88cc05dd46717f147e8dddd74a2be24")
	w.Write(data)
}
