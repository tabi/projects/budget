package restapi

import (
	"encoding/json"
	"net/http"
	"backend/database"
	"backend/global"
)

// PhoneBody
// {
// 	"user" 		 : {"name" : "nu1n", "password" : "nu1p"},
// 	"phone" 	 : {"name" : "somephoneid"}, 
// 	"phoneInfos" : [ {"key" : "key1", "value" : "val1A"},
//                   {"key" : "key2", "value" : "val2A"}]
// }

func Phone_Register(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Phone_Register")

	var body global.PhoneBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserEnabled(*body.User) {
		body.User.Id = -1
		json.NewEncoder(w).Encode(body)
		return
	}

	var user global.User
	user, err = database.Select_User_ByNamePassword(*body.User)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var new_phone bool
	new_phone = false
	var phone global.Phone
	body.Phone.User_Id = user.Id
	phone, err = database.Select_Phone_ByUserIdName(*body.Phone)
	if err != nil {
		_, err := database.Create_Phone(user, *body.Phone)
		if err != nil {
			badRequest(w, true, err.Error())
			return
		}
		new_phone = true
	}

	phone, err = database.Select_Phone_ByUserIdName(*body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if new_phone {
		for _, phone_info := range body.Phone_Infos {
			phone_info.Phone_Id = phone.Id
			_, err = database.Create_PhoneInfo(phone_info)
			if err != nil {
				badRequest(w, true, err.Error())
				return
			}
		}
	}

	var group_infos []global.Group_Info
	group_infos, err = database.Select_GroupInfos_ByGroup(user.Group_Id)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	var phone_data global.PhoneData
	phone_data.User = &user
	phone_data.Phone = &phone
	phone_data.Group_Infos = group_infos

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(phone_data)
}
