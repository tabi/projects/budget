package restapi

import (
	"encoding/json"
	"net/http"
	"backend/database"
	"backend/global"
)

func Process_Pull_ReceiptForProcess(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Process_Pull_ReceiptForProcess")
	
	var body global.PullProcessBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserCbsProcess(*body.User) {
		badRequest(w, true, "User '" + body.User.Name + "' is not a CBS Process!")
		return
	}

	phone, err := database.RegisterPhone(*body.User, *body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	group, err := database.Select_Group_ByName(body.Group.Name)
	if err != nil {
		badRequest(w, true, "Group '" + body.Group.Name + "' does not exist!")
		return
	}

	var receiptData global.ReceiptData
	receiptData, _ = database.PullNextReceiptDataForProcess(phone.Id, group.Id, body.Sync_Id, body.Sync_Time) 

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(receiptData)
}


func Process_Push_ReceiptForProcess(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Process_Push_ReceiptForProcess")

	var body global.ReceiptBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}
	
	if !database.IsUserCbsProcess(*body.User) {
		badRequest(w, true, "User '" + body.User.Name + "' is not a CBS Process!")
		return
	}
	
	phone, err := database.RegisterPhone(*body.User, *body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	setSynchronisation(body.Synchronisation, global.DATA_RECEIPT, body.Synchronisation.User_Id, phone.Id, body.Sync_Order)
	sync_id := database.PushNextReceiptData(body, false)
	data := database.GetReceiptData(sync_id)
	
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}


