package restapi

import (
	"encoding/json"
	"net/http"
	"backend/database"
	"backend/global"
)


// type Parameter struct {
// 	Name 		string  `json:"name"`
// 	Value	 	string  `json:"value"`
// }

// type DashboardBody struct {
// 	Superuser	*User		`json:"superuser"`
// 	Group		*Group		`json:"group"`
// 	User		*User		`json:"user"`
// 	Parameters	[]Parameter	`json:"parameters"`
// }

func Dashboard_UserReceipts(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Dashboard_UserReceipts")

	var body global.DashboardBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	if !database.IsUserSuperuser(*body.Superuser) && !database.IsUserMonitoring(*body.Superuser) {
		badRequest(w, true, "Only superusers and monitoring users can view user receipts!")
		return
	}
	
	user, err := database.Select_User_ByName(body.User.Name)
	if err != nil {
		badRequest(w, true, "User '" + body.User.Name + "' does not exist!")
		return
	}

	var data global.UserReceiptsData
	data, _ = database.Get_UserReceipts(user.Id)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
