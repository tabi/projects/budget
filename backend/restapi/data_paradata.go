package restapi

import (
	"encoding/json"
	"net/http"
	"backend/database"
	"backend/global"
)

func Data_Push_Paradata(w http.ResponseWriter, r *http.Request) {
	global.MyPrint("#")
	global.MyPrint("Data_Push_Paradata")

	var body global.ParadataBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}
	
	phone, err := database.RegisterPhone(*body.User, *body.Phone)
	if err != nil {
		badRequest(w, true, err.Error())
		return
	}

	setSynchronisation(body.Synchronisation, global.DATA_PARADATA, phone.User_Id, phone.Id, body.Sync_Order)
	sync_id := database.PushNextParadataData(body)
	data := database.GetParadataData(sync_id)
	
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}


